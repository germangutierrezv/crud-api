<?php

namespace App\Helpers;
use App\Models\Audit;

class Helper
{
	public function user_log($operation, $user_id = null)
	{
		$user_id = $user_id == NULL ? (isset(auth()->user()->id) ? auth()->user()->id : NULL) : $user_id;
		$user_log = Audit::create([
            'ip' =>  \Request::ip(),
            'url' => \Request::getPathInfo(),
            'operation' => $operation,
            'user_id' => $user_id
        ]);
        \Log::info($user_log);

        return $user_log;
	}
}