<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\City;

class CityController extends Controller
{
    public function getCitiesByProvice(Request $request, $id)
    {
    	if($request->ajax()) {
    		$cities = City::province($id)
    			->get(['id','name','province_id']);

    		return $cities;
    	}
    }
}
