<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Country;

class CountryController extends Controller
{
    public function index(Request $request)
    {
    	if($request->ajax()) {
    		$countries = Country::get(['id','name']);

    		return response()->json($countries);
    	}
    }
}
