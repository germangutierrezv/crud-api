<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Requests\CreateMailRequest;
use App\Models\Mail as Email;
use Auth;
use App\Events\SendMailUser;

class MailController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
    	$mails = Email::send(Auth::id())->paginate(10);

    	if(Auth::user()->role == 'admin') {
    		$mails = Email::paginate(10);
    	}

    	return view('mail.index', compact('mails'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('mail.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {

    	$data = $request->all();

    	$data['send_id'] = Auth::id();

    	Email::create($data);

    	$send = [
    		'affair' => $data['affair'],
    		'destination' => $data['destination'],
    		'message' => $data['message']
    	];

    	event(new SendMailUser($send));

    	return redirect()->route('mails')->with('success','Email creado correctamente.');
    }

}
