<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Province;

class ProvinceController extends Controller
{
    public function getProvicesByCountry(Request $request, $id)
    {
    	if($request->ajax()) {
    		$provinces = Province::country($id)
    			->get(['id','name','country_id']);

    		return response()->json($provinces);

    	}
    }
}
