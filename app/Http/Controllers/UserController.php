<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\User;
use App\Http\Requests\CreateUserRequest;
use App\Http\Requests\UpdateUserRequest;
use Illuminate\Support\Facades\Hash;
use App\Helpers\Helper;

class UserController extends Controller
{
    public function __construct(Helper $helper)
    {
        $this->helper = $helper;
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $query = User::with('city');

        if($request->search) {
            $search = $request->search;

            $query = $query->where('name', 'LIKE' ,'%'. $search . '%')
                    ->orWhere('surnames', 'LIKE' ,'%'. $search . '%')
                    ->orWhere('identification_number', 'LIKE' ,'%'. $search . '%')
                    ->orWhere('email', 'LIKE' ,'%'. $search . '%')
                    ->orWhere('phone', 'LIKE' ,'%'. $search . '%');
        }

        $field = 'id';

        if($request->field) {
            $field = $request->field;
        }

        $sort = 'asc';

        if($request->sort) {
            $sort = $request->sort;
        }

        $query = $query->orderBy($field, $sort);

        $users = $query->paginate(10);

        if($request->ajax()){
           return [
                'users' => view('users.partial')->with(compact('users'))->render(),
                'next_page' => $users->nextPageUrl()
            ];
        }

        return view('users.index', compact('users'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('users.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(CreateUserRequest $request)
    {
        $data = $request->all();

        // Encripta el password
        $data['password'] = Hash::make($data['password']);
        //Asigna Rol
        $data['role'] = 'user';

        User::create($data);

        return redirect()->route('users.index')->with('success','Usuario creado correctamente.');
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(UpdateUserRequest $request, $id)
    {
        $data = $request->all();

        $user = User::find($id);

        $user->update($data);

        $this->helper->user_log('Editando registro ' . $id);

        if($request->ajax()){
            return response()->json('Usuario actualizado');
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy(Request $request, $id)
    {
        User::where('id', $id)->delete();

        $this->helper->user_log('Eliminando registro ' . $id);

        if($request->ajax()){
            return response()->json('Usuario eliminado');
        }

    }
}
