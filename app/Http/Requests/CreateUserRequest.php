<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class CreateUserRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'name' => 'required|max:100',
            'surnames' => 'required|max:100',
            'email' => 'required|email|unique:users,email',
            'phone' => 'required|min:8|max:11',
            'identification_number' => 'required|numeric|digits_between:11,11|unique:users,identification_number',
            'password' => 'required|min:8|regex:/^.*(?=.{1,})(?=.*[a-zA-Z])(?=.*[0-9])(?=.*[\d\x])(?=.*[!$#%]).*$/',
            'birthdate' => 'required',
            'city_id' => 'required|integer'
        ];
    }

    public function attributes()
    {
        return [
            'name' => 'Nombres',
            'surnames' => 'Apellidos',
            'email' => 'Correo Electronico',
            'phone' => 'Telefono',
            'identification_number' => 'Numero de identificacion',
            'password' => 'Contraseña',
            'birthdate' => 'Fecha de nacimiento',
            'city_id' => 'Ciudad'
        ];
    }
}
