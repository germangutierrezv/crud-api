<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class UpdateUserRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'names' => 'required|max:100',
            'surnames' => 'required|max:100',
            'email' => 'required|email|unique:users,email,'.$this->user,
            'phone' => 'required|min:8|max:11',
            'identification_number' => 'required|numeric|digits_between:11,11|unique:users,identification_number,'.$this->user,
            'birthdate' => 'required'
        ];
    }
}
