<?php

namespace App\Listeners;

use App\Events\SendMailUser as SendMailUserEvent;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Queue\InteractsWithQueue;
use Mail;
use App\Mail\SendEmail as SendEmailMail;

class SendMailUser
{
    /**
     * Create the event listener.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    /**
     * Handle the event.
     *
     * @param  SendMailUser  $event
     * @return void
     */
    public function handle(SendMailUserEvent $event)
    {
        Mail::to($event->data['destination'])->send(new SendEMailMail($event));
    }
}
