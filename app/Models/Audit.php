<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Audit extends Model
{
    use HasFactory;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'user_id',
        'ip',
        'url',
        'operation'
    ];

     /**
    * Relation country with province.
    */
    public function user()
    {
    	return $this->belongsTo(User::class, 'id', 'user_id');
    }
}
