<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Mail extends Model
{
    use HasFactory;

     /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'affair',
        'destination',
        'message',
        'status',
        'send_id'
    ];

     /**
    * Scope Send.
    */
    public function scopeSend($query, $value)
    {
        return $query->where('send_id', $value);
    }
}
