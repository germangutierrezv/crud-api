<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Province extends Model
{
    use HasFactory;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name',
        'country_id'
    ];

    /**
    * Relation province with city.
    */
    public function cities()
    {
        return $this->hasMany(City::class);
    }

    /**
    * Scope country.
    */
    public function scopeCountry($query, $value)
    {
        return $query->where('country_id', $value);
    }
}
