<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use DB;

class CitiesSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
    	$path = 'database/sql/cities.sql';
    	DB::unprepared(file_get_contents($path));
    	$this->command->info('Tabla Ciudades cargada correctamente!');
    }
}
