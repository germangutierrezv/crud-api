<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use App\Models\Province;

class ProvincesSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        Province::create([
			'id'=>1,
			'country_id'=>3,
			'name'=>'Azerbaijan'
		]);

		Province::create([
			'id'=>2,
			'country_id'=>3,
			'name'=>'Nargorni Karabakh'
		]);

		Province::create([
			'id'=>3,
			'country_id'=>3,
			'name'=>'Nakhichevanskaya Region'
		]);

		Province::create([
			'id'=>4,
			'country_id'=>4,
			'name'=>'Anguilla'
		]);

		Province::create([
			'id'=>5,
			'country_id'=>7,
			'name'=>'Brestskaya obl.'
		]);

		Province::create([
			'id'=>6,
			'country_id'=>7,
			'name'=>'Vitebskaya obl.'
		]);

		Province::create([
			'id'=>7,
			'country_id'=>7,
			'name'=>'Gomelskaya obl.'
		]);

		Province::create([
			'id'=>8,
			'country_id'=>7,
			'name'=>'Grodnenskaya obl.'
		]);

		Province::create([
			'id'=>9,
			'country_id'=>7,
			'name'=>'Minskaya obl.'
		]);

		Province::create([
			'id'=>10,
			'country_id'=>7,
			'name'=>'Mogilevskaya obl.'
		]);

		Province::create([
			'id'=>11,
			'country_id'=>8,
			'name'=>'Belize'
		]);

		Province::create([
			'id'=>12,
			'country_id'=>10,
			'name'=>'Hamilton'
		]);

		Province::create([
			'id'=>13,
			'country_id'=>15,
			'name'=>'Dong Bang Song Cuu Long'
		]);

		Province::create([
			'id'=>14,
			'country_id'=>15,
			'name'=>'Dong Bang Song Hong'
		]);

		Province::create([
			'id'=>15,
			'country_id'=>15,
			'name'=>'Dong Nam Bo'
		]);

		Province::create([
			'id'=>16,
			'country_id'=>15,
			'name'=>'Duyen Hai Mien Trung'
		]);

		Province::create([
			'id'=>17,
			'country_id'=>15,
			'name'=>'Khu Bon Cu'
		]);

		Province::create([
			'id'=>18,
			'country_id'=>15,
			'name'=>'Mien Nui Va Trung Du'
		]);

		Province::create([
			'id'=>19,
			'country_id'=>15,
			'name'=>'Thai Nguyen'
		]);

		Province::create([
			'id'=>20,
			'country_id'=>16,
			'name'=>'Artibonite'
		]);

		Province::create([
			'id'=>21,
			'country_id'=>16,
			'name'=>'GrandAnse'
		]);

		Province::create([
			'id'=>22,
			'country_id'=>16,
			'name'=>'North West'
		]);

		Province::create([
			'id'=>23,
			'country_id'=>16,
			'name'=>'West'
		]);

		Province::create([
			'id'=>24,
			'country_id'=>16,
			'name'=>'South'
		]);

		Province::create([
			'id'=>25,
			'country_id'=>16,
			'name'=>'South East'
		]);

		Province::create([
			'id'=>26,
			'country_id'=>17,
			'name'=>'Grande-Terre'
		]);

		Province::create([
			'id'=>27,
			'country_id'=>17,
			'name'=>'Basse-Terre'
		]);

		Province::create([
			'id'=>28,
			'country_id'=>21,
			'name'=>'Abkhazia'
		]);

		Province::create([
			'id'=>29,
			'country_id'=>21,
			'name'=>'Ajaria'
		]);

		Province::create([
			'id'=>30,
			'country_id'=>21,
			'name'=>'Georgia'
		]);

		Province::create([
			'id'=>31,
			'country_id'=>21,
			'name'=>'South Ossetia'
		]);

		Province::create([
			'id'=>32,
			'country_id'=>23,
			'name'=>'Al QÃ„Âhira'
		]);

		Province::create([
			'id'=>33,
			'country_id'=>23,
			'name'=>'Aswan'
		]);

		Province::create([
			'id'=>34,
			'country_id'=>23,
			'name'=>'Asyut'
		]);

		Province::create([
			'id'=>35,
			'country_id'=>23,
			'name'=>'Beni Suef'
		]);

		Province::create([
			'id'=>36,
			'country_id'=>23,
			'name'=>'Gharbia'
		]);

		Province::create([
			'id'=>37,
			'country_id'=>23,
			'name'=>'Damietta'
		]);

		Province::create([
			'id'=>38,
			'country_id'=>24,
			'name'=>'Southern District'
		]);

		Province::create([
			'id'=>39,
			'country_id'=>24,
			'name'=>'Central District'
		]);

		Province::create([
			'id'=>40,
			'country_id'=>24,
			'name'=>'Northern District'
		]);

		Province::create([
			'id'=>41,
			'country_id'=>24,
			'name'=>'Haifa'
		]);

		Province::create([
			'id'=>42,
			'country_id'=>24,
			'name'=>'Tel Aviv'
		]);

		Province::create([
			'id'=>43,
			'country_id'=>24,
			'name'=>'Jerusalem'
		]);

		Province::create([
			'id'=>44,
			'country_id'=>25,
			'name'=>'Bangala'
		]);

		Province::create([
			'id'=>45,
			'country_id'=>25,
			'name'=>'Chhattisgarh'
		]);

		Province::create([
			'id'=>46,
			'country_id'=>25,
			'name'=>'Karnataka'
		]);

		Province::create([
			'id'=>47,
			'country_id'=>25,
			'name'=>'Uttaranchal'
		]);

		Province::create([
			'id'=>48,
			'country_id'=>25,
			'name'=>'Andhara Pradesh'
		]);

		Province::create([
			'id'=>49,
			'country_id'=>25,
			'name'=>'Assam'
		]);

		Province::create([
			'id'=>50,
			'country_id'=>25,
			'name'=>'Bihar'
		]);

		Province::create([
			'id'=>51,
			'country_id'=>25,
			'name'=>'Gujarat'
		]);

		Province::create([
			'id'=>52,
			'country_id'=>25,
			'name'=>'Jammu and Kashmir'
		]);

		Province::create([
			'id'=>53,
			'country_id'=>25,
			'name'=>'Kerala'
		]);

		Province::create([
			'id'=>54,
			'country_id'=>25,
			'name'=>'Madhya Pradesh'
		]);

		Province::create([
			'id'=>55,
			'country_id'=>25,
			'name'=>'Manipur'
		]);

		Province::create([
			'id'=>56,
			'country_id'=>25,
			'name'=>'Maharashtra'
		]);

		Province::create([
			'id'=>57,
			'country_id'=>25,
			'name'=>'Megahalaya'
		]);

		Province::create([
			'id'=>58,
			'country_id'=>25,
			'name'=>'Orissa'
		]);

		Province::create([
			'id'=>59,
			'country_id'=>25,
			'name'=>'Punjab'
		]);

		Province::create([
			'id'=>60,
			'country_id'=>25,
			'name'=>'Pondisheri'
		]);

		Province::create([
			'id'=>61,
			'country_id'=>25,
			'name'=>'Rajasthan'
		]);

		Province::create([
			'id'=>62,
			'country_id'=>25,
			'name'=>'Tamil Nadu'
		]);

		Province::create([
			'id'=>63,
			'country_id'=>25,
			'name'=>'Tripura'
		]);

		Province::create([
			'id'=>64,
			'country_id'=>25,
			'name'=>'Uttar Pradesh'
		]);

		Province::create([
			'id'=>65,
			'country_id'=>25,
			'name'=>'Haryana'
		]);

		Province::create([
			'id'=>66,
			'country_id'=>25,
			'name'=>'Chandigarh'
		]);

		Province::create([
			'id'=>67,
			'country_id'=>26,
			'name'=>'Azarbayjan-e Khavari'
		]);

		Province::create([
			'id'=>68,
			'country_id'=>26,
			'name'=>'Esfahan'
		]);

		Province::create([
			'id'=>69,
			'country_id'=>26,
			'name'=>'Hamadan'
		]);

		Province::create([
			'id'=>70,
			'country_id'=>26,
			'name'=>'Kordestan'
		]);

		Province::create([
			'id'=>71,
			'country_id'=>26,
			'name'=>'Markazi'
		]);

		Province::create([
			'id'=>72,
			'country_id'=>26,
			'name'=>'Sistan-e Baluches'
		]);

		Province::create([
			'id'=>73,
			'country_id'=>26,
			'name'=>'Yazd'
		]);

		Province::create([
			'id'=>74,
			'country_id'=>26,
			'name'=>'Kerman'
		]);

		Province::create([
			'id'=>75,
			'country_id'=>26,
			'name'=>'Kermanshakhan'
		]);

		Province::create([
			'id'=>76,
			'country_id'=>26,
			'name'=>'Mazenderan'
		]);

		Province::create([
			'id'=>77,
			'country_id'=>26,
			'name'=>'Tehran'
		]);

		Province::create([
			'id'=>78,
			'country_id'=>26,
			'name'=>'Fars'
		]);

		Province::create([
			'id'=>79,
			'country_id'=>26,
			'name'=>'Horasan'
		]);

		Province::create([
			'id'=>80,
			'country_id'=>26,
			'name'=>'Husistan'
		]);

		Province::create([
			'id'=>81,
			'country_id'=>30,
			'name'=>'Aktyubinskaya obl.'
		]);

		Province::create([
			'id'=>82,
			'country_id'=>30,
			'name'=>'Alma-Atinskaya obl.'
		]);

		Province::create([
			'id'=>83,
			'country_id'=>30,
			'name'=>'Vostochno-Kazahstanskaya obl.'
		]);

		Province::create([
			'id'=>84,
			'country_id'=>30,
			'name'=>'Gurevskaya obl.'
		]);

		Province::create([
			'id'=>85,
			'country_id'=>30,
			'name'=>'Zhambylskaya obl. (Dzhambulskaya obl.)'
		]);

		Province::create([
			'id'=>86,
			'country_id'=>30,
			'name'=>'Dzhezkazganskaya obl.'
		]);

		Province::create([
			'id'=>87,
			'country_id'=>30,
			'name'=>'Karagandinskaya obl.'
		]);

		Province::create([
			'id'=>88,
			'country_id'=>30,
			'name'=>'Kzyl-Ordinskaya obl.'
		]);

		Province::create([
			'id'=>89,
			'country_id'=>30,
			'name'=>'Kokchetavskaya obl.'
		]);

		Province::create([
			'id'=>90,
			'country_id'=>30,
			'name'=>'Kustanaiskaya obl.'
		]);

		Province::create([
			'id'=>91,
			'country_id'=>30,
			'name'=>'Mangystauskaya (Mangyshlakskaya obl.)'
		]);

		Province::create([
			'id'=>92,
			'country_id'=>30,
			'name'=>'Pavlodarskaya obl.'
		]);

		Province::create([
			'id'=>93,
			'country_id'=>30,
			'name'=>'Severo-Kazahstanskaya obl.'
		]);

		Province::create([
			'id'=>94,
			'country_id'=>30,
			'name'=>'Taldy-Kurganskaya obl.'
		]);

		Province::create([
			'id'=>95,
			'country_id'=>30,
			'name'=>'Turgaiskaya obl.'
		]);

		Province::create([
			'id'=>96,
			'country_id'=>30,
			'name'=>'Akmolinskaya obl. (Tselinogradskaya obl.)'
		]);

		Province::create([
			'id'=>97,
			'country_id'=>30,
			'name'=>'Chimkentskaya obl.'
		]);

		Province::create([
			'id'=>98,
			'country_id'=>31,
			'name'=>'Littoral'
		]);

		Province::create([
			'id'=>99,
			'country_id'=>31,
			'name'=>'Southwest Region'
		]);

		Province::create([
			'id'=>100,
			'country_id'=>31,
			'name'=>'North'
		]);

		Province::create([
			'id'=>101,
			'country_id'=>31,
			'name'=>'Central'
		]);

		Province::create([
			'id'=>102,
			'country_id'=>33,
			'name'=>'Government controlled area'
		]);

		Province::create([
			'id'=>103,
			'country_id'=>33,
			'name'=>'Turkish controlled area'
		]);

		Province::create([
			'id'=>104,
			'country_id'=>34,
			'name'=>'Issik Kulskaya Region'
		]);

		Province::create([
			'id'=>105,
			'country_id'=>34,
			'name'=>'Kyrgyzstan'
		]);

		Province::create([
			'id'=>106,
			'country_id'=>34,
			'name'=>'Narinskaya Region'
		]);

		Province::create([
			'id'=>107,
			'country_id'=>34,
			'name'=>'Oshskaya Region'
		]);

		Province::create([
			'id'=>108,
			'country_id'=>34,
			'name'=>'Tallaskaya Region'
		]);

		Province::create([
			'id'=>109,
			'country_id'=>37,
			'name'=>'al-Jahra'
		]);

		Province::create([
			'id'=>110,
			'country_id'=>37,
			'name'=>'al-Kuwait'
		]);

		Province::create([
			'id'=>111,
			'country_id'=>38,
			'name'=>'Latviya'
		]);

		Province::create([
			'id'=>112,
			'country_id'=>39,
			'name'=>'Tarabulus'
		]);

		Province::create([
			'id'=>113,
			'country_id'=>39,
			'name'=>'Bengasi'
		]);

		Province::create([
			'id'=>114,
			'country_id'=>40,
			'name'=>'Litva'
		]);

		Province::create([
			'id'=>115,
			'country_id'=>43,
			'name'=>'Moldova'
		]);

		Province::create([
			'id'=>116,
			'country_id'=>45,
			'name'=>'Auckland'
		]);

		Province::create([
			'id'=>117,
			'country_id'=>45,
			'name'=>'Bay of Plenty'
		]);

		Province::create([
			'id'=>118,
			'country_id'=>45,
			'name'=>'Canterbury'
		]);

		Province::create([
			'id'=>119,
			'country_id'=>45,
			'name'=>'Gisborne'
		]);

		Province::create([
			'id'=>120,
			'country_id'=>45,
			'name'=>'Hawkes Bay'
		]);

		Province::create([
			'id'=>121,
			'country_id'=>45,
			'name'=>'Manawatu-Wanganui'
		]);

		Province::create([
			'id'=>122,
			'country_id'=>45,
			'name'=>'Marlborough'
		]);

		Province::create([
			'id'=>123,
			'country_id'=>45,
			'name'=>'Nelson'
		]);

		Province::create([
			'id'=>124,
			'country_id'=>45,
			'name'=>'Northland'
		]);

		Province::create([
			'id'=>125,
			'country_id'=>45,
			'name'=>'Otago'
		]);

		Province::create([
			'id'=>126,
			'country_id'=>45,
			'name'=>'Southland'
		]);

		Province::create([
			'id'=>127,
			'country_id'=>45,
			'name'=>'Taranaki'
		]);

		Province::create([
			'id'=>128,
			'country_id'=>45,
			'name'=>'Tasman'
		]);

		Province::create([
			'id'=>129,
			'country_id'=>45,
			'name'=>'Waikato'
		]);

		Province::create([
			'id'=>130,
			'country_id'=>45,
			'name'=>'Wellington'
		]);

		Province::create([
			'id'=>131,
			'country_id'=>45,
			'name'=>'West Coast'
		]);

		Province::create([
			'id'=>132,
			'country_id'=>49,
			'name'=>'Saint-Denis'
		]);

		Province::create([
			'id'=>133,
			'country_id'=>50,
			'name'=>'Altaiskii krai'
		]);

		Province::create([
			'id'=>134,
			'country_id'=>50,
			'name'=>'Amurskaya obl.'
		]);

		Province::create([
			'id'=>135,
			'country_id'=>50,
			'name'=>'Arhangelskaya obl.'
		]);

		Province::create([
			'id'=>136,
			'country_id'=>50,
			'name'=>'Astrahanskaya obl.'
		]);

		Province::create([
			'id'=>137,
			'country_id'=>50,
			'name'=>'Bashkiriya obl.'
		]);

		Province::create([
			'id'=>138,
			'country_id'=>50,
			'name'=>'Belgorodskaya obl.'
		]);

		Province::create([
			'id'=>139,
			'country_id'=>50,
			'name'=>'Bryanskaya obl.'
		]);

		Province::create([
			'id'=>140,
			'country_id'=>50,
			'name'=>'Buryatiya'
		]);

		Province::create([
			'id'=>141,
			'country_id'=>50,
			'name'=>'Vladimirskaya obl.'
		]);

		Province::create([
			'id'=>142,
			'country_id'=>50,
			'name'=>'Volgogradskaya obl.'
		]);

		Province::create([
			'id'=>143,
			'country_id'=>50,
			'name'=>'Vologodskaya obl.'
		]);

		Province::create([
			'id'=>144,
			'country_id'=>50,
			'name'=>'Voronezhskaya obl.'
		]);

		Province::create([
			'id'=>145,
			'country_id'=>50,
			'name'=>'Nizhegorodskaya obl.'
		]);

		Province::create([
			'id'=>146,
			'country_id'=>50,
			'name'=>'Dagestan'
		]);

		Province::create([
			'id'=>147,
			'country_id'=>50,
			'name'=>'Evreiskaya obl.'
		]);

		Province::create([
			'id'=>148,
			'country_id'=>50,
			'name'=>'Ivanovskaya obl.'
		]);

		Province::create([
			'id'=>149,
			'country_id'=>50,
			'name'=>'Irkutskaya obl.'
		]);

		Province::create([
			'id'=>150,
			'country_id'=>50,
			'name'=>'Kabardino-Balkariya'
		]);

		Province::create([
			'id'=>151,
			'country_id'=>50,
			'name'=>'Kaliningradskaya obl.'
		]);

		Province::create([
			'id'=>152,
			'country_id'=>50,
			'name'=>'Tverskaya obl.'
		]);

		Province::create([
			'id'=>153,
			'country_id'=>50,
			'name'=>'Kalmykiya'
		]);

		Province::create([
			'id'=>154,
			'country_id'=>50,
			'name'=>'Kaluzhskaya obl.'
		]);

		Province::create([
			'id'=>155,
			'country_id'=>50,
			'name'=>'Kamchatskaya obl.'
		]);

		Province::create([
			'id'=>156,
			'country_id'=>50,
			'name'=>'Kareliya'
		]);

		Province::create([
			'id'=>157,
			'country_id'=>50,
			'name'=>'Kemerovskaya obl.'
		]);

		Province::create([
			'id'=>158,
			'country_id'=>50,
			'name'=>'Kirovskaya obl.'
		]);

		Province::create([
			'id'=>159,
			'country_id'=>50,
			'name'=>'Komi'
		]);

		Province::create([
			'id'=>160,
			'country_id'=>50,
			'name'=>'Kostromskaya obl.'
		]);

		Province::create([
			'id'=>161,
			'country_id'=>50,
			'name'=>'Krasnodarskii krai'
		]);

		Province::create([
			'id'=>162,
			'country_id'=>50,
			'name'=>'Krasnoyarskii krai'
		]);

		Province::create([
			'id'=>163,
			'country_id'=>50,
			'name'=>'Kurganskaya obl.'
		]);

		Province::create([
			'id'=>164,
			'country_id'=>50,
			'name'=>'Kurskaya obl.'
		]);

		Province::create([
			'id'=>165,
			'country_id'=>50,
			'name'=>'Lipetskaya obl.'
		]);

		Province::create([
			'id'=>166,
			'country_id'=>50,
			'name'=>'Magadanskaya obl.'
		]);

		Province::create([
			'id'=>167,
			'country_id'=>50,
			'name'=>'Marii El'
		]);

		Province::create([
			'id'=>168,
			'country_id'=>50,
			'name'=>'Mordoviya'
		]);

		Province::create([
			'id'=>169,
			'country_id'=>50,
			'name'=>'Moscow Y Moscow Region'
		]);

		Province::create([
			'id'=>170,
			'country_id'=>50,
			'name'=>'Murmanskaya obl.'
		]);

		Province::create([
			'id'=>171,
			'country_id'=>50,
			'name'=>'Novgorodskaya obl.'
		]);

		Province::create([
			'id'=>172,
			'country_id'=>50,
			'name'=>'Novosibirskaya obl.'
		]);

		Province::create([
			'id'=>173,
			'country_id'=>50,
			'name'=>'Omskaya obl.'
		]);

		Province::create([
			'id'=>174,
			'country_id'=>50,
			'name'=>'Orenburgskaya obl.'
		]);

		Province::create([
			'id'=>175,
			'country_id'=>50,
			'name'=>'Orlovskaya obl.'
		]);

		Province::create([
			'id'=>176,
			'country_id'=>50,
			'name'=>'Penzenskaya obl.'
		]);

		Province::create([
			'id'=>177,
			'country_id'=>50,
			'name'=>'Permskiy krai'
		]);

		Province::create([
			'id'=>178,
			'country_id'=>50,
			'name'=>'Primorskii krai'
		]);

		Province::create([
			'id'=>179,
			'country_id'=>50,
			'name'=>'Pskovskaya obl.'
		]);

		Province::create([
			'id'=>180,
			'country_id'=>50,
			'name'=>'Rostovskaya obl.'
		]);

		Province::create([
			'id'=>181,
			'country_id'=>50,
			'name'=>'Ryazanskaya obl.'
		]);

		Province::create([
			'id'=>182,
			'country_id'=>50,
			'name'=>'Samarskaya obl.'
		]);

		Province::create([
			'id'=>183,
			'country_id'=>50,
			'name'=>'Saint-Petersburg and Region'
		]);

		Province::create([
			'id'=>184,
			'country_id'=>50,
			'name'=>'Saratovskaya obl.'
		]);

		Province::create([
			'id'=>185,
			'country_id'=>50,
			'name'=>'Saha (Yakutiya)'
		]);

		Province::create([
			'id'=>186,
			'country_id'=>50,
			'name'=>'Sahalin'
		]);

		Province::create([
			'id'=>187,
			'country_id'=>50,
			'name'=>'Sverdlovskaya obl.'
		]);

		Province::create([
			'id'=>188,
			'country_id'=>50,
			'name'=>'Severnaya Osetiya'
		]);

		Province::create([
			'id'=>189,
			'country_id'=>50,
			'name'=>'Smolenskaya obl.'
		]);

		Province::create([
			'id'=>190,
			'country_id'=>50,
			'name'=>'Stavropolskii krai'
		]);

		Province::create([
			'id'=>191,
			'country_id'=>50,
			'name'=>'Tambovskaya obl.'
		]);

		Province::create([
			'id'=>192,
			'country_id'=>50,
			'name'=>'Tatarstan'
		]);

		Province::create([
			'id'=>193,
			'country_id'=>50,
			'name'=>'Tomskaya obl.'
		]);

		Province::create([
			'id'=>195,
			'country_id'=>50,
			'name'=>'Tulskaya obl.'
		]);

		Province::create([
			'id'=>196,
			'country_id'=>50,
			'name'=>'Tyumenskaya obl. i Hanty-Mansiiskii AO'
		]);

		Province::create([
			'id'=>197,
			'country_id'=>50,
			'name'=>'Udmurtiya'
		]);

		Province::create([
			'id'=>198,
			'country_id'=>50,
			'name'=>'Ulyanovskaya obl.'
		]);

		Province::create([
			'id'=>199,
			'country_id'=>50,
			'name'=>'Uralskaya obl.'
		]);

		Province::create([
			'id'=>200,
			'country_id'=>50,
			'name'=>'Habarovskii krai'
		]);

		Province::create([
			'id'=>201,
			'country_id'=>50,
			'name'=>'Chelyabinskaya obl.'
		]);

		Province::create([
			'id'=>202,
			'country_id'=>50,
			'name'=>'Checheno-Ingushetiya'
		]);

		Province::create([
			'id'=>203,
			'country_id'=>50,
			'name'=>'Chitinskaya obl.'
		]);

		Province::create([
			'id'=>204,
			'country_id'=>50,
			'name'=>'Chuvashiya'
		]);

		Province::create([
			'id'=>205,
			'country_id'=>50,
			'name'=>'Yaroslavskaya obl.'
		]);

		Province::create([
			'id'=>206,
			'country_id'=>51,
			'name'=>'Ahuachapán'
		]);

		Province::create([
			'id'=>207,
			'country_id'=>51,
			'name'=>'Cuscatlán'
		]);

		Province::create([
			'id'=>208,
			'country_id'=>51,
			'name'=>'La Libertad'
		]);

		Province::create([
			'id'=>209,
			'country_id'=>51,
			'name'=>'La Paz'
		]);

		Province::create([
			'id'=>210,
			'country_id'=>51,
			'name'=>'La Unión'
		]);

		Province::create([
			'id'=>211,
			'country_id'=>51,
			'name'=>'San Miguel'
		]);

		Province::create([
			'id'=>212,
			'country_id'=>51,
			'name'=>'San Salvador'
		]);

		Province::create([
			'id'=>213,
			'country_id'=>51,
			'name'=>'Santa Ana'
		]);

		Province::create([
			'id'=>214,
			'country_id'=>51,
			'name'=>'Sonsonate'
		]);

		Province::create([
			'id'=>215,
			'country_id'=>54,
			'name'=>'Paramaribo'
		]);

		Province::create([
			'id'=>216,
			'country_id'=>56,
			'name'=>'Gorno-Badakhshan Region'
		]);

		Province::create([
			'id'=>217,
			'country_id'=>56,
			'name'=>'Kuljabsk Region'
		]);

		Province::create([
			'id'=>218,
			'country_id'=>56,
			'name'=>'Kurgan-Tjube Region'
		]);

		Province::create([
			'id'=>219,
			'country_id'=>56,
			'name'=>'Sughd Region'
		]);

		Province::create([
			'id'=>220,
			'country_id'=>56,
			'name'=>'Tajikistan'
		]);

		Province::create([
			'id'=>221,
			'country_id'=>57,
			'name'=>'Ashgabat Region'
		]);

		Province::create([
			'id'=>222,
			'country_id'=>57,
			'name'=>'Krasnovodsk Region'
		]);

		Province::create([
			'id'=>223,
			'country_id'=>57,
			'name'=>'Mary Region'
		]);

		Province::create([
			'id'=>224,
			'country_id'=>57,
			'name'=>'Tashauz Region'
		]);

		Province::create([
			'id'=>225,
			'country_id'=>57,
			'name'=>'Chardzhou Region'
		]);

		Province::create([
			'id'=>226,
			'country_id'=>58,
			'name'=>'Grand Turk'
		]);

		Province::create([
			'id'=>227,
			'country_id'=>59,
			'name'=>'Bartin'
		]);

		Province::create([
			'id'=>228,
			'country_id'=>59,
			'name'=>'Bayburt'
		]);

		Province::create([
			'id'=>229,
			'country_id'=>59,
			'name'=>'Karabuk'
		]);

		Province::create([
			'id'=>230,
			'country_id'=>59,
			'name'=>'Adana'
		]);

		Province::create([
			'id'=>231,
			'country_id'=>59,
			'name'=>'Aydin'
		]);

		Province::create([
			'id'=>232,
			'country_id'=>59,
			'name'=>'Amasya'
		]);

		Province::create([
			'id'=>233,
			'country_id'=>59,
			'name'=>'Ankara'
		]);

		Province::create([
			'id'=>234,
			'country_id'=>59,
			'name'=>'Antalya'
		]);

		Province::create([
			'id'=>235,
			'country_id'=>59,
			'name'=>'Artvin'
		]);

		Province::create([
			'id'=>236,
			'country_id'=>59,
			'name'=>'Afion'
		]);

		Province::create([
			'id'=>237,
			'country_id'=>59,
			'name'=>'Balikesir'
		]);

		Province::create([
			'id'=>238,
			'country_id'=>59,
			'name'=>'Bilecik'
		]);

		Province::create([
			'id'=>239,
			'country_id'=>59,
			'name'=>'Bursa'
		]);

		Province::create([
			'id'=>240,
			'country_id'=>59,
			'name'=>'Gaziantep'
		]);

		Province::create([
			'id'=>241,
			'country_id'=>59,
			'name'=>'Denizli'
		]);

		Province::create([
			'id'=>242,
			'country_id'=>59,
			'name'=>'Izmir'
		]);

		Province::create([
			'id'=>243,
			'country_id'=>59,
			'name'=>'Isparta'
		]);

		Province::create([
			'id'=>244,
			'country_id'=>59,
			'name'=>'Icel'
		]);

		Province::create([
			'id'=>245,
			'country_id'=>59,
			'name'=>'Kayseri'
		]);

		Province::create([
			'id'=>246,
			'country_id'=>59,
			'name'=>'Kars'
		]);

		Province::create([
			'id'=>247,
			'country_id'=>59,
			'name'=>'Kodjaeli'
		]);

		Province::create([
			'id'=>248,
			'country_id'=>59,
			'name'=>'Konya'
		]);

		Province::create([
			'id'=>249,
			'country_id'=>59,
			'name'=>'Kirklareli'
		]);

		Province::create([
			'id'=>250,
			'country_id'=>59,
			'name'=>'Kutahya'
		]);

		Province::create([
			'id'=>251,
			'country_id'=>59,
			'name'=>'Malatya'
		]);

		Province::create([
			'id'=>252,
			'country_id'=>59,
			'name'=>'Manisa'
		]);

		Province::create([
			'id'=>253,
			'country_id'=>59,
			'name'=>'Sakarya'
		]);

		Province::create([
			'id'=>254,
			'country_id'=>59,
			'name'=>'Samsun'
		]);

		Province::create([
			'id'=>255,
			'country_id'=>59,
			'name'=>'Sivas'
		]);

		Province::create([
			'id'=>256,
			'country_id'=>59,
			'name'=>'Istanbul'
		]);

		Province::create([
			'id'=>257,
			'country_id'=>59,
			'name'=>'Trabzon'
		]);

		Province::create([
			'id'=>258,
			'country_id'=>59,
			'name'=>'Corum'
		]);

		Province::create([
			'id'=>259,
			'country_id'=>59,
			'name'=>'Edirne'
		]);

		Province::create([
			'id'=>260,
			'country_id'=>59,
			'name'=>'Elazig'
		]);

		Province::create([
			'id'=>261,
			'country_id'=>59,
			'name'=>'Erzincan'
		]);

		Province::create([
			'id'=>262,
			'country_id'=>59,
			'name'=>'Erzurum'
		]);

		Province::create([
			'id'=>263,
			'country_id'=>59,
			'name'=>'Eskisehir'
		]);

		Province::create([
			'id'=>264,
			'country_id'=>60,
			'name'=>'Jinja'
		]);

		Province::create([
			'id'=>265,
			'country_id'=>60,
			'name'=>'Kampala'
		]);

		Province::create([
			'id'=>266,
			'country_id'=>61,
			'name'=>'Andijon Region'
		]);

		Province::create([
			'id'=>267,
			'country_id'=>61,
			'name'=>'Buxoro Region'
		]);

		Province::create([
			'id'=>268,
			'country_id'=>61,
			'name'=>'Jizzac Region'
		]);

		Province::create([
			'id'=>269,
			'country_id'=>61,
			'name'=>'Qaraqalpaqstan'
		]);

		Province::create([
			'id'=>270,
			'country_id'=>61,
			'name'=>'Qashqadaryo Region'
		]);

		Province::create([
			'id'=>271,
			'country_id'=>61,
			'name'=>'Navoiy Region'
		]);

		Province::create([
			'id'=>272,
			'country_id'=>61,
			'name'=>'Namangan Region'
		]);

		Province::create([
			'id'=>273,
			'country_id'=>61,
			'name'=>'Samarqand Region'
		]);

		Province::create([
			'id'=>274,
			'country_id'=>61,
			'name'=>'Surxondaryo Region'
		]);

		Province::create([
			'id'=>275,
			'country_id'=>61,
			'name'=>'Sirdaryo Region'
		]);

		Province::create([
			'id'=>276,
			'country_id'=>61,
			'name'=>'Tashkent Region'
		]);

		Province::create([
			'id'=>277,
			'country_id'=>61,
			'name'=>'Fergana Region'
		]);

		Province::create([
			'id'=>278,
			'country_id'=>61,
			'name'=>'Xorazm Region'
		]);

		Province::create([
			'id'=>279,
			'country_id'=>62,
			'name'=>'Vinnitskaya obl.'
		]);

		Province::create([
			'id'=>280,
			'country_id'=>62,
			'name'=>'Volynskaya obl.'
		]);

		Province::create([
			'id'=>281,
			'country_id'=>62,
			'name'=>'Dnepropetrovskaya obl.'
		]);

		Province::create([
			'id'=>282,
			'country_id'=>62,
			'name'=>'Donetskaya obl.'
		]);

		Province::create([
			'id'=>283,
			'country_id'=>62,
			'name'=>'Zhitomirskaya obl.'
		]);

		Province::create([
			'id'=>284,
			'country_id'=>62,
			'name'=>'Zakarpatskaya obl.'
		]);

		Province::create([
			'id'=>285,
			'country_id'=>62,
			'name'=>'Zaporozhskaya obl.'
		]);

		Province::create([
			'id'=>286,
			'country_id'=>62,
			'name'=>'Ivano-Frankovskaya obl.'
		]);

		Province::create([
			'id'=>287,
			'country_id'=>62,
			'name'=>'Kievskaya obl.'
		]);

		Province::create([
			'id'=>288,
			'country_id'=>62,
			'name'=>'Kirovogradskaya obl.'
		]);

		Province::create([
			'id'=>289,
			'country_id'=>62,
			'name'=>'Krymskaya obl.'
		]);

		Province::create([
			'id'=>290,
			'country_id'=>62,
			'name'=>'Luganskaya obl.'
		]);

		Province::create([
			'id'=>291,
			'country_id'=>62,
			'name'=>'Lvovskaya obl.'
		]);

		Province::create([
			'id'=>292,
			'country_id'=>62,
			'name'=>'Nikolaevskaya obl.'
		]);

		Province::create([
			'id'=>293,
			'country_id'=>62,
			'name'=>'Odesskaya obl.'
		]);

		Province::create([
			'id'=>294,
			'country_id'=>62,
			'name'=>'Poltavskaya obl.'
		]);

		Province::create([
			'id'=>295,
			'country_id'=>62,
			'name'=>'Rovenskaya obl.'
		]);

		Province::create([
			'id'=>296,
			'country_id'=>62,
			'name'=>'Sumskaya obl.'
		]);

		Province::create([
			'id'=>297,
			'country_id'=>62,
			'name'=>'Ternopolskaya obl.'
		]);

		Province::create([
			'id'=>298,
			'country_id'=>62,
			'name'=>'Harkovskaya obl.'
		]);

		Province::create([
			'id'=>299,
			'country_id'=>62,
			'name'=>'Hersonskaya obl.'
		]);

		Province::create([
			'id'=>300,
			'country_id'=>62,
			'name'=>'Hmelnitskaya obl.'
		]);

		Province::create([
			'id'=>301,
			'country_id'=>62,
			'name'=>'Cherkasskaya obl.'
		]);

		Province::create([
			'id'=>302,
			'country_id'=>62,
			'name'=>'Chernigovskaya obl.'
		]);

		Province::create([
			'id'=>303,
			'country_id'=>62,
			'name'=>'Chernovitskaya obl.'
		]);

		Province::create([
			'id'=>304,
			'country_id'=>68,
			'name'=>'Estoniya'
		]);

		Province::create([
			'id'=>305,
			'country_id'=>69,
			'name'=>'Cheju'
		]);

		Province::create([
			'id'=>306,
			'country_id'=>69,
			'name'=>'Chollabuk'
		]);

		Province::create([
			'id'=>307,
			'country_id'=>69,
			'name'=>'Chollanam'
		]);

		Province::create([
			'id'=>308,
			'country_id'=>69,
			'name'=>'Chungcheongbuk'
		]);

		Province::create([
			'id'=>309,
			'country_id'=>69,
			'name'=>'Chungcheongnam'
		]);

		Province::create([
			'id'=>310,
			'country_id'=>69,
			'name'=>'Incheon'
		]);

		Province::create([
			'id'=>311,
			'country_id'=>69,
			'name'=>'Kangweon'
		]);

		Province::create([
			'id'=>312,
			'country_id'=>69,
			'name'=>'Kwangju'
		]);

		Province::create([
			'id'=>313,
			'country_id'=>69,
			'name'=>'Kyeonggi'
		]);

		Province::create([
			'id'=>314,
			'country_id'=>69,
			'name'=>'Kyeongsangbuk'
		]);

		Province::create([
			'id'=>315,
			'country_id'=>69,
			'name'=>'Kyeongsangnam'
		]);

		Province::create([
			'id'=>316,
			'country_id'=>69,
			'name'=>'Pusan'
		]);

		Province::create([
			'id'=>317,
			'country_id'=>69,
			'name'=>'Seoul'
		]);

		Province::create([
			'id'=>318,
			'country_id'=>69,
			'name'=>'Taegu'
		]);

		Province::create([
			'id'=>319,
			'country_id'=>69,
			'name'=>'Taejeon'
		]);

		Province::create([
			'id'=>320,
			'country_id'=>69,
			'name'=>'Ulsan'
		]);

		Province::create([
			'id'=>321,
			'country_id'=>70,
			'name'=>'Aichi'
		]);

		Province::create([
			'id'=>322,
			'country_id'=>70,
			'name'=>'Akita'
		]);

		Province::create([
			'id'=>323,
			'country_id'=>70,
			'name'=>'Aomori'
		]);

		Province::create([
			'id'=>324,
			'country_id'=>70,
			'name'=>'Wakayama'
		]);

		Province::create([
			'id'=>325,
			'country_id'=>70,
			'name'=>'Gifu'
		]);

		Province::create([
			'id'=>326,
			'country_id'=>70,
			'name'=>'Gunma'
		]);

		Province::create([
			'id'=>327,
			'country_id'=>70,
			'name'=>'Ibaraki'
		]);

		Province::create([
			'id'=>328,
			'country_id'=>70,
			'name'=>'Iwate'
		]);

		Province::create([
			'id'=>329,
			'country_id'=>70,
			'name'=>'Ishikawa'
		]);

		Province::create([
			'id'=>330,
			'country_id'=>70,
			'name'=>'Kagawa'
		]);

		Province::create([
			'id'=>331,
			'country_id'=>70,
			'name'=>'Kagoshima'
		]);

		Province::create([
			'id'=>332,
			'country_id'=>70,
			'name'=>'Kanagawa'
		]);

		Province::create([
			'id'=>333,
			'country_id'=>70,
			'name'=>'Kyoto'
		]);

		Province::create([
			'id'=>334,
			'country_id'=>70,
			'name'=>'Kochi'
		]);

		Province::create([
			'id'=>335,
			'country_id'=>70,
			'name'=>'Kumamoto'
		]);

		Province::create([
			'id'=>336,
			'country_id'=>70,
			'name'=>'Mie'
		]);

		Province::create([
			'id'=>337,
			'country_id'=>70,
			'name'=>'Miyagi'
		]);

		Province::create([
			'id'=>338,
			'country_id'=>70,
			'name'=>'Miyazaki'
		]);

		Province::create([
			'id'=>339,
			'country_id'=>70,
			'name'=>'Nagano'
		]);

		Province::create([
			'id'=>340,
			'country_id'=>70,
			'name'=>'Nagasaki'
		]);

		Province::create([
			'id'=>341,
			'country_id'=>70,
			'name'=>'Nara'
		]);

		Province::create([
			'id'=>342,
			'country_id'=>70,
			'name'=>'Niigata'
		]);

		Province::create([
			'id'=>343,
			'country_id'=>70,
			'name'=>'Okayama'
		]);

		Province::create([
			'id'=>344,
			'country_id'=>70,
			'name'=>'Okinawa'
		]);

		Province::create([
			'id'=>345,
			'country_id'=>70,
			'name'=>'Osaka'
		]);

		Province::create([
			'id'=>346,
			'country_id'=>70,
			'name'=>'Saga'
		]);

		Province::create([
			'id'=>347,
			'country_id'=>70,
			'name'=>'Saitama'
		]);

		Province::create([
			'id'=>348,
			'country_id'=>70,
			'name'=>'Shiga'
		]);

		Province::create([
			'id'=>349,
			'country_id'=>70,
			'name'=>'Shizuoka'
		]);

		Province::create([
			'id'=>350,
			'country_id'=>70,
			'name'=>'Shimane'
		]);

		Province::create([
			'id'=>351,
			'country_id'=>70,
			'name'=>'Tiba'
		]);

		Province::create([
			'id'=>352,
			'country_id'=>70,
			'name'=>'Tokyo'
		]);

		Province::create([
			'id'=>353,
			'country_id'=>70,
			'name'=>'Tokushima'
		]);

		Province::create([
			'id'=>354,
			'country_id'=>70,
			'name'=>'Tochigi'
		]);

		Province::create([
			'id'=>355,
			'country_id'=>70,
			'name'=>'Tottori'
		]);

		Province::create([
			'id'=>356,
			'country_id'=>70,
			'name'=>'Toyama'
		]);

		Province::create([
			'id'=>357,
			'country_id'=>70,
			'name'=>'Fukui'
		]);

		Province::create([
			'id'=>358,
			'country_id'=>70,
			'name'=>'Fukuoka'
		]);

		Province::create([
			'id'=>359,
			'country_id'=>70,
			'name'=>'Fukushima'
		]);

		Province::create([
			'id'=>360,
			'country_id'=>70,
			'name'=>'Hiroshima'
		]);

		Province::create([
			'id'=>361,
			'country_id'=>70,
			'name'=>'Hokkaido'
		]);

		Province::create([
			'id'=>362,
			'country_id'=>70,
			'name'=>'Hyogo'
		]);

		Province::create([
			'id'=>363,
			'country_id'=>70,
			'name'=>'Yoshimi'
		]);

		Province::create([
			'id'=>364,
			'country_id'=>70,
			'name'=>'Yamagata'
		]);

		Province::create([
			'id'=>365,
			'country_id'=>70,
			'name'=>'Yamaguchi'
		]);

		Province::create([
			'id'=>366,
			'country_id'=>70,
			'name'=>'Yamanashi'
		]);

		Province::create([
			'id'=>368,
			'country_id'=>73,
			'name'=>'Hong Kong'
		]);

		Province::create([
			'id'=>369,
			'country_id'=>74,
			'name'=>'Indonesia'
		]);

		Province::create([
			'id'=>370,
			'country_id'=>75,
			'name'=>'Jordan'
		]);

		Province::create([
			'id'=>371,
			'country_id'=>76,
			'name'=>'Malaysia'
		]);

		Province::create([
			'id'=>372,
			'country_id'=>77,
			'name'=>'Singapore'
		]);

		Province::create([
			'id'=>373,
			'country_id'=>78,
			'name'=>'Taiwan'
		]);

		Province::create([
			'id'=>374,
			'country_id'=>30,
			'name'=>'Kazahstan'
		]);

		Province::create([
			'id'=>375,
			'country_id'=>62,
			'name'=>'Ukraina'
		]);

		Province::create([
			'id'=>376,
			'country_id'=>25,
			'name'=>'India'
		]);

		Province::create([
			'id'=>377,
			'country_id'=>23,
			'name'=>'Egypt'
		]);

		Province::create([
			'id'=>378,
			'country_id'=>106,
			'name'=>'Damascus'
		]);

		Province::create([
			'id'=>379,
			'country_id'=>131,
			'name'=>'Isle of Man'
		]);

		Province::create([
			'id'=>380,
			'country_id'=>30,
			'name'=>'Zapadno-Kazahstanskaya obl.'
		]);

		Province::create([
			'id'=>381,
			'country_id'=>50,
			'name'=>'Adygeya'
		]);

		Province::create([
			'id'=>382,
			'country_id'=>50,
			'name'=>'Hakasiya'
		]);

		Province::create([
			'id'=>383,
			'country_id'=>93,
			'name'=>'Dubai'
		]);

		Province::create([
			'id'=>384,
			'country_id'=>50,
			'name'=>'Chukotskii AO'
		]);

		Province::create([
			'id'=>385,
			'country_id'=>99,
			'name'=>'Beirut'
		]);

		Province::create([
			'id'=>386,
			'country_id'=>137,
			'name'=>'Tegucigalpa'
		]);

		Province::create([
			'id'=>387,
			'country_id'=>138,
			'name'=>'Santo Domingo'
		]);

		Province::create([
			'id'=>388,
			'country_id'=>139,
			'name'=>'Ulan Bator'
		]);

		Province::create([
			'id'=>389,
			'country_id'=>23,
			'name'=>'Sinai'
		]);

		Province::create([
			'id'=>390,
			'country_id'=>140,
			'name'=>'Baghdad'
		]);

		Province::create([
			'id'=>391,
			'country_id'=>140,
			'name'=>'Basra'
		]);

		Province::create([
			'id'=>392,
			'country_id'=>140,
			'name'=>'Mosul'
		]);

		Province::create([
			'id'=>393,
			'country_id'=>141,
			'name'=>'Johannesburg'
		]);

		Province::create([
			'id'=>394,
			'country_id'=>104,
			'name'=>'Morocco'
		]);

		Province::create([
			'id'=>395,
			'country_id'=>104,
			'name'=>'Tangier'
		]);

		Province::create([
			'id'=>396,
			'country_id'=>50,
			'name'=>'Yamalo-Nenetskii AO'
		]);

		Province::create([
			'id'=>397,
			'country_id'=>122,
			'name'=>'Tunisia'
		]);

		Province::create([
			'id'=>398,
			'country_id'=>92,
			'name'=>'Thailand'
		]);

		Province::create([
			'id'=>399,
			'country_id'=>117,
			'name'=>'Mozambique'
		]);

		Province::create([
			'id'=>400,
			'country_id'=>84,
			'name'=>'Korea'
		]);

		Province::create([
			'id'=>401,
			'country_id'=>87,
			'name'=>'Pakistan'
		]);

		Province::create([
			'id'=>402,
			'country_id'=>142,
			'name'=>'Aruba'
		]);

		Province::create([
			'id'=>403,
			'country_id'=>80,
			'name'=>'Bahamas'
		]);

		Province::create([
			'id'=>404,
			'country_id'=>69,
			'name'=>'South Korea'
		]);

		Province::create([
			'id'=>405,
			'country_id'=>132,
			'name'=>'Jamaica'
		]);

		Province::create([
			'id'=>406,
			'country_id'=>93,
			'name'=>'Sharjah'
		]);

		Province::create([
			'id'=>407,
			'country_id'=>93,
			'name'=>'Abu Dhabi'
		]);

		Province::create([
			'id'=>409,
			'country_id'=>24,
			'name'=>'Ramat Hagolan'
		]);

		Province::create([
			'id'=>410,
			'country_id'=>115,
			'name'=>'Nigeria'
		]);

		Province::create([
			'id'=>411,
			'country_id'=>64,
			'name'=>'Ain'
		]);

		Province::create([
			'id'=>412,
			'country_id'=>64,
			'name'=>'Haute-Savoie'
		]);

		Province::create([
			'id'=>413,
			'country_id'=>64,
			'name'=>'Aisne'
		]);

		Province::create([
			'id'=>414,
			'country_id'=>64,
			'name'=>'Allier'
		]);

		Province::create([
			'id'=>415,
			'country_id'=>64,
			'name'=>'Alpes-de-Haute-Provence'
		]);

		Province::create([
			'id'=>416,
			'country_id'=>64,
			'name'=>'Hautes-Alpes'
		]);

		Province::create([
			'id'=>417,
			'country_id'=>64,
			'name'=>'Alpes-Maritimes'
		]);

		Province::create([
			'id'=>418,
			'country_id'=>64,
			'name'=>'Ardèche'
		]);

		Province::create([
			'id'=>419,
			'country_id'=>64,
			'name'=>'Ardennes'
		]);

		Province::create([
			'id'=>420,
			'country_id'=>64,
			'name'=>'Ariège'
		]);

		Province::create([
			'id'=>421,
			'country_id'=>64,
			'name'=>'Aube'
		]);

		Province::create([
			'id'=>422,
			'country_id'=>64,
			'name'=>'Aude'
		]);

		Province::create([
			'id'=>423,
			'country_id'=>64,
			'name'=>'Aveyron'
		]);

		Province::create([
			'id'=>424,
			'country_id'=>64,
			'name'=>'Bouches-du-Rhône'
		]);

		Province::create([
			'id'=>425,
			'country_id'=>64,
			'name'=>'Calvados'
		]);

		Province::create([
			'id'=>426,
			'country_id'=>64,
			'name'=>'Cantal'
		]);

		Province::create([
			'id'=>427,
			'country_id'=>64,
			'name'=>'Charente'
		]);

		Province::create([
			'id'=>428,
			'country_id'=>64,
			'name'=>'Charente Maritime'
		]);

		Province::create([
			'id'=>429,
			'country_id'=>64,
			'name'=>'Cher'
		]);

		Province::create([
			'id'=>430,
			'country_id'=>64,
			'name'=>'Corrèze'
		]);

		Province::create([
			'id'=>431,
			'country_id'=>64,
			'name'=>'Dordogne'
		]);

		Province::create([
			'id'=>432,
			'country_id'=>64,
			'name'=>'Corse'
		]);

		Province::create([
			'id'=>433,
			'country_id'=>64,
			'name'=>'Côte dOr'
		]);

		Province::create([
			'id'=>434,
			'country_id'=>64,
			'name'=>'Saône et Loire'
		]);

		Province::create([
			'id'=>435,
			'country_id'=>64,
			'name'=>'Côtes dArmor'
		]);

		Province::create([
			'id'=>436,
			'country_id'=>64,
			'name'=>'Creuse'
		]);

		Province::create([
			'id'=>437,
			'country_id'=>64,
			'name'=>'Doubs'
		]);

		Province::create([
			'id'=>438,
			'country_id'=>64,
			'name'=>'Drôme'
		]);

		Province::create([
			'id'=>439,
			'country_id'=>64,
			'name'=>'Eure'
		]);

		Province::create([
			'id'=>440,
			'country_id'=>64,
			'name'=>'Eure-et-Loire'
		]);

		Province::create([
			'id'=>441,
			'country_id'=>64,
			'name'=>'Finistère'
		]);

		Province::create([
			'id'=>442,
			'country_id'=>64,
			'name'=>'Gard'
		]);

		Province::create([
			'id'=>443,
			'country_id'=>64,
			'name'=>'Haute-Garonne'
		]);

		Province::create([
			'id'=>444,
			'country_id'=>64,
			'name'=>'Gers'
		]);

		Province::create([
			'id'=>445,
			'country_id'=>64,
			'name'=>'Gironde'
		]);

		Province::create([
			'id'=>446,
			'country_id'=>64,
			'name'=>'Hérault'
		]);

		Province::create([
			'id'=>447,
			'country_id'=>64,
			'name'=>'Ille et Vilaine'
		]);

		Province::create([
			'id'=>448,
			'country_id'=>64,
			'name'=>'Indre'
		]);

		Province::create([
			'id'=>449,
			'country_id'=>64,
			'name'=>'Indre-et-Loire'
		]);

		Province::create([
			'id'=>450,
			'country_id'=>64,
			'name'=>'Isère'
		]);

		Province::create([
			'id'=>451,
			'country_id'=>64,
			'name'=>'Jura'
		]);

		Province::create([
			'id'=>452,
			'country_id'=>64,
			'name'=>'Landes'
		]);

		Province::create([
			'id'=>453,
			'country_id'=>64,
			'name'=>'Loir-et-Cher'
		]);

		Province::create([
			'id'=>454,
			'country_id'=>64,
			'name'=>'Loire'
		]);

		Province::create([
			'id'=>455,
			'country_id'=>64,
			'name'=>'Rhône'
		]);

		Province::create([
			'id'=>456,
			'country_id'=>64,
			'name'=>'Haute-Loire'
		]);

		Province::create([
			'id'=>457,
			'country_id'=>64,
			'name'=>'Loire Atlantique'
		]);

		Province::create([
			'id'=>458,
			'country_id'=>64,
			'name'=>'Loiret'
		]);

		Province::create([
			'id'=>459,
			'country_id'=>64,
			'name'=>'Lot'
		]);

		Province::create([
			'id'=>460,
			'country_id'=>64,
			'name'=>'Lot-et-Garonne'
		]);

		Province::create([
			'id'=>461,
			'country_id'=>64,
			'name'=>'Lozère'
		]);

		Province::create([
			'id'=>462,
			'country_id'=>64,
			'name'=>'Maine et Loire'
		]);

		Province::create([
			'id'=>463,
			'country_id'=>64,
			'name'=>'Manche'
		]);

		Province::create([
			'id'=>464,
			'country_id'=>64,
			'name'=>'Marne'
		]);

		Province::create([
			'id'=>465,
			'country_id'=>64,
			'name'=>'Haute-Marne'
		]);

		Province::create([
			'id'=>466,
			'country_id'=>64,
			'name'=>'Mayenne'
		]);

		Province::create([
			'id'=>467,
			'country_id'=>64,
			'name'=>'Meurthe-et-Moselle'
		]);

		Province::create([
			'id'=>468,
			'country_id'=>64,
			'name'=>'Meuse'
		]);

		Province::create([
			'id'=>469,
			'country_id'=>64,
			'name'=>'Morbihan'
		]);

		Province::create([
			'id'=>470,
			'country_id'=>64,
			'name'=>'Moselle'
		]);

		Province::create([
			'id'=>471,
			'country_id'=>64,
			'name'=>'Nièvre'
		]);

		Province::create([
			'id'=>472,
			'country_id'=>64,
			'name'=>'Nord'
		]);

		Province::create([
			'id'=>473,
			'country_id'=>64,
			'name'=>'Oise'
		]);

		Province::create([
			'id'=>474,
			'country_id'=>64,
			'name'=>'Orne'
		]);

		Province::create([
			'id'=>475,
			'country_id'=>64,
			'name'=>'Pas-de-Calais'
		]);

		Province::create([
			'id'=>476,
			'country_id'=>64,
			'name'=>'Puy-de-Dôme'
		]);

		Province::create([
			'id'=>477,
			'country_id'=>64,
			'name'=>'Pyrénées-Atlantiques'
		]);

		Province::create([
			'id'=>478,
			'country_id'=>64,
			'name'=>'Hautes-Pyrénées'
		]);

		Province::create([
			'id'=>479,
			'country_id'=>64,
			'name'=>'Pyrénées-Orientales'
		]);

		Province::create([
			'id'=>480,
			'country_id'=>64,
			'name'=>'Bas Rhin'
		]);

		Province::create([
			'id'=>481,
			'country_id'=>64,
			'name'=>'Haut Rhin'
		]);

		Province::create([
			'id'=>482,
			'country_id'=>64,
			'name'=>'Haute-Saône'
		]);

		Province::create([
			'id'=>483,
			'country_id'=>64,
			'name'=>'Sarthe'
		]);

		Province::create([
			'id'=>484,
			'country_id'=>64,
			'name'=>'Savoie'
		]);

		Province::create([
			'id'=>485,
			'country_id'=>64,
			'name'=>'Paris'
		]);

		Province::create([
			'id'=>486,
			'country_id'=>64,
			'name'=>'Seine-Maritime'
		]);

		Province::create([
			'id'=>487,
			'country_id'=>64,
			'name'=>'Seine-et-Marne'
		]);

		Province::create([
			'id'=>488,
			'country_id'=>64,
			'name'=>'Yvelines'
		]);

		Province::create([
			'id'=>489,
			'country_id'=>64,
			'name'=>'Deux-Sèvres'
		]);

		Province::create([
			'id'=>490,
			'country_id'=>64,
			'name'=>'Somme'
		]);

		Province::create([
			'id'=>491,
			'country_id'=>64,
			'name'=>'Tarn'
		]);

		Province::create([
			'id'=>492,
			'country_id'=>64,
			'name'=>'Tarn-et-Garonne'
		]);

		Province::create([
			'id'=>493,
			'country_id'=>64,
			'name'=>'Var'
		]);

		Province::create([
			'id'=>494,
			'country_id'=>64,
			'name'=>'Vaucluse'
		]);

		Province::create([
			'id'=>495,
			'country_id'=>64,
			'name'=>'Vendée'
		]);

		Province::create([
			'id'=>496,
			'country_id'=>64,
			'name'=>'Vienne'
		]);

		Province::create([
			'id'=>497,
			'country_id'=>64,
			'name'=>'Haute-Vienne'
		]);

		Province::create([
			'id'=>498,
			'country_id'=>64,
			'name'=>'Vosges'
		]);

		Province::create([
			'id'=>499,
			'country_id'=>64,
			'name'=>'Yonne'
		]);

		Province::create([
			'id'=>500,
			'country_id'=>64,
			'name'=>'Territoire de Belfort'
		]);

		Province::create([
			'id'=>501,
			'country_id'=>64,
			'name'=>'Essonne'
		]);

		Province::create([
			'id'=>502,
			'country_id'=>64,
			'name'=>'Hauts-de-Seine'
		]);

		Province::create([
			'id'=>503,
			'country_id'=>64,
			'name'=>'Seine-Saint-Denis'
		]);

		Province::create([
			'id'=>504,
			'country_id'=>64,
			'name'=>'Val-de-Marne'
		]);

		Province::create([
			'id'=>505,
			'country_id'=>64,
			'name'=>'Val-dOise'
		]);

		Province::create([
			'id'=>506,
			'country_id'=>29,
			'name'=>'Piemonte - Torino'
		]);

		Province::create([
			'id'=>507,
			'country_id'=>29,
			'name'=>'Piemonte - Alessandria'
		]);

		Province::create([
			'id'=>508,
			'country_id'=>29,
			'name'=>'Piemonte - Asti'
		]);

		Province::create([
			'id'=>509,
			'country_id'=>29,
			'name'=>'Piemonte - Biella'
		]);

		Province::create([
			'id'=>510,
			'country_id'=>29,
			'name'=>'Piemonte - Cuneo'
		]);

		Province::create([
			'id'=>511,
			'country_id'=>29,
			'name'=>'Piemonte - Novara'
		]);

		Province::create([
			'id'=>512,
			'country_id'=>29,
			'name'=>'Piemonte - Verbania'
		]);

		Province::create([
			'id'=>513,
			'country_id'=>29,
			'name'=>'Piemonte - Vercelli'
		]);

		Province::create([
			'id'=>514,
			'country_id'=>29,
			'name'=>'Valle dAosta - Aosta'
		]);

		Province::create([
			'id'=>515,
			'country_id'=>29,
			'name'=>'Lombardia - Milano'
		]);

		Province::create([
			'id'=>516,
			'country_id'=>29,
			'name'=>'Lombardia - Bergamo'
		]);

		Province::create([
			'id'=>517,
			'country_id'=>29,
			'name'=>'Lombardia - Brescia'
		]);

		Province::create([
			'id'=>518,
			'country_id'=>29,
			'name'=>'Lombardia - Como'
		]);

		Province::create([
			'id'=>519,
			'country_id'=>29,
			'name'=>'Lombardia - Cremona'
		]);

		Province::create([
			'id'=>520,
			'country_id'=>29,
			'name'=>'Lombardia - Lecco'
		]);

		Province::create([
			'id'=>521,
			'country_id'=>29,
			'name'=>'Lombardia - Lodi'
		]);

		Province::create([
			'id'=>522,
			'country_id'=>29,
			'name'=>'Lombardia - Mantova'
		]);

		Province::create([
			'id'=>523,
			'country_id'=>29,
			'name'=>'Lombardia - Pavia'
		]);

		Province::create([
			'id'=>524,
			'country_id'=>29,
			'name'=>'Lombardia - Sondrio'
		]);

		Province::create([
			'id'=>525,
			'country_id'=>29,
			'name'=>'Lombardia - Varese'
		]);

		Province::create([
			'id'=>526,
			'country_id'=>29,
			'name'=>'Trentino Alto Adige - Trento'
		]);

		Province::create([
			'id'=>527,
			'country_id'=>29,
			'name'=>'Trentino Alto Adige - Bolzano'
		]);

		Province::create([
			'id'=>528,
			'country_id'=>29,
			'name'=>'Veneto - Venezia'
		]);

		Province::create([
			'id'=>529,
			'country_id'=>29,
			'name'=>'Veneto - Belluno'
		]);

		Province::create([
			'id'=>530,
			'country_id'=>29,
			'name'=>'Veneto - Padova'
		]);

		Province::create([
			'id'=>531,
			'country_id'=>29,
			'name'=>'Veneto - Rovigo'
		]);

		Province::create([
			'id'=>532,
			'country_id'=>29,
			'name'=>'Veneto - Treviso'
		]);

		Province::create([
			'id'=>533,
			'country_id'=>29,
			'name'=>'Veneto - Verona'
		]);

		Province::create([
			'id'=>534,
			'country_id'=>29,
			'name'=>'Veneto - Vicenza'
		]);

		Province::create([
			'id'=>535,
			'country_id'=>29,
			'name'=>'Friuli Venezia Giulia - Trieste'
		]);

		Province::create([
			'id'=>536,
			'country_id'=>29,
			'name'=>'Friuli Venezia Giulia - Gorizia'
		]);

		Province::create([
			'id'=>537,
			'country_id'=>29,
			'name'=>'Friuli Venezia Giulia - Pordenone'
		]);

		Province::create([
			'id'=>538,
			'country_id'=>29,
			'name'=>'Friuli Venezia Giulia - Udine'
		]);

		Province::create([
			'id'=>539,
			'country_id'=>29,
			'name'=>'Liguria - Genova'
		]);

		Province::create([
			'id'=>540,
			'country_id'=>29,
			'name'=>'Liguria - Imperia'
		]);

		Province::create([
			'id'=>541,
			'country_id'=>29,
			'name'=>'Liguria - La Spezia'
		]);

		Province::create([
			'id'=>542,
			'country_id'=>29,
			'name'=>'Liguria - Savona'
		]);

		Province::create([
			'id'=>543,
			'country_id'=>29,
			'name'=>'Emilia Romagna - Bologna'
		]);

		Province::create([
			'id'=>544,
			'country_id'=>29,
			'name'=>'Emilia Romagna - Ferrara'
		]);

		Province::create([
			'id'=>545,
			'country_id'=>29,
			'name'=>'Emilia Romagna - Forlì-Cesena'
		]);

		Province::create([
			'id'=>546,
			'country_id'=>29,
			'name'=>'Emilia Romagna - Modena'
		]);

		Province::create([
			'id'=>547,
			'country_id'=>29,
			'name'=>'Emilia Romagna - Parma'
		]);

		Province::create([
			'id'=>548,
			'country_id'=>29,
			'name'=>'Emilia Romagna - Piacenza'
		]);

		Province::create([
			'id'=>549,
			'country_id'=>29,
			'name'=>'Emilia Romagna - Ravenna'
		]);

		Province::create([
			'id'=>550,
			'country_id'=>29,
			'name'=>'Emilia Romagna - Reggio Emilia'
		]);

		Province::create([
			'id'=>551,
			'country_id'=>29,
			'name'=>'Emilia Romagna - Rimini'
		]);

		Province::create([
			'id'=>552,
			'country_id'=>29,
			'name'=>'Toscana - Firenze'
		]);

		Province::create([
			'id'=>553,
			'country_id'=>29,
			'name'=>'Toscana - Arezzo'
		]);

		Province::create([
			'id'=>554,
			'country_id'=>29,
			'name'=>'Toscana - Grosseto'
		]);

		Province::create([
			'id'=>555,
			'country_id'=>29,
			'name'=>'Toscana - Livorno'
		]);

		Province::create([
			'id'=>556,
			'country_id'=>29,
			'name'=>'Toscana - Lucca'
		]);

		Province::create([
			'id'=>557,
			'country_id'=>29,
			'name'=>'Toscana - Massa Carrara'
		]);

		Province::create([
			'id'=>558,
			'country_id'=>29,
			'name'=>'Toscana - Pisa'
		]);

		Province::create([
			'id'=>559,
			'country_id'=>29,
			'name'=>'Toscana - Pistoia'
		]);

		Province::create([
			'id'=>560,
			'country_id'=>29,
			'name'=>'Toscana - Prato'
		]);

		Province::create([
			'id'=>561,
			'country_id'=>29,
			'name'=>'Toscana - Siena'
		]);

		Province::create([
			'id'=>562,
			'country_id'=>29,
			'name'=>'Umbria - Perugia'
		]);

		Province::create([
			'id'=>563,
			'country_id'=>29,
			'name'=>'Umbria - Terni'
		]);

		Province::create([
			'id'=>564,
			'country_id'=>29,
			'name'=>'Marche - Ancona'
		]);

		Province::create([
			'id'=>565,
			'country_id'=>29,
			'name'=>'Marche - Ascoli Piceno'
		]);

		Province::create([
			'id'=>566,
			'country_id'=>29,
			'name'=>'Marche - Macerata'
		]);

		Province::create([
			'id'=>567,
			'country_id'=>29,
			'name'=>'Marche - Pesaro - Urbino'
		]);

		Province::create([
			'id'=>568,
			'country_id'=>29,
			'name'=>'Lazio - Roma'
		]);

		Province::create([
			'id'=>569,
			'country_id'=>29,
			'name'=>'Lazio - Frosinone'
		]);

		Province::create([
			'id'=>570,
			'country_id'=>29,
			'name'=>'Lazio - Latina'
		]);

		Province::create([
			'id'=>571,
			'country_id'=>29,
			'name'=>'Lazio - Rieti'
		]);

		Province::create([
			'id'=>572,
			'country_id'=>29,
			'name'=>'Lazio - Viterbo'
		]);

		Province::create([
			'id'=>573,
			'country_id'=>29,
			'name'=>'Abruzzo - LAquila'
		]);

		Province::create([
			'id'=>574,
			'country_id'=>29,
			'name'=>'Abruzzo - Chieti'
		]);

		Province::create([
			'id'=>575,
			'country_id'=>29,
			'name'=>'Abruzzo - Pescara'
		]);

		Province::create([
			'id'=>576,
			'country_id'=>29,
			'name'=>'Abruzzo - Teramo'
		]);

		Province::create([
			'id'=>577,
			'country_id'=>29,
			'name'=>'Molise - Campobasso'
		]);

		Province::create([
			'id'=>578,
			'country_id'=>29,
			'name'=>'Molise - Isernia'
		]);

		Province::create([
			'id'=>579,
			'country_id'=>29,
			'name'=>'Campania - Napoli'
		]);

		Province::create([
			'id'=>580,
			'country_id'=>29,
			'name'=>'Campania - Avellino'
		]);

		Province::create([
			'id'=>581,
			'country_id'=>29,
			'name'=>'Campania - Benevento'
		]);

		Province::create([
			'id'=>582,
			'country_id'=>29,
			'name'=>'Campania - Caserta'
		]);

		Province::create([
			'id'=>583,
			'country_id'=>29,
			'name'=>'Campania - Salerno'
		]);

		Province::create([
			'id'=>584,
			'country_id'=>29,
			'name'=>'Puglia - Bari'
		]);

		Province::create([
			'id'=>585,
			'country_id'=>29,
			'name'=>'Puglia - Brindisi'
		]);

		Province::create([
			'id'=>586,
			'country_id'=>29,
			'name'=>'Puglia - Foggia'
		]);

		Province::create([
			'id'=>587,
			'country_id'=>29,
			'name'=>'Puglia - Lecce'
		]);

		Province::create([
			'id'=>588,
			'country_id'=>29,
			'name'=>'Puglia - Taranto'
		]);

		Province::create([
			'id'=>589,
			'country_id'=>29,
			'name'=>'Basilicata - Potenza'
		]);

		Province::create([
			'id'=>590,
			'country_id'=>29,
			'name'=>'Basilicata - Matera'
		]);

		Province::create([
			'id'=>591,
			'country_id'=>29,
			'name'=>'Calabria - Catanzaro'
		]);

		Province::create([
			'id'=>592,
			'country_id'=>29,
			'name'=>'Calabria - Cosenza'
		]);

		Province::create([
			'id'=>593,
			'country_id'=>29,
			'name'=>'Calabria - Crotone'
		]);

		Province::create([
			'id'=>594,
			'country_id'=>29,
			'name'=>'Calabria - Reggio Calabria'
		]);

		Province::create([
			'id'=>595,
			'country_id'=>29,
			'name'=>'Calabria - Vibo Valentia'
		]);

		Province::create([
			'id'=>596,
			'country_id'=>29,
			'name'=>'Sicilia - Palermo'
		]);

		Province::create([
			'id'=>597,
			'country_id'=>29,
			'name'=>'Sicilia - Agrigento'
		]);

		Province::create([
			'id'=>598,
			'country_id'=>29,
			'name'=>'Sicilia - Caltanissetta'
		]);

		Province::create([
			'id'=>599,
			'country_id'=>29,
			'name'=>'Sicilia - Catania'
		]);

		Province::create([
			'id'=>600,
			'country_id'=>29,
			'name'=>'Sicilia - Enna'
		]);

		Province::create([
			'id'=>601,
			'country_id'=>29,
			'name'=>'Sicilia - Messina'
		]);

		Province::create([
			'id'=>602,
			'country_id'=>29,
			'name'=>'Sicilia - Ragusa'
		]);

		Province::create([
			'id'=>603,
			'country_id'=>29,
			'name'=>'Sicilia - Siracusa'
		]);

		Province::create([
			'id'=>604,
			'country_id'=>29,
			'name'=>'Sicilia - Trapani'
		]);

		Province::create([
			'id'=>605,
			'country_id'=>29,
			'name'=>'Sardegna - Cagliari'
		]);

		Province::create([
			'id'=>606,
			'country_id'=>29,
			'name'=>'Sardegna - Nuoro'
		]);

		Province::create([
			'id'=>607,
			'country_id'=>29,
			'name'=>'Sardegna - Oristano'
		]);

		Province::create([
			'id'=>608,
			'country_id'=>29,
			'name'=>'Sardegna - Sassari'
		]);

		Province::create([
			'id'=>609,
			'country_id'=>28,
			'name'=>'Las Palmas'
		]);

		Province::create([
			'id'=>610,
			'country_id'=>28,
			'name'=>'Soria'
		]);

		Province::create([
			'id'=>611,
			'country_id'=>28,
			'name'=>'Palencia'
		]);

		Province::create([
			'id'=>612,
			'country_id'=>28,
			'name'=>'Zamora'
		]);

		Province::create([
			'id'=>613,
			'country_id'=>28,
			'name'=>'Cádiz'
		]);

		Province::create([
			'id'=>614,
			'country_id'=>28,
			'name'=>'Navarra'
		]);

		Province::create([
			'id'=>615,
			'country_id'=>28,
			'name'=>'Ourense'
		]);

		Province::create([
			'id'=>616,
			'country_id'=>28,
			'name'=>'Segovia'
		]);

		Province::create([
			'id'=>617,
			'country_id'=>28,
			'name'=>'Guipúzcoa'
		]);

		Province::create([
			'id'=>618,
			'country_id'=>28,
			'name'=>'Ciudad Real'
		]);

		Province::create([
			'id'=>619,
			'country_id'=>28,
			'name'=>'Vizcaya'
		]);

		Province::create([
			'id'=>620,
			'country_id'=>28,
			'name'=>'Álava'
		]);

		Province::create([
			'id'=>621,
			'country_id'=>28,
			'name'=>'A Coruña'
		]);

		Province::create([
			'id'=>622,
			'country_id'=>28,
			'name'=>'Cantabria'
		]);

		Province::create([
			'id'=>623,
			'country_id'=>28,
			'name'=>'Almería'
		]);

		Province::create([
			'id'=>624,
			'country_id'=>28,
			'name'=>'Zaragoza'
		]);

		Province::create([
			'id'=>625,
			'country_id'=>28,
			'name'=>'Santa Cruz de Tenerife'
		]);

		Province::create([
			'id'=>626,
			'country_id'=>28,
			'name'=>'Cáceres'
		]);

		Province::create([
			'id'=>627,
			'country_id'=>28,
			'name'=>'Guadalajara'
		]);

		Province::create([
			'id'=>628,
			'country_id'=>28,
			'name'=>'Ávila'
		]);

		Province::create([
			'id'=>629,
			'country_id'=>28,
			'name'=>'Toledo'
		]);

		Province::create([
			'id'=>630,
			'country_id'=>28,
			'name'=>'Castellón'
		]);

		Province::create([
			'id'=>631,
			'country_id'=>28,
			'name'=>'Tarragona'
		]);

		Province::create([
			'id'=>632,
			'country_id'=>28,
			'name'=>'Lugo'
		]);

		Province::create([
			'id'=>633,
			'country_id'=>28,
			'name'=>'La Rioja'
		]);

		Province::create([
			'id'=>634,
			'country_id'=>28,
			'name'=>'Ceuta'
		]);

		Province::create([
			'id'=>635,
			'country_id'=>28,
			'name'=>'Murcia'
		]);

		Province::create([
			'id'=>636,
			'country_id'=>28,
			'name'=>'Salamanca'
		]);

		Province::create([
			'id'=>637,
			'country_id'=>28,
			'name'=>'Valladolid'
		]);

		Province::create([
			'id'=>638,
			'country_id'=>28,
			'name'=>'Jaén'
		]);

		Province::create([
			'id'=>639,
			'country_id'=>28,
			'name'=>'Girona'
		]);

		Province::create([
			'id'=>640,
			'country_id'=>28,
			'name'=>'Granada'
		]);

		Province::create([
			'id'=>641,
			'country_id'=>28,
			'name'=>'Alacant'
		]);

		Province::create([
			'id'=>642,
			'country_id'=>28,
			'name'=>'Córdoba'
		]);

		Province::create([
			'id'=>643,
			'country_id'=>28,
			'name'=>'Albacete'
		]);

		Province::create([
			'id'=>644,
			'country_id'=>28,
			'name'=>'Cuenca'
		]);

		Province::create([
			'id'=>645,
			'country_id'=>28,
			'name'=>'Pontevedra'
		]);

		Province::create([
			'id'=>646,
			'country_id'=>28,
			'name'=>'Teruel'
		]);

		Province::create([
			'id'=>647,
			'country_id'=>28,
			'name'=>'Melilla'
		]);

		Province::create([
			'id'=>648,
			'country_id'=>28,
			'name'=>'Barcelona'
		]);

		Province::create([
			'id'=>649,
			'country_id'=>28,
			'name'=>'Badajoz'
		]);

		Province::create([
			'id'=>650,
			'country_id'=>28,
			'name'=>'Madrid'
		]);

		Province::create([
			'id'=>651,
			'country_id'=>28,
			'name'=>'Sevilla'
		]);

		Province::create([
			'id'=>652,
			'country_id'=>28,
			'name'=>'València'
		]);

		Province::create([
			'id'=>653,
			'country_id'=>28,
			'name'=>'Huelva'
		]);

		Province::create([
			'id'=>654,
			'country_id'=>28,
			'name'=>'Lleida'
		]);

		Province::create([
			'id'=>655,
			'country_id'=>28,
			'name'=>'León'
		]);

		Province::create([
			'id'=>656,
			'country_id'=>28,
			'name'=>'Illes Balears'
		]);

		Province::create([
			'id'=>657,
			'country_id'=>28,
			'name'=>'Burgos'
		]);

		Province::create([
			'id'=>658,
			'country_id'=>28,
			'name'=>'Huesca'
		]);

		Province::create([
			'id'=>659,
			'country_id'=>28,
			'name'=>'Asturias'
		]);

		Province::create([
			'id'=>660,
			'country_id'=>28,
			'name'=>'Málaga'
		]);

		Province::create([
			'id'=>661,
			'country_id'=>144,
			'name'=>'Afghanistan'
		]);

		Province::create([
			'id'=>662,
			'country_id'=>210,
			'name'=>'Niger'
		]);

		Province::create([
			'id'=>663,
			'country_id'=>133,
			'name'=>'Mali'
		]);

		Province::create([
			'id'=>664,
			'country_id'=>156,
			'name'=>'Burkina Faso'
		]);

		Province::create([
			'id'=>665,
			'country_id'=>136,
			'name'=>'Togo'
		]);

		Province::create([
			'id'=>666,
			'country_id'=>151,
			'name'=>'Benin'
		]);

		Province::create([
			'id'=>667,
			'country_id'=>119,
			'name'=>'Angola'
		]);

		Province::create([
			'id'=>668,
			'country_id'=>102,
			'name'=>'Namibia'
		]);

		Province::create([
			'id'=>669,
			'country_id'=>100,
			'name'=>'Botswana'
		]);

		Province::create([
			'id'=>670,
			'country_id'=>134,
			'name'=>'Madagascar'
		]);

		Province::create([
			'id'=>671,
			'country_id'=>202,
			'name'=>'Mauritius'
		]);

		Province::create([
			'id'=>672,
			'country_id'=>196,
			'name'=>'Laos'
		]);

		Province::create([
			'id'=>673,
			'country_id'=>158,
			'name'=>'Cambodia'
		]);

		Province::create([
			'id'=>674,
			'country_id'=>90,
			'name'=>'Philippines'
		]);

		Province::create([
			'id'=>675,
			'country_id'=>88,
			'name'=>'Papua New Guinea'
		]);

		Province::create([
			'id'=>676,
			'country_id'=>228,
			'name'=>'Solomon Islands'
		]);

		Province::create([
			'id'=>677,
			'country_id'=>240,
			'name'=>'Vanuatu'
		]);

		Province::create([
			'id'=>678,
			'country_id'=>176,
			'name'=>'Fiji'
		]);

		Province::create([
			'id'=>679,
			'country_id'=>223,
			'name'=>'Samoa'
		]);

		Province::create([
			'id'=>680,
			'country_id'=>206,
			'name'=>'Nauru'
		]);

		Province::create([
			'id'=>681,
			'country_id'=>168,
			'name'=>'Cote DIvoire'
		]);

		Province::create([
			'id'=>682,
			'country_id'=>198,
			'name'=>'Liberia'
		]);

		Province::create([
			'id'=>683,
			'country_id'=>187,
			'name'=>'Guinea'
		]);

		Province::create([
			'id'=>684,
			'country_id'=>189,
			'name'=>'Guyana'
		]);

		Province::create([
			'id'=>685,
			'country_id'=>98,
			'name'=>'Algeria'
		]);

		Province::create([
			'id'=>686,
			'country_id'=>147,
			'name'=>'Antigua and Barbuda'
		]);

		Province::create([
			'id'=>687,
			'country_id'=>127,
			'name'=>'Bahrain'
		]);

		Province::create([
			'id'=>688,
			'country_id'=>149,
			'name'=>'Bangladesh'
		]);

		Province::create([
			'id'=>689,
			'country_id'=>128,
			'name'=>'Barbados'
		]);

		Province::create([
			'id'=>690,
			'country_id'=>152,
			'name'=>'Bhutan'
		]);

		Province::create([
			'id'=>691,
			'country_id'=>155,
			'name'=>'Brunei'
		]);

		Province::create([
			'id'=>692,
			'country_id'=>157,
			'name'=>'Burundi'
		]);

		Province::create([
			'id'=>693,
			'country_id'=>159,
			'name'=>'Cape Verde'
		]);

		Province::create([
			'id'=>694,
			'country_id'=>130,
			'name'=>'Chad'
		]);

		Province::create([
			'id'=>695,
			'country_id'=>164,
			'name'=>'Comoros'
		]);

		Province::create([
			'id'=>696,
			'country_id'=>112,
			'name'=>'Congo (Brazzaville)'
		]);

		Province::create([
			'id'=>697,
			'country_id'=>169,
			'name'=>'Djibouti'
		]);

		Province::create([
			'id'=>698,
			'country_id'=>171,
			'name'=>'East Timor'
		]);

		Province::create([
			'id'=>699,
			'country_id'=>173,
			'name'=>'Eritrea'
		]);

		Province::create([
			'id'=>700,
			'country_id'=>121,
			'name'=>'Ethiopia'
		]);

		Province::create([
			'id'=>701,
			'country_id'=>180,
			'name'=>'Gabon'
		]);

		Province::create([
			'id'=>702,
			'country_id'=>181,
			'name'=>'Gambia'
		]);

		Province::create([
			'id'=>703,
			'country_id'=>105,
			'name'=>'Ghana'
		]);

		Province::create([
			'id'=>704,
			'country_id'=>197,
			'name'=>'Lesotho'
		]);

		Province::create([
			'id'=>705,
			'country_id'=>125,
			'name'=>'Malawi'
		]);

		Province::create([
			'id'=>706,
			'country_id'=>200,
			'name'=>'Maldives'
		]);

		Province::create([
			'id'=>707,
			'country_id'=>205,
			'name'=>'Myanmar (Burma)'
		]);

		Province::create([
			'id'=>708,
			'country_id'=>107,
			'name'=>'Nepal'
		]);

		Province::create([
			'id'=>709,
			'country_id'=>213,
			'name'=>'Oman'
		]);

		Province::create([
			'id'=>710,
			'country_id'=>217,
			'name'=>'Rwanda'
		]);

		Province::create([
			'id'=>711,
			'country_id'=>91,
			'name'=>'Saudi Arabia'
		]);

		Province::create([
			'id'=>712,
			'country_id'=>120,
			'name'=>'Sri Lanka'
		]);

		Province::create([
			'id'=>713,
			'country_id'=>232,
			'name'=>'Sudan'
		]);

		Province::create([
			'id'=>714,
			'country_id'=>234,
			'name'=>'Swaziland'
		]);

		Province::create([
			'id'=>715,
			'country_id'=>101,
			'name'=>'Tanzania'
		]);

		Province::create([
			'id'=>716,
			'country_id'=>236,
			'name'=>'Tonga'
		]);

		Province::create([
			'id'=>717,
			'country_id'=>239,
			'name'=>'Tuvalu'
		]);

		Province::create([
			'id'=>718,
			'country_id'=>242,
			'name'=>'Western Sahara'
		]);

		Province::create([
			'id'=>719,
			'country_id'=>243,
			'name'=>'Yemen'
		]);

		Province::create([
			'id'=>720,
			'country_id'=>116,
			'name'=>'Zambia'
		]);

		Province::create([
			'id'=>721,
			'country_id'=>96,
			'name'=>'Zimbabwe'
		]);

		Province::create([
			'id'=>722,
			'country_id'=>66,
			'name'=>'Aargau'
		]);

		Province::create([
			'id'=>723,
			'country_id'=>66,
			'name'=>'Appenzell Innerrhoden'
		]);

		Province::create([
			'id'=>724,
			'country_id'=>66,
			'name'=>'Appenzell Ausserrhoden'
		]);

		Province::create([
			'id'=>725,
			'country_id'=>66,
			'name'=>'Bern'
		]);

		Province::create([
			'id'=>726,
			'country_id'=>66,
			'name'=>'Basel-Landschaft'
		]);

		Province::create([
			'id'=>727,
			'country_id'=>66,
			'name'=>'Basel-Stadt'
		]);

		Province::create([
			'id'=>728,
			'country_id'=>66,
			'name'=>'Fribourg'
		]);

		Province::create([
			'id'=>729,
			'country_id'=>66,
			'name'=>'Genève'
		]);

		Province::create([
			'id'=>730,
			'country_id'=>66,
			'name'=>'Glarus'
		]);

		Province::create([
			'id'=>731,
			'country_id'=>66,
			'name'=>'Graubünden'
		]);

		Province::create([
			'id'=>732,
			'country_id'=>66,
			'name'=>'Jura'
		]);

		Province::create([
			'id'=>733,
			'country_id'=>66,
			'name'=>'Luzern'
		]);

		Province::create([
			'id'=>734,
			'country_id'=>66,
			'name'=>'Neuchâtel'
		]);

		Province::create([
			'id'=>735,
			'country_id'=>66,
			'name'=>'Nidwalden'
		]);

		Province::create([
			'id'=>736,
			'country_id'=>66,
			'name'=>'Obwalden'
		]);

		Province::create([
			'id'=>737,
			'country_id'=>66,
			'name'=>'Sankt Gallen'
		]);

		Province::create([
			'id'=>738,
			'country_id'=>66,
			'name'=>'Schaffhausen'
		]);

		Province::create([
			'id'=>739,
			'country_id'=>66,
			'name'=>'Solothurn'
		]);

		Province::create([
			'id'=>740,
			'country_id'=>66,
			'name'=>'Schwyz'
		]);

		Province::create([
			'id'=>741,
			'country_id'=>66,
			'name'=>'Thurgau'
		]);

		Province::create([
			'id'=>742,
			'country_id'=>66,
			'name'=>'Ticino'
		]);

		Province::create([
			'id'=>743,
			'country_id'=>66,
			'name'=>'Uri'
		]);

		Province::create([
			'id'=>744,
			'country_id'=>66,
			'name'=>'Vaud'
		]);

		Province::create([
			'id'=>745,
			'country_id'=>66,
			'name'=>'Valais'
		]);

		Province::create([
			'id'=>746,
			'country_id'=>66,
			'name'=>'Zug'
		]);

		Province::create([
			'id'=>747,
			'country_id'=>66,
			'name'=>'Zürich'
		]);

		Province::create([
			'id'=>749,
			'country_id'=>48,
			'name'=>'Aveiro'
		]);

		Province::create([
			'id'=>750,
			'country_id'=>48,
			'name'=>'Beja'
		]);

		Province::create([
			'id'=>751,
			'country_id'=>48,
			'name'=>'Braga'
		]);

		Province::create([
			'id'=>752,
			'country_id'=>48,
			'name'=>'Braganca'
		]);

		Province::create([
			'id'=>753,
			'country_id'=>48,
			'name'=>'Castelo Branco'
		]);

		Province::create([
			'id'=>754,
			'country_id'=>48,
			'name'=>'Coimbra'
		]);

		Province::create([
			'id'=>755,
			'country_id'=>48,
			'name'=>'Evora'
		]);

		Province::create([
			'id'=>756,
			'country_id'=>48,
			'name'=>'Faro'
		]);

		Province::create([
			'id'=>757,
			'country_id'=>48,
			'name'=>'Madeira'
		]);

		Province::create([
			'id'=>758,
			'country_id'=>48,
			'name'=>'Guarda'
		]);

		Province::create([
			'id'=>759,
			'country_id'=>48,
			'name'=>'Leiria'
		]);

		Province::create([
			'id'=>760,
			'country_id'=>48,
			'name'=>'Lisboa'
		]);

		Province::create([
			'id'=>761,
			'country_id'=>48,
			'name'=>'Portalegre'
		]);

		Province::create([
			'id'=>762,
			'country_id'=>48,
			'name'=>'Porto'
		]);

		Province::create([
			'id'=>763,
			'country_id'=>48,
			'name'=>'Santarem'
		]);

		Province::create([
			'id'=>764,
			'country_id'=>48,
			'name'=>'Setubal'
		]);

		Province::create([
			'id'=>765,
			'country_id'=>48,
			'name'=>'Viana do Castelo'
		]);

		Province::create([
			'id'=>766,
			'country_id'=>48,
			'name'=>'Vila Real'
		]);

		Province::create([
			'id'=>767,
			'country_id'=>48,
			'name'=>'Viseu'
		]);

		Province::create([
			'id'=>768,
			'country_id'=>48,
			'name'=>'Azores'
		]);

		Province::create([
			'id'=>769,
			'country_id'=>55,
			'name'=>'Armed Forces Americas'
		]);

		Province::create([
			'id'=>770,
			'country_id'=>55,
			'name'=>'Armed Forces Europe'
		]);

		Province::create([
			'id'=>771,
			'country_id'=>55,
			'name'=>'Alaska'
		]);

		Province::create([
			'id'=>772,
			'country_id'=>55,
			'name'=>'Alabama'
		]);

		Province::create([
			'id'=>773,
			'country_id'=>55,
			'name'=>'Armed Forces Pacific'
		]);

		Province::create([
			'id'=>774,
			'country_id'=>55,
			'name'=>'Arkansas'
		]);

		Province::create([
			'id'=>775,
			'country_id'=>55,
			'name'=>'American Samoa'
		]);

		Province::create([
			'id'=>776,
			'country_id'=>55,
			'name'=>'Arizona'
		]);

		Province::create([
			'id'=>777,
			'country_id'=>55,
			'name'=>'California'
		]);

		Province::create([
			'id'=>778,
			'country_id'=>55,
			'name'=>'Colorado'
		]);

		Province::create([
			'id'=>779,
			'country_id'=>55,
			'name'=>'Connecticut'
		]);

		Province::create([
			'id'=>780,
			'country_id'=>55,
			'name'=>'District of Columbia'
		]);

		Province::create([
			'id'=>781,
			'country_id'=>55,
			'name'=>'Delaware'
		]);

		Province::create([
			'id'=>782,
			'country_id'=>55,
			'name'=>'Florida'
		]);

		Province::create([
			'id'=>783,
			'country_id'=>55,
			'name'=>'Federated Provincess of Micronesia'
		]);

		Province::create([
			'id'=>784,
			'country_id'=>55,
			'name'=>'Georgia'
		]);

		Province::create([
			'id'=>786,
			'country_id'=>55,
			'name'=>'Hawaii'
		]);

		Province::create([
			'id'=>787,
			'country_id'=>55,
			'name'=>'Iowa'
		]);

		Province::create([
			'id'=>788,
			'country_id'=>55,
			'name'=>'Idaho'
		]);

		Province::create([
			'id'=>789,
			'country_id'=>55,
			'name'=>'Illinois'
		]);

		Province::create([
			'id'=>790,
			'country_id'=>55,
			'name'=>'Indiana'
		]);

		Province::create([
			'id'=>791,
			'country_id'=>55,
			'name'=>'Kansas'
		]);

		Province::create([
			'id'=>792,
			'country_id'=>55,
			'name'=>'Kentucky'
		]);

		Province::create([
			'id'=>793,
			'country_id'=>55,
			'name'=>'Louisiana'
		]);

		Province::create([
			'id'=>794,
			'country_id'=>55,
			'name'=>'Massachusetts'
		]);

		Province::create([
			'id'=>795,
			'country_id'=>55,
			'name'=>'Maryland'
		]);

		Province::create([
			'id'=>796,
			'country_id'=>55,
			'name'=>'Maine'
		]);

		Province::create([
			'id'=>797,
			'country_id'=>55,
			'name'=>'Marshall Islands'
		]);

		Province::create([
			'id'=>798,
			'country_id'=>55,
			'name'=>'Michigan'
		]);

		Province::create([
			'id'=>799,
			'country_id'=>55,
			'name'=>'Minnesota'
		]);

		Province::create([
			'id'=>800,
			'country_id'=>55,
			'name'=>'Missouri'
		]);

		Province::create([
			'id'=>801,
			'country_id'=>55,
			'name'=>'Northern Mariana Islands'
		]);

		Province::create([
			'id'=>802,
			'country_id'=>55,
			'name'=>'Mississippi'
		]);

		Province::create([
			'id'=>803,
			'country_id'=>55,
			'name'=>'Montana'
		]);

		Province::create([
			'id'=>804,
			'country_id'=>55,
			'name'=>'North Carolina'
		]);

		Province::create([
			'id'=>805,
			'country_id'=>55,
			'name'=>'North Dakota'
		]);

		Province::create([
			'id'=>806,
			'country_id'=>55,
			'name'=>'Nebraska'
		]);

		Province::create([
			'id'=>807,
			'country_id'=>55,
			'name'=>'New Hampshire'
		]);

		Province::create([
			'id'=>808,
			'country_id'=>55,
			'name'=>'New Jersey'
		]);

		Province::create([
			'id'=>809,
			'country_id'=>55,
			'name'=>'New Mexico'
		]);

		Province::create([
			'id'=>810,
			'country_id'=>55,
			'name'=>'Nevada'
		]);

		Province::create([
			'id'=>811,
			'country_id'=>55,
			'name'=>'New York'
		]);

		Province::create([
			'id'=>812,
			'country_id'=>55,
			'name'=>'Ohio'
		]);

		Province::create([
			'id'=>813,
			'country_id'=>55,
			'name'=>'Oklahoma'
		]);

		Province::create([
			'id'=>814,
			'country_id'=>55,
			'name'=>'Oregon'
		]);

		Province::create([
			'id'=>815,
			'country_id'=>55,
			'name'=>'Pennsylvania'
		]);

		Province::create([
			'id'=>816,
			'country_id'=>246,
			'name'=>'Puerto Rico'
		]);

		Province::create([
			'id'=>817,
			'country_id'=>55,
			'name'=>'Palau'
		]);

		Province::create([
			'id'=>818,
			'country_id'=>55,
			'name'=>'Rhode Island'
		]);

		Province::create([
			'id'=>819,
			'country_id'=>55,
			'name'=>'South Carolina'
		]);

		Province::create([
			'id'=>820,
			'country_id'=>55,
			'name'=>'South Dakota'
		]);

		Province::create([
			'id'=>821,
			'country_id'=>55,
			'name'=>'Tennessee'
		]);

		Province::create([
			'id'=>822,
			'country_id'=>55,
			'name'=>'Texas'
		]);

		Province::create([
			'id'=>823,
			'country_id'=>55,
			'name'=>'Utah'
		]);

		Province::create([
			'id'=>824,
			'country_id'=>55,
			'name'=>'Virginia'
		]);

		Province::create([
			'id'=>825,
			'country_id'=>55,
			'name'=>'Virgin Islands'
		]);

		Province::create([
			'id'=>826,
			'country_id'=>55,
			'name'=>'Vermont'
		]);

		Province::create([
			'id'=>827,
			'country_id'=>55,
			'name'=>'Washington'
		]);

		Province::create([
			'id'=>828,
			'country_id'=>55,
			'name'=>'West Virginia'
		]);

		Province::create([
			'id'=>829,
			'country_id'=>55,
			'name'=>'Wisconsin'
		]);

		Province::create([
			'id'=>830,
			'country_id'=>55,
			'name'=>'Wyoming'
		]);

		Province::create([
			'id'=>831,
			'country_id'=>94,
			'name'=>'Greenland'
		]);

		Province::create([
			'id'=>832,
			'country_id'=>18,
			'name'=>'Brandenburg'
		]);

		Province::create([
			'id'=>833,
			'country_id'=>18,
			'name'=>'Baden-Württemberg'
		]);

		Province::create([
			'id'=>834,
			'country_id'=>18,
			'name'=>'Bayern'
		]);

		Province::create([
			'id'=>835,
			'country_id'=>18,
			'name'=>'Hessen'
		]);

		Province::create([
			'id'=>836,
			'country_id'=>18,
			'name'=>'Hamburg'
		]);

		Province::create([
			'id'=>837,
			'country_id'=>18,
			'name'=>'Mecklenburg-Vorpommern'
		]);

		Province::create([
			'id'=>838,
			'country_id'=>18,
			'name'=>'Niedersachsen'
		]);

		Province::create([
			'id'=>839,
			'country_id'=>18,
			'name'=>'Nordrhein-Westfalen'
		]);

		Province::create([
			'id'=>840,
			'country_id'=>18,
			'name'=>'Rheinland-Pfalz'
		]);

		Province::create([
			'id'=>841,
			'country_id'=>18,
			'name'=>'Schleswig-Holstein'
		]);

		Province::create([
			'id'=>842,
			'country_id'=>18,
			'name'=>'Sachsen'
		]);

		Province::create([
			'id'=>843,
			'country_id'=>18,
			'name'=>'Sachsen-Anhalt'
		]);

		Province::create([
			'id'=>844,
			'country_id'=>18,
			'name'=>'Thüringen'
		]);

		Province::create([
			'id'=>845,
			'country_id'=>18,
			'name'=>'Berlin'
		]);

		Province::create([
			'id'=>846,
			'country_id'=>18,
			'name'=>'Bremen'
		]);

		Province::create([
			'id'=>847,
			'country_id'=>18,
			'name'=>'Saarland'
		]);

		Province::create([
			'id'=>848,
			'country_id'=>13,
			'name'=>'Scotland North'
		]);

		Province::create([
			'id'=>849,
			'country_id'=>13,
			'name'=>'England - East'
		]);

		Province::create([
			'id'=>850,
			'country_id'=>13,
			'name'=>'England - West Midlands'
		]);

		Province::create([
			'id'=>851,
			'country_id'=>13,
			'name'=>'England - South West'
		]);

		Province::create([
			'id'=>852,
			'country_id'=>13,
			'name'=>'England - North West'
		]);

		Province::create([
			'id'=>853,
			'country_id'=>13,
			'name'=>'England - Yorks Y Humber'
		]);

		Province::create([
			'id'=>854,
			'country_id'=>13,
			'name'=>'England - South East'
		]);

		Province::create([
			'id'=>855,
			'country_id'=>13,
			'name'=>'England - London'
		]);

		Province::create([
			'id'=>856,
			'country_id'=>13,
			'name'=>'Northern Ireland'
		]);

		Province::create([
			'id'=>857,
			'country_id'=>13,
			'name'=>'England - North East'
		]);

		Province::create([
			'id'=>858,
			'country_id'=>13,
			'name'=>'Wales South'
		]);

		Province::create([
			'id'=>859,
			'country_id'=>13,
			'name'=>'Wales North'
		]);

		Province::create([
			'id'=>860,
			'country_id'=>13,
			'name'=>'England - East Midlands'
		]);

		Province::create([
			'id'=>861,
			'country_id'=>13,
			'name'=>'Scotland Central'
		]);

		Province::create([
			'id'=>862,
			'country_id'=>13,
			'name'=>'Scotland South'
		]);

		Province::create([
			'id'=>863,
			'country_id'=>13,
			'name'=>'Channel Islands'
		]);

		Province::create([
			'id'=>864,
			'country_id'=>13,
			'name'=>'Isle of Man'
		]);

		Province::create([
			'id'=>865,
			'country_id'=>2,
			'name'=>'Burgenland'
		]);

		Province::create([
			'id'=>866,
			'country_id'=>2,
			'name'=>'Kärnten'
		]);

		Province::create([
			'id'=>867,
			'country_id'=>2,
			'name'=>'Niederösterreich'
		]);

		Province::create([
			'id'=>868,
			'country_id'=>2,
			'name'=>'Oberösterreich'
		]);

		Province::create([
			'id'=>869,
			'country_id'=>2,
			'name'=>'Salzburg'
		]);

		Province::create([
			'id'=>870,
			'country_id'=>2,
			'name'=>'Steiermark'
		]);

		Province::create([
			'id'=>871,
			'country_id'=>2,
			'name'=>'Tirol'
		]);

		Province::create([
			'id'=>872,
			'country_id'=>2,
			'name'=>'Vorarlberg'
		]);

		Province::create([
			'id'=>873,
			'country_id'=>2,
			'name'=>'Wien'
		]);

		Province::create([
			'id'=>874,
			'country_id'=>9,
			'name'=>'Bruxelles'
		]);

		Province::create([
			'id'=>875,
			'country_id'=>9,
			'name'=>'West-Vlaanderen'
		]);

		Province::create([
			'id'=>876,
			'country_id'=>9,
			'name'=>'Oost-Vlaanderen'
		]);

		Province::create([
			'id'=>877,
			'country_id'=>9,
			'name'=>'Limburg'
		]);

		Province::create([
			'id'=>878,
			'country_id'=>9,
			'name'=>'Vlaams Brabant'
		]);

		Province::create([
			'id'=>879,
			'country_id'=>9,
			'name'=>'Antwerpen'
		]);

		Province::create([
			'id'=>880,
			'country_id'=>9,
			'name'=>'LiÃ„Âge'
		]);

		Province::create([
			'id'=>881,
			'country_id'=>9,
			'name'=>'Namur'
		]);

		Province::create([
			'id'=>882,
			'country_id'=>9,
			'name'=>'Hainaut'
		]);

		Province::create([
			'id'=>883,
			'country_id'=>9,
			'name'=>'Luxembourg'
		]);

		Province::create([
			'id'=>884,
			'country_id'=>9,
			'name'=>'Brabant Wallon'
		]);

		Province::create([
			'id'=>887,
			'country_id'=>67,
			'name'=>'Blekinge Lan'
		]);

		Province::create([
			'id'=>888,
			'country_id'=>67,
			'name'=>'Gavleborgs Lan'
		]);

		Province::create([
			'id'=>890,
			'country_id'=>67,
			'name'=>'Gotlands Lan'
		]);

		Province::create([
			'id'=>891,
			'country_id'=>67,
			'name'=>'Hallands Lan'
		]);

		Province::create([
			'id'=>892,
			'country_id'=>67,
			'name'=>'Jamtlands Lan'
		]);

		Province::create([
			'id'=>893,
			'country_id'=>67,
			'name'=>'Jonkopings Lan'
		]);

		Province::create([
			'id'=>894,
			'country_id'=>67,
			'name'=>'Kalmar Lan'
		]);

		Province::create([
			'id'=>895,
			'country_id'=>67,
			'name'=>'Dalarnas Lan'
		]);

		Province::create([
			'id'=>897,
			'country_id'=>67,
			'name'=>'Kronobergs Lan'
		]);

		Province::create([
			'id'=>899,
			'country_id'=>67,
			'name'=>'Norrbottens Lan'
		]);

		Province::create([
			'id'=>900,
			'country_id'=>67,
			'name'=>'Orebro Lan'
		]);

		Province::create([
			'id'=>901,
			'country_id'=>67,
			'name'=>'Ostergotlands Lan'
		]);

		Province::create([
			'id'=>903,
			'country_id'=>67,
			'name'=>'Sodermanlands Lan'
		]);

		Province::create([
			'id'=>904,
			'country_id'=>67,
			'name'=>'Uppsala Lan'
		]);

		Province::create([
			'id'=>905,
			'country_id'=>67,
			'name'=>'Varmlands Lan'
		]);

		Province::create([
			'id'=>906,
			'country_id'=>67,
			'name'=>'Vasterbottens Lan'
		]);

		Province::create([
			'id'=>907,
			'country_id'=>67,
			'name'=>'Vasternorrlands Lan'
		]);

		Province::create([
			'id'=>908,
			'country_id'=>67,
			'name'=>'Vastmanlands Lan'
		]);

		Province::create([
			'id'=>909,
			'country_id'=>67,
			'name'=>'Stockholms Lan'
		]);

		Province::create([
			'id'=>910,
			'country_id'=>67,
			'name'=>'Skane Lan'
		]);

		Province::create([
			'id'=>911,
			'country_id'=>67,
			'name'=>'Vastra Gotaland'
		]);

		Province::create([
			'id'=>913,
			'country_id'=>46,
			'name'=>'Akershus'
		]);

		Province::create([
			'id'=>914,
			'country_id'=>46,
			'name'=>'Aust-Agder'
		]);

		Province::create([
			'id'=>915,
			'country_id'=>46,
			'name'=>'Buskerud'
		]);

		Province::create([
			'id'=>916,
			'country_id'=>46,
			'name'=>'Finnmark'
		]);

		Province::create([
			'id'=>917,
			'country_id'=>46,
			'name'=>'Hedmark'
		]);

		Province::create([
			'id'=>918,
			'country_id'=>46,
			'name'=>'Hordaland'
		]);

		Province::create([
			'id'=>919,
			'country_id'=>46,
			'name'=>'More og Romsdal'
		]);

		Province::create([
			'id'=>920,
			'country_id'=>46,
			'name'=>'Nordland'
		]);

		Province::create([
			'id'=>921,
			'country_id'=>46,
			'name'=>'Nord-Trondelag'
		]);

		Province::create([
			'id'=>922,
			'country_id'=>46,
			'name'=>'Oppland'
		]);

		Province::create([
			'id'=>923,
			'country_id'=>46,
			'name'=>'Oslo'
		]);

		Province::create([
			'id'=>924,
			'country_id'=>46,
			'name'=>'Ostfold'
		]);

		Province::create([
			'id'=>925,
			'country_id'=>46,
			'name'=>'Rogaland'
		]);

		Province::create([
			'id'=>926,
			'country_id'=>46,
			'name'=>'Sogn og Fjordane'
		]);

		Province::create([
			'id'=>927,
			'country_id'=>46,
			'name'=>'Sor-Trondelag'
		]);

		Province::create([
			'id'=>928,
			'country_id'=>46,
			'name'=>'Telemark'
		]);

		Province::create([
			'id'=>929,
			'country_id'=>46,
			'name'=>'Troms'
		]);

		Province::create([
			'id'=>930,
			'country_id'=>46,
			'name'=>'Vest-Agder'
		]);

		Province::create([
			'id'=>931,
			'country_id'=>46,
			'name'=>'Vestfold'
		]);

		Province::create([
			'id'=>933,
			'country_id'=>63,
			'name'=>'Ð•land'
		]);

		Province::create([
			'id'=>934,
			'country_id'=>63,
			'name'=>'Lapland'
		]);

		Province::create([
			'id'=>935,
			'country_id'=>63,
			'name'=>'Oulu'
		]);

		Province::create([
			'id'=>936,
			'country_id'=>63,
			'name'=>'Southern Finland'
		]);

		Province::create([
			'id'=>937,
			'country_id'=>63,
			'name'=>'Eastern Finland'
		]);

		Province::create([
			'id'=>938,
			'country_id'=>63,
			'name'=>'Western Finland'
		]);

		Province::create([
			'id'=>940,
			'country_id'=>22,
			'name'=>'Arhus'
		]);

		Province::create([
			'id'=>941,
			'country_id'=>22,
			'name'=>'Bornholm'
		]);

		Province::create([
			'id'=>942,
			'country_id'=>22,
			'name'=>'Frederiksborg'
		]);

		Province::create([
			'id'=>943,
			'country_id'=>22,
			'name'=>'Fyn'
		]);

		Province::create([
			'id'=>944,
			'country_id'=>22,
			'name'=>'Kobenhavn'
		]);

		Province::create([
			'id'=>945,
			'country_id'=>22,
			'name'=>'Staden Kobenhavn'
		]);

		Province::create([
			'id'=>946,
			'country_id'=>22,
			'name'=>'Nordjylland'
		]);

		Province::create([
			'id'=>947,
			'country_id'=>22,
			'name'=>'Ribe'
		]);

		Province::create([
			'id'=>948,
			'country_id'=>22,
			'name'=>'Ringkobing'
		]);

		Province::create([
			'id'=>949,
			'country_id'=>22,
			'name'=>'Roskilde'
		]);

		Province::create([
			'id'=>950,
			'country_id'=>22,
			'name'=>'Sonderjylland'
		]);

		Province::create([
			'id'=>951,
			'country_id'=>22,
			'name'=>'Storstrom'
		]);

		Province::create([
			'id'=>952,
			'country_id'=>22,
			'name'=>'Vejle'
		]);

		Province::create([
			'id'=>953,
			'country_id'=>22,
			'name'=>'Vestsjalland'
		]);

		Province::create([
			'id'=>954,
			'country_id'=>22,
			'name'=>'Viborg'
		]);

		Province::create([
			'id'=>956,
			'country_id'=>65,
			'name'=>'Hlavni Mesto Praha'
		]);

		Province::create([
			'id'=>957,
			'country_id'=>65,
			'name'=>'Jihomoravsky Kraj'
		]);

		Province::create([
			'id'=>958,
			'country_id'=>65,
			'name'=>'Jihocesky Kraj'
		]);

		Province::create([
			'id'=>959,
			'country_id'=>65,
			'name'=>'Vysocina'
		]);

		Province::create([
			'id'=>960,
			'country_id'=>65,
			'name'=>'Karlovarsky Kraj'
		]);

		Province::create([
			'id'=>961,
			'country_id'=>65,
			'name'=>'Kralovehradecky Kraj'
		]);

		Province::create([
			'id'=>962,
			'country_id'=>65,
			'name'=>'Liberecky Kraj'
		]);

		Province::create([
			'id'=>963,
			'country_id'=>65,
			'name'=>'Olomoucky Kraj'
		]);

		Province::create([
			'id'=>964,
			'country_id'=>65,
			'name'=>'Moravskoslezsky Kraj'
		]);

		Province::create([
			'id'=>965,
			'country_id'=>65,
			'name'=>'Pardubicky Kraj'
		]);

		Province::create([
			'id'=>966,
			'country_id'=>65,
			'name'=>'Plzensky Kraj'
		]);

		Province::create([
			'id'=>967,
			'country_id'=>65,
			'name'=>'Stredocesky Kraj'
		]);

		Province::create([
			'id'=>968,
			'country_id'=>65,
			'name'=>'Ustecky Kraj'
		]);

		Province::create([
			'id'=>969,
			'country_id'=>65,
			'name'=>'Zlinsky Kraj'
		]);

		Province::create([
			'id'=>971,
			'country_id'=>114,
			'name'=>'Berat'
		]);

		Province::create([
			'id'=>972,
			'country_id'=>114,
			'name'=>'Diber'
		]);

		Province::create([
			'id'=>973,
			'country_id'=>114,
			'name'=>'Durres'
		]);

		Province::create([
			'id'=>974,
			'country_id'=>114,
			'name'=>'Elbasan'
		]);

		Province::create([
			'id'=>975,
			'country_id'=>114,
			'name'=>'Fier'
		]);

		Province::create([
			'id'=>976,
			'country_id'=>114,
			'name'=>'Gjirokaster'
		]);

		Province::create([
			'id'=>977,
			'country_id'=>114,
			'name'=>'Korce'
		]);

		Province::create([
			'id'=>978,
			'country_id'=>114,
			'name'=>'Kukes'
		]);

		Province::create([
			'id'=>979,
			'country_id'=>114,
			'name'=>'Lezhe'
		]);

		Province::create([
			'id'=>980,
			'country_id'=>114,
			'name'=>'Shkoder'
		]);

		Province::create([
			'id'=>981,
			'country_id'=>114,
			'name'=>'Tirane'
		]);

		Province::create([
			'id'=>982,
			'country_id'=>114,
			'name'=>'Vlore'
		]);

		Province::create([
			'id'=>984,
			'country_id'=>145,
			'name'=>'Canillo'
		]);

		Province::create([
			'id'=>985,
			'country_id'=>145,
			'name'=>'Encamp'
		]);

		Province::create([
			'id'=>986,
			'country_id'=>145,
			'name'=>'La Massana'
		]);

		Province::create([
			'id'=>987,
			'country_id'=>145,
			'name'=>'Ordino'
		]);

		Province::create([
			'id'=>988,
			'country_id'=>145,
			'name'=>'Sant Julia de Loria'
		]);

		Province::create([
			'id'=>989,
			'country_id'=>145,
			'name'=>'Andorra la Vella'
		]);

		Province::create([
			'id'=>990,
			'country_id'=>145,
			'name'=>'Escaldes-Engordany'
		]);

		Province::create([
			'id'=>992,
			'country_id'=>6,
			'name'=>'Aragatsotn'
		]);

		Province::create([
			'id'=>993,
			'country_id'=>6,
			'name'=>'Ararat'
		]);

		Province::create([
			'id'=>994,
			'country_id'=>6,
			'name'=>'Armavir'
		]);

		Province::create([
			'id'=>995,
			'country_id'=>6,
			'name'=>'Gegharkunik'
		]);

		Province::create([
			'id'=>996,
			'country_id'=>6,
			'name'=>'Kotayk'
		]);

		Province::create([
			'id'=>997,
			'country_id'=>6,
			'name'=>'Lorri'
		]);

		Province::create([
			'id'=>998,
			'country_id'=>6,
			'name'=>'Shirak'
		]);

		Province::create([
			'id'=>999,
			'country_id'=>6,
			'name'=>'Syunik'
		]);

		Province::create([
			'id'=>1000,
			'country_id'=>6,
			'name'=>'Tavush'
		]);

		Province::create([
			'id'=>1001,
			'country_id'=>6,
			'name'=>'Vayots Dzor'
		]);

		Province::create([
			'id'=>1002,
			'country_id'=>6,
			'name'=>'Yerevan'
		]);

		Province::create([
			'id'=>1004,
			'country_id'=>79,
			'name'=>'Federation of Bosnia and Herzegovina'
		]);

		Province::create([
			'id'=>1005,
			'country_id'=>79,
			'name'=>'Republika Srpska'
		]);

		Province::create([
			'id'=>1007,
			'country_id'=>11,
			'name'=>'Mikhaylovgrad'
		]);

		Province::create([
			'id'=>1008,
			'country_id'=>11,
			'name'=>'Blagoevgrad'
		]);

		Province::create([
			'id'=>1009,
			'country_id'=>11,
			'name'=>'Burgas'
		]);

		Province::create([
			'id'=>1010,
			'country_id'=>11,
			'name'=>'Dobrich'
		]);

		Province::create([
			'id'=>1011,
			'country_id'=>11,
			'name'=>'Gabrovo'
		]);

		Province::create([
			'id'=>1012,
			'country_id'=>11,
			'name'=>'Grad Sofiya'
		]);

		Province::create([
			'id'=>1013,
			'country_id'=>11,
			'name'=>'Khaskovo'
		]);

		Province::create([
			'id'=>1014,
			'country_id'=>11,
			'name'=>'Kurdzhali'
		]);

		Province::create([
			'id'=>1015,
			'country_id'=>11,
			'name'=>'Kyustendil'
		]);

		Province::create([
			'id'=>1016,
			'country_id'=>11,
			'name'=>'Lovech'
		]);

		Province::create([
			'id'=>1017,
			'country_id'=>11,
			'name'=>'Montana'
		]);

		Province::create([
			'id'=>1018,
			'country_id'=>11,
			'name'=>'Pazardzhik'
		]);

		Province::create([
			'id'=>1019,
			'country_id'=>11,
			'name'=>'Pernik'
		]);

		Province::create([
			'id'=>1020,
			'country_id'=>11,
			'name'=>'Pleven'
		]);

		Province::create([
			'id'=>1021,
			'country_id'=>11,
			'name'=>'Plovdiv'
		]);

		Province::create([
			'id'=>1022,
			'country_id'=>11,
			'name'=>'Razgrad'
		]);

		Province::create([
			'id'=>1023,
			'country_id'=>11,
			'name'=>'Ruse'
		]);

		Province::create([
			'id'=>1024,
			'country_id'=>11,
			'name'=>'Shumen'
		]);

		Province::create([
			'id'=>1025,
			'country_id'=>11,
			'name'=>'Silistra'
		]);

		Province::create([
			'id'=>1026,
			'country_id'=>11,
			'name'=>'Sliven'
		]);

		Province::create([
			'id'=>1027,
			'country_id'=>11,
			'name'=>'Smolyan'
		]);

		Province::create([
			'id'=>1028,
			'country_id'=>11,
			'name'=>'Sofiya'
		]);

		Province::create([
			'id'=>1029,
			'country_id'=>11,
			'name'=>'Stara Zagora'
		]);

		Province::create([
			'id'=>1030,
			'country_id'=>11,
			'name'=>'Turgovishte'
		]);

		Province::create([
			'id'=>1031,
			'country_id'=>11,
			'name'=>'Varna'
		]);

		Province::create([
			'id'=>1032,
			'country_id'=>11,
			'name'=>'Veliko Turnovo'
		]);

		Province::create([
			'id'=>1033,
			'country_id'=>11,
			'name'=>'Vidin'
		]);

		Province::create([
			'id'=>1034,
			'country_id'=>11,
			'name'=>'Vratsa'
		]);

		Province::create([
			'id'=>1035,
			'country_id'=>11,
			'name'=>'Yambol'
		]);

		Province::create([
			'id'=>1037,
			'country_id'=>71,
			'name'=>'Bjelovarsko-Bilogorska'
		]);

		Province::create([
			'id'=>1038,
			'country_id'=>71,
			'name'=>'Brodsko-Posavska'
		]);

		Province::create([
			'id'=>1039,
			'country_id'=>71,
			'name'=>'Dubrovacko-Neretvanska'
		]);

		Province::create([
			'id'=>1040,
			'country_id'=>71,
			'name'=>'Istarska'
		]);

		Province::create([
			'id'=>1041,
			'country_id'=>71,
			'name'=>'Karlovacka'
		]);

		Province::create([
			'id'=>1042,
			'country_id'=>71,
			'name'=>'Koprivnicko-Krizevacka'
		]);

		Province::create([
			'id'=>1043,
			'country_id'=>71,
			'name'=>'Krapinsko-Zagorska'
		]);

		Province::create([
			'id'=>1044,
			'country_id'=>71,
			'name'=>'Licko-Senjska'
		]);

		Province::create([
			'id'=>1045,
			'country_id'=>71,
			'name'=>'Medimurska'
		]);

		Province::create([
			'id'=>1046,
			'country_id'=>71,
			'name'=>'Osjecko-Baranjska'
		]);

		Province::create([
			'id'=>1047,
			'country_id'=>71,
			'name'=>'Pozesko-Slavonska'
		]);

		Province::create([
			'id'=>1048,
			'country_id'=>71,
			'name'=>'Primorsko-Goranska'
		]);

		Province::create([
			'id'=>1049,
			'country_id'=>71,
			'name'=>'Sibensko-Kninska'
		]);

		Province::create([
			'id'=>1050,
			'country_id'=>71,
			'name'=>'Sisacko-Moslavacka'
		]);

		Province::create([
			'id'=>1051,
			'country_id'=>71,
			'name'=>'Splitsko-Dalmatinska'
		]);

		Province::create([
			'id'=>1052,
			'country_id'=>71,
			'name'=>'Varazdinska'
		]);

		Province::create([
			'id'=>1053,
			'country_id'=>71,
			'name'=>'Viroviticko-Podravska'
		]);

		Province::create([
			'id'=>1054,
			'country_id'=>71,
			'name'=>'Vukovarsko-Srijemska'
		]);

		Province::create([
			'id'=>1055,
			'country_id'=>71,
			'name'=>'Zadarska'
		]);

		Province::create([
			'id'=>1056,
			'country_id'=>71,
			'name'=>'Zagrebacka'
		]);

		Province::create([
			'id'=>1057,
			'country_id'=>71,
			'name'=>'Grad Zagreb'
		]);

		Province::create([
			'id'=>1059,
			'country_id'=>143,
			'name'=>'Gibraltar'
		]);

		Province::create([
			'id'=>1060,
			'country_id'=>20,
			'name'=>'Evros'
		]);

		Province::create([
			'id'=>1061,
			'country_id'=>20,
			'name'=>'Rodhopi'
		]);

		Province::create([
			'id'=>1062,
			'country_id'=>20,
			'name'=>'Xanthi'
		]);

		Province::create([
			'id'=>1063,
			'country_id'=>20,
			'name'=>'Drama'
		]);

		Province::create([
			'id'=>1064,
			'country_id'=>20,
			'name'=>'Serrai'
		]);

		Province::create([
			'id'=>1065,
			'country_id'=>20,
			'name'=>'Kilkis'
		]);

		Province::create([
			'id'=>1066,
			'country_id'=>20,
			'name'=>'Pella'
		]);

		Province::create([
			'id'=>1067,
			'country_id'=>20,
			'name'=>'Florina'
		]);

		Province::create([
			'id'=>1068,
			'country_id'=>20,
			'name'=>'Kastoria'
		]);

		Province::create([
			'id'=>1069,
			'country_id'=>20,
			'name'=>'Grevena'
		]);

		Province::create([
			'id'=>1070,
			'country_id'=>20,
			'name'=>'Kozani'
		]);

		Province::create([
			'id'=>1071,
			'country_id'=>20,
			'name'=>'Imathia'
		]);

		Province::create([
			'id'=>1072,
			'country_id'=>20,
			'name'=>'Thessaloniki'
		]);

		Province::create([
			'id'=>1073,
			'country_id'=>20,
			'name'=>'Kavala'
		]);

		Province::create([
			'id'=>1074,
			'country_id'=>20,
			'name'=>'Khalkidhiki'
		]);

		Province::create([
			'id'=>1075,
			'country_id'=>20,
			'name'=>'Pieria'
		]);

		Province::create([
			'id'=>1076,
			'country_id'=>20,
			'name'=>'Ioannina'
		]);

		Province::create([
			'id'=>1077,
			'country_id'=>20,
			'name'=>'Thesprotia'
		]);

		Province::create([
			'id'=>1078,
			'country_id'=>20,
			'name'=>'Preveza'
		]);

		Province::create([
			'id'=>1079,
			'country_id'=>20,
			'name'=>'Arta'
		]);

		Province::create([
			'id'=>1080,
			'country_id'=>20,
			'name'=>'Larisa'
		]);

		Province::create([
			'id'=>1081,
			'country_id'=>20,
			'name'=>'Trikala'
		]);

		Province::create([
			'id'=>1082,
			'country_id'=>20,
			'name'=>'Kardhitsa'
		]);

		Province::create([
			'id'=>1083,
			'country_id'=>20,
			'name'=>'Magnisia'
		]);

		Province::create([
			'id'=>1084,
			'country_id'=>20,
			'name'=>'Kerkira'
		]);

		Province::create([
			'id'=>1085,
			'country_id'=>20,
			'name'=>'Levkas'
		]);

		Province::create([
			'id'=>1086,
			'country_id'=>20,
			'name'=>'Kefallinia'
		]);

		Province::create([
			'id'=>1087,
			'country_id'=>20,
			'name'=>'Zakinthos'
		]);

		Province::create([
			'id'=>1088,
			'country_id'=>20,
			'name'=>'Fthiotis'
		]);

		Province::create([
			'id'=>1089,
			'country_id'=>20,
			'name'=>'Evritania'
		]);

		Province::create([
			'id'=>1090,
			'country_id'=>20,
			'name'=>'Aitolia kai Akarnania'
		]);

		Province::create([
			'id'=>1091,
			'country_id'=>20,
			'name'=>'Fokis'
		]);

		Province::create([
			'id'=>1092,
			'country_id'=>20,
			'name'=>'Voiotia'
		]);

		Province::create([
			'id'=>1093,
			'country_id'=>20,
			'name'=>'Evvoia'
		]);

		Province::create([
			'id'=>1094,
			'country_id'=>20,
			'name'=>'Attiki'
		]);

		Province::create([
			'id'=>1095,
			'country_id'=>20,
			'name'=>'Argolis'
		]);

		Province::create([
			'id'=>1096,
			'country_id'=>20,
			'name'=>'Korinthia'
		]);

		Province::create([
			'id'=>1097,
			'country_id'=>20,
			'name'=>'Akhaia'
		]);

		Province::create([
			'id'=>1098,
			'country_id'=>20,
			'name'=>'Ilia'
		]);

		Province::create([
			'id'=>1099,
			'country_id'=>20,
			'name'=>'Messinia'
		]);

		Province::create([
			'id'=>1100,
			'country_id'=>20,
			'name'=>'Arkadhia'
		]);

		Province::create([
			'id'=>1101,
			'country_id'=>20,
			'name'=>'Lakonia'
		]);

		Province::create([
			'id'=>1102,
			'country_id'=>20,
			'name'=>'Khania'
		]);

		Province::create([
			'id'=>1103,
			'country_id'=>20,
			'name'=>'Rethimni'
		]);

		Province::create([
			'id'=>1104,
			'country_id'=>20,
			'name'=>'Iraklion'
		]);

		Province::create([
			'id'=>1105,
			'country_id'=>20,
			'name'=>'Lasithi'
		]);

		Province::create([
			'id'=>1106,
			'country_id'=>20,
			'name'=>'Dhodhekanisos'
		]);

		Province::create([
			'id'=>1107,
			'country_id'=>20,
			'name'=>'Samos'
		]);

		Province::create([
			'id'=>1108,
			'country_id'=>20,
			'name'=>'Kikladhes'
		]);

		Province::create([
			'id'=>1109,
			'country_id'=>20,
			'name'=>'Khios'
		]);

		Province::create([
			'id'=>1110,
			'country_id'=>20,
			'name'=>'Lesvos'
		]);

		Province::create([
			'id'=>1112,
			'country_id'=>14,
			'name'=>'Bacs-Kiskun'
		]);

		Province::create([
			'id'=>1113,
			'country_id'=>14,
			'name'=>'Baranya'
		]);

		Province::create([
			'id'=>1114,
			'country_id'=>14,
			'name'=>'Bekes'
		]);

		Province::create([
			'id'=>1115,
			'country_id'=>14,
			'name'=>'Borsod-Abauj-Zemplen'
		]);

		Province::create([
			'id'=>1116,
			'country_id'=>14,
			'name'=>'Budapest'
		]);

		Province::create([
			'id'=>1117,
			'country_id'=>14,
			'name'=>'Csongrad'
		]);

		Province::create([
			'id'=>1118,
			'country_id'=>14,
			'name'=>'Debrecen'
		]);

		Province::create([
			'id'=>1119,
			'country_id'=>14,
			'name'=>'Fejer'
		]);

		Province::create([
			'id'=>1120,
			'country_id'=>14,
			'name'=>'Gyor-Moson-Sopron'
		]);

		Province::create([
			'id'=>1121,
			'country_id'=>14,
			'name'=>'Hajdu-Bihar'
		]);

		Province::create([
			'id'=>1122,
			'country_id'=>14,
			'name'=>'Heves'
		]);

		Province::create([
			'id'=>1123,
			'country_id'=>14,
			'name'=>'Komarom-Esztergom'
		]);

		Province::create([
			'id'=>1124,
			'country_id'=>14,
			'name'=>'Miskolc'
		]);

		Province::create([
			'id'=>1125,
			'country_id'=>14,
			'name'=>'Nograd'
		]);

		Province::create([
			'id'=>1126,
			'country_id'=>14,
			'name'=>'Pecs'
		]);

		Province::create([
			'id'=>1127,
			'country_id'=>14,
			'name'=>'Pest'
		]);

		Province::create([
			'id'=>1128,
			'country_id'=>14,
			'name'=>'Somogy'
		]);

		Province::create([
			'id'=>1129,
			'country_id'=>14,
			'name'=>'Szabolcs-Szatmar-Bereg'
		]);

		Province::create([
			'id'=>1130,
			'country_id'=>14,
			'name'=>'Szeged'
		]);

		Province::create([
			'id'=>1131,
			'country_id'=>14,
			'name'=>'Jasz-Nagykun-Szolnok'
		]);

		Province::create([
			'id'=>1132,
			'country_id'=>14,
			'name'=>'Tolna'
		]);

		Province::create([
			'id'=>1133,
			'country_id'=>14,
			'name'=>'Vas'
		]);

		Province::create([
			'id'=>1134,
			'country_id'=>14,
			'name'=>'Veszprem'
		]);

		Province::create([
			'id'=>1135,
			'country_id'=>14,
			'name'=>'Zala'
		]);

		Province::create([
			'id'=>1136,
			'country_id'=>14,
			'name'=>'Gyor'
		]);

		Province::create([
			'id'=>1150,
			'country_id'=>14,
			'name'=>'Veszprem'
		]);

		Province::create([
			'id'=>1152,
			'country_id'=>126,
			'name'=>'Balzers'
		]);

		Province::create([
			'id'=>1153,
			'country_id'=>126,
			'name'=>'Eschen'
		]);

		Province::create([
			'id'=>1154,
			'country_id'=>126,
			'name'=>'Gamprin'
		]);

		Province::create([
			'id'=>1155,
			'country_id'=>126,
			'name'=>'Mauren'
		]);

		Province::create([
			'id'=>1156,
			'country_id'=>126,
			'name'=>'Planken'
		]);

		Province::create([
			'id'=>1157,
			'country_id'=>126,
			'name'=>'Ruggell'
		]);

		Province::create([
			'id'=>1158,
			'country_id'=>126,
			'name'=>'Schaan'
		]);

		Province::create([
			'id'=>1159,
			'country_id'=>126,
			'name'=>'Schellenberg'
		]);

		Province::create([
			'id'=>1160,
			'country_id'=>126,
			'name'=>'Triesen'
		]);

		Province::create([
			'id'=>1161,
			'country_id'=>126,
			'name'=>'Triesenberg'
		]);

		Province::create([
			'id'=>1162,
			'country_id'=>126,
			'name'=>'Vaduz'
		]);

		Province::create([
			'id'=>1163,
			'country_id'=>41,
			'name'=>'Diekirch'
		]);

		Province::create([
			'id'=>1164,
			'country_id'=>41,
			'name'=>'Grevenmacher'
		]);

		Province::create([
			'id'=>1165,
			'country_id'=>41,
			'name'=>'Luxembourg'
		]);

		Province::create([
			'id'=>1167,
			'country_id'=>85,
			'name'=>'Aracinovo'
		]);

		Province::create([
			'id'=>1168,
			'country_id'=>85,
			'name'=>'Bac'
		]);

		Province::create([
			'id'=>1169,
			'country_id'=>85,
			'name'=>'Belcista'
		]);

		Province::create([
			'id'=>1170,
			'country_id'=>85,
			'name'=>'Berovo'
		]);

		Province::create([
			'id'=>1171,
			'country_id'=>85,
			'name'=>'Bistrica'
		]);

		Province::create([
			'id'=>1172,
			'country_id'=>85,
			'name'=>'Bitola'
		]);

		Province::create([
			'id'=>1173,
			'country_id'=>85,
			'name'=>'Blatec'
		]);

		Province::create([
			'id'=>1174,
			'country_id'=>85,
			'name'=>'Bogdanci'
		]);

		Province::create([
			'id'=>1175,
			'country_id'=>85,
			'name'=>'Bogomila'
		]);

		Province::create([
			'id'=>1176,
			'country_id'=>85,
			'name'=>'Bogovinje'
		]);

		Province::create([
			'id'=>1177,
			'country_id'=>85,
			'name'=>'Bosilovo'
		]);

		Province::create([
			'id'=>1179,
			'country_id'=>85,
			'name'=>'Cair'
		]);

		Province::create([
			'id'=>1180,
			'country_id'=>85,
			'name'=>'Capari'
		]);

		Province::create([
			'id'=>1181,
			'country_id'=>85,
			'name'=>'Caska'
		]);

		Province::create([
			'id'=>1182,
			'country_id'=>85,
			'name'=>'Cegrane'
		]);

		Province::create([
			'id'=>1184,
			'country_id'=>85,
			'name'=>'Centar Zupa'
		]);

		Province::create([
			'id'=>1187,
			'country_id'=>85,
			'name'=>'Debar'
		]);

		Province::create([
			'id'=>1188,
			'country_id'=>85,
			'name'=>'Delcevo'
		]);

		Province::create([
			'id'=>1190,
			'country_id'=>85,
			'name'=>'Demir Hisar'
		]);

		Province::create([
			'id'=>1191,
			'country_id'=>85,
			'name'=>'Demir Kapija'
		]);

		Province::create([
			'id'=>1195,
			'country_id'=>85,
			'name'=>'Dorce Petrov'
		]);

		Province::create([
			'id'=>1198,
			'country_id'=>85,
			'name'=>'Gazi Baba'
		]);

		Province::create([
			'id'=>1199,
			'country_id'=>85,
			'name'=>'Gevgelija'
		]);

		Province::create([
			'id'=>1200,
			'country_id'=>85,
			'name'=>'Gostivar'
		]);		Province::create([
			'id'=>1201,
			'country_id'=>85,
			'name'=>'Gradsko'
		]);

		Province::create([
			'id'=>1204,
			'country_id'=>85,
			'name'=>'Jegunovce'
		]);

		Province::create([
			'id'=>1205,
			'country_id'=>85,
			'name'=>'Kamenjane'
		]);

		Province::create([
			'id'=>1207,
			'country_id'=>85,
			'name'=>'Karpos'
		]);

		Province::create([
			'id'=>1208,
			'country_id'=>85,
			'name'=>'Kavadarci'
		]);

		Province::create([
			'id'=>1209,
			'country_id'=>85,
			'name'=>'Kicevo'
		]);

		Province::create([
			'id'=>1210,
			'country_id'=>85,
			'name'=>'Kisela Voda'
		]);

		Province::create([
			'id'=>1211,
			'country_id'=>85,
			'name'=>'Klecevce'
		]);

		Province::create([
			'id'=>1212,
			'country_id'=>85,
			'name'=>'Kocani'
		]);

		Province::create([
			'id'=>1214,
			'country_id'=>85,
			'name'=>'Kondovo'
		]);

		Province::create([
			'id'=>1217,
			'country_id'=>85,
			'name'=>'Kratovo'
		]);

		Province::create([
			'id'=>1219,
			'country_id'=>85,
			'name'=>'Krivogastani'
		]);

		Province::create([
			'id'=>1220,
			'country_id'=>85,
			'name'=>'Krusevo'
		]);

		Province::create([
			'id'=>1223,
			'country_id'=>85,
			'name'=>'Kumanovo'
		]);

		Province::create([
			'id'=>1224,
			'country_id'=>85,
			'name'=>'Labunista'
		]);

		Province::create([
			'id'=>1225,
			'country_id'=>85,
			'name'=>'Lipkovo'
		]);

		Province::create([
			'id'=>1228,
			'country_id'=>85,
			'name'=>'Makedonska Kamenica'
		]);

		Province::create([
			'id'=>1229,
			'country_id'=>85,
			'name'=>'Makedonski Brod'
		]);

		Province::create([
			'id'=>1234,
			'country_id'=>85,
			'name'=>'Murtino'
		]);

		Province::create([
			'id'=>1235,
			'country_id'=>85,
			'name'=>'Negotino'
		]);

		Province::create([
			'id'=>1238,
			'country_id'=>85,
			'name'=>'Novo Selo'
		]);

		Province::create([
			'id'=>1240,
			'country_id'=>85,
			'name'=>'Ohrid'
		]);

		Province::create([
			'id'=>1242,
			'country_id'=>85,
			'name'=>'Orizari'
		]);

		Province::create([
			'id'=>1245,
			'country_id'=>85,
			'name'=>'Petrovec'
		]);

		Province::create([
			'id'=>1248,
			'country_id'=>85,
			'name'=>'Prilep'
		]);

		Province::create([
			'id'=>1249,
			'country_id'=>85,
			'name'=>'Probistip'
		]);

		Province::create([
			'id'=>1250,
			'country_id'=>85,
			'name'=>'Radovis'
		]);

		Province::create([
			'id'=>1252,
			'country_id'=>85,
			'name'=>'Resen'
		]);

		Province::create([
			'id'=>1253,
			'country_id'=>85,
			'name'=>'Rosoman'
		]);

		Province::create([
			'id'=>1256,
			'country_id'=>85,
			'name'=>'Saraj'
		]);

		Province::create([
			'id'=>1260,
			'country_id'=>85,
			'name'=>'Srbinovo'
		]);

		Province::create([
			'id'=>1262,
			'country_id'=>85,
			'name'=>'Star Dojran'
		]);

		Province::create([
			'id'=>1264,
			'country_id'=>85,
			'name'=>'Stip'
		]);

		Province::create([
			'id'=>1265,
			'country_id'=>85,
			'name'=>'Struga'
		]);

		Province::create([
			'id'=>1266,
			'country_id'=>85,
			'name'=>'Strumica'
		]);

		Province::create([
			'id'=>1267,
			'country_id'=>85,
			'name'=>'Studenicani'
		]);

		Province::create([
			'id'=>1268,
			'country_id'=>85,
			'name'=>'Suto Orizari'
		]);

		Province::create([
			'id'=>1269,
			'country_id'=>85,
			'name'=>'Sveti Nikole'
		]);

		Province::create([
			'id'=>1270,
			'country_id'=>85,
			'name'=>'Tearce'
		]);

		Province::create([
			'id'=>1271,
			'country_id'=>85,
			'name'=>'Tetovo'
		]);

		Province::create([
			'id'=>1273,
			'country_id'=>85,
			'name'=>'Valandovo'
		]);

		Province::create([
			'id'=>1275,
			'country_id'=>85,
			'name'=>'Veles'
		]);

		Province::create([
			'id'=>1277,
			'country_id'=>85,
			'name'=>'Vevcani'
		]);

		Province::create([
			'id'=>1278,
			'country_id'=>85,
			'name'=>'Vinica'
		]);

		Province::create([
			'id'=>1281,
			'country_id'=>85,
			'name'=>'Vrapciste'
		]);

		Province::create([
			'id'=>1286,
			'country_id'=>85,
			'name'=>'Zelino'
		]);

		Province::create([
			'id'=>1289,
			'country_id'=>85,
			'name'=>'Zrnovci'
		]);

		Province::create([
			'id'=>1291,
			'country_id'=>86,
			'name'=>'Malta'
		]);

		Province::create([
			'id'=>1292,
			'country_id'=>44,
			'name'=>'La Condamine'
		]);

		Province::create([
			'id'=>1293,
			'country_id'=>44,
			'name'=>'Monaco'
		]);

		Province::create([
			'id'=>1294,
			'country_id'=>44,
			'name'=>'Monte-Carlo'
		]);

		Province::create([
			'id'=>1295,
			'country_id'=>47,
			'name'=>'Biala Podlaska'
		]);

		Province::create([
			'id'=>1296,
			'country_id'=>47,
			'name'=>'Bialystok'
		]);

		Province::create([
			'id'=>1297,
			'country_id'=>47,
			'name'=>'Bielsko'
		]);

		Province::create([
			'id'=>1298,
			'country_id'=>47,
			'name'=>'Bydgoszcz'
		]);

		Province::create([
			'id'=>1299,
			'country_id'=>47,
			'name'=>'Chelm'
		]);

		Province::create([
			'id'=>1300,
			'country_id'=>47,
			'name'=>'Ciechanow'
		]);

		Province::create([
			'id'=>1301,
			'country_id'=>47,
			'name'=>'Czestochowa'
		]);

		Province::create([
			'id'=>1302,
			'country_id'=>47,
			'name'=>'Elblag'
		]);

		Province::create([
			'id'=>1303,
			'country_id'=>47,
			'name'=>'Gdansk'
		]);

		Province::create([
			'id'=>1304,
			'country_id'=>47,
			'name'=>'Gorzow'
		]);

		Province::create([
			'id'=>1305,
			'country_id'=>47,
			'name'=>'Jelenia Gora'
		]);

		Province::create([
			'id'=>1306,
			'country_id'=>47,
			'name'=>'Kalisz'
		]);

		Province::create([
			'id'=>1307,
			'country_id'=>47,
			'name'=>'Katowice'
		]);

		Province::create([
			'id'=>1308,
			'country_id'=>47,
			'name'=>'Kielce'
		]);

		Province::create([
			'id'=>1309,
			'country_id'=>47,
			'name'=>'Konin'
		]);

		Province::create([
			'id'=>1310,
			'country_id'=>47,
			'name'=>'Koszalin'
		]);

		Province::create([
			'id'=>1311,
			'country_id'=>47,
			'name'=>'Krakow'
		]);

		Province::create([
			'id'=>1312,
			'country_id'=>47,
			'name'=>'Krosno'
		]);

		Province::create([
			'id'=>1313,
			'country_id'=>47,
			'name'=>'Legnica'
		]);

		Province::create([
			'id'=>1314,
			'country_id'=>47,
			'name'=>'Leszno'
		]);

		Province::create([
			'id'=>1315,
			'country_id'=>47,
			'name'=>'Lodz'
		]);

		Province::create([
			'id'=>1316,
			'country_id'=>47,
			'name'=>'Lomza'
		]);

		Province::create([
			'id'=>1317,
			'country_id'=>47,
			'name'=>'Lublin'
		]);

		Province::create([
			'id'=>1318,
			'country_id'=>47,
			'name'=>'Nowy Sacz'
		]);

		Province::create([
			'id'=>1319,
			'country_id'=>47,
			'name'=>'Olsztyn'
		]);

		Province::create([
			'id'=>1320,
			'country_id'=>47,
			'name'=>'Opole'
		]);

		Province::create([
			'id'=>1321,
			'country_id'=>47,
			'name'=>'Ostroleka'
		]);

		Province::create([
			'id'=>1322,
			'country_id'=>47,
			'name'=>'Pila'
		]);

		Province::create([
			'id'=>1323,
			'country_id'=>47,
			'name'=>'Piotrkow'
		]);

		Province::create([
			'id'=>1324,
			'country_id'=>47,
			'name'=>'Plock'
		]);

		Province::create([
			'id'=>1325,
			'country_id'=>47,
			'name'=>'Poznan'
		]);

		Province::create([
			'id'=>1326,
			'country_id'=>47,
			'name'=>'Przemysl'
		]);

		Province::create([
			'id'=>1327,
			'country_id'=>47,
			'name'=>'Radom'
		]);

		Province::create([
			'id'=>1328,
			'country_id'=>47,
			'name'=>'Rzeszow'
		]);

		Province::create([
			'id'=>1329,
			'country_id'=>47,
			'name'=>'Siedlce'
		]);

		Province::create([
			'id'=>1330,
			'country_id'=>47,
			'name'=>'Sieradz'
		]);

		Province::create([
			'id'=>1331,
			'country_id'=>47,
			'name'=>'Skierniewice'
		]);

		Province::create([
			'id'=>1332,
			'country_id'=>47,
			'name'=>'Slupsk'
		]);

		Province::create([
			'id'=>1333,
			'country_id'=>47,
			'name'=>'Suwalki'
		]);

		Province::create([
			'id'=>1335,
			'country_id'=>47,
			'name'=>'Tarnobrzeg'
		]);

		Province::create([
			'id'=>1336,
			'country_id'=>47,
			'name'=>'Tarnow'
		]);

		Province::create([
			'id'=>1337,
			'country_id'=>47,
			'name'=>'Torun'
		]);

		Province::create([
			'id'=>1338,
			'country_id'=>47,
			'name'=>'Walbrzych'
		]);

		Province::create([
			'id'=>1339,
			'country_id'=>47,
			'name'=>'Warszawa'
		]);

		Province::create([
			'id'=>1340,
			'country_id'=>47,
			'name'=>'Wloclawek'
		]);

		Province::create([
			'id'=>1341,
			'country_id'=>47,
			'name'=>'Wroclaw'
		]);

		Province::create([
			'id'=>1342,
			'country_id'=>47,
			'name'=>'Zamosc'
		]);

		Province::create([
			'id'=>1343,
			'country_id'=>47,
			'name'=>'Zielona Gora'
		]);

		Province::create([
			'id'=>1344,
			'country_id'=>47,
			'name'=>'Dolnoslaskie'
		]);

		Province::create([
			'id'=>1345,
			'country_id'=>47,
			'name'=>'Kujawsko-Pomorskie'
		]);

		Province::create([
			'id'=>1346,
			'country_id'=>47,
			'name'=>'Lodzkie'
		]);

		Province::create([
			'id'=>1347,
			'country_id'=>47,
			'name'=>'Lubelskie'
		]);

		Province::create([
			'id'=>1348,
			'country_id'=>47,
			'name'=>'Lubuskie'
		]);

		Province::create([
			'id'=>1349,
			'country_id'=>47,
			'name'=>'Malopolskie'
		]);

		Province::create([
			'id'=>1350,
			'country_id'=>47,
			'name'=>'Mazowieckie'
		]);

		Province::create([
			'id'=>1351,
			'country_id'=>47,
			'name'=>'Opolskie'
		]);

		Province::create([
			'id'=>1352,
			'country_id'=>47,
			'name'=>'Podkarpackie'
		]);

		Province::create([
			'id'=>1353,
			'country_id'=>47,
			'name'=>'Podlaskie'
		]);

		Province::create([
			'id'=>1354,
			'country_id'=>47,
			'name'=>'Pomorskie'
		]);

		Province::create([
			'id'=>1355,
			'country_id'=>47,
			'name'=>'Slaskie'
		]);

		Province::create([
			'id'=>1356,
			'country_id'=>47,
			'name'=>'Swietokrzyskie'
		]);

		Province::create([
			'id'=>1357,
			'country_id'=>47,
			'name'=>'Warminsko-Mazurskie'
		]);

		Province::create([
			'id'=>1358,
			'country_id'=>47,
			'name'=>'Wielkopolskie'
		]);

		Province::create([
			'id'=>1359,
			'country_id'=>47,
			'name'=>'Zachodniopomorskie'
		]);

		Province::create([
			'id'=>1361,
			'country_id'=>72,
			'name'=>'Alba'
		]);

		Province::create([
			'id'=>1362,
			'country_id'=>72,
			'name'=>'Arad'
		]);

		Province::create([
			'id'=>1363,
			'country_id'=>72,
			'name'=>'Arges'
		]);

		Province::create([
			'id'=>1364,
			'country_id'=>72,
			'name'=>'Bacau'
		]);

		Province::create([
			'id'=>1365,
			'country_id'=>72,
			'name'=>'Bihor'
		]);

		Province::create([
			'id'=>1366,
			'country_id'=>72,
			'name'=>'Bistrita-Nasaud'
		]);

		Province::create([
			'id'=>1367,
			'country_id'=>72,
			'name'=>'Botosani'
		]);

		Province::create([
			'id'=>1368,
			'country_id'=>72,
			'name'=>'Braila'
		]);

		Province::create([
			'id'=>1369,
			'country_id'=>72,
			'name'=>'Brasov'
		]);

		Province::create([
			'id'=>1370,
			'country_id'=>72,
			'name'=>'Bucuresti'
		]);

		Province::create([
			'id'=>1371,
			'country_id'=>72,
			'name'=>'Buzau'
		]);

		Province::create([
			'id'=>1372,
			'country_id'=>72,
			'name'=>'Caras-Severin'
		]);

		Province::create([
			'id'=>1373,
			'country_id'=>72,
			'name'=>'Cluj'
		]);

		Province::create([
			'id'=>1374,
			'country_id'=>72,
			'name'=>'Constanta'
		]);

		Province::create([
			'id'=>1375,
			'country_id'=>72,
			'name'=>'Covasna'
		]);

		Province::create([
			'id'=>1376,
			'country_id'=>72,
			'name'=>'Dambovita'
		]);

		Province::create([
			'id'=>1377,
			'country_id'=>72,
			'name'=>'Dolj'
		]);

		Province::create([
			'id'=>1378,
			'country_id'=>72,
			'name'=>'Galati'
		]);

		Province::create([
			'id'=>1379,
			'country_id'=>72,
			'name'=>'Gorj'
		]);

		Province::create([
			'id'=>1380,
			'country_id'=>72,
			'name'=>'Harghita'
		]);

		Province::create([
			'id'=>1381,
			'country_id'=>72,
			'name'=>'Hunedoara'
		]);

		Province::create([
			'id'=>1382,
			'country_id'=>72,
			'name'=>'Ialomita'
		]);

		Province::create([
			'id'=>1383,
			'country_id'=>72,
			'name'=>'Iasi'
		]);

		Province::create([
			'id'=>1384,
			'country_id'=>72,
			'name'=>'Maramures'
		]);

		Province::create([
			'id'=>1385,
			'country_id'=>72,
			'name'=>'Mehedinti'
		]);

		Province::create([
			'id'=>1386,
			'country_id'=>72,
			'name'=>'Mures'
		]);

		Province::create([
			'id'=>1387,
			'country_id'=>72,
			'name'=>'Neamt'
		]);

		Province::create([
			'id'=>1388,
			'country_id'=>72,
			'name'=>'Olt'
		]);

		Province::create([
			'id'=>1389,
			'country_id'=>72,
			'name'=>'Prahova'
		]);

		Province::create([
			'id'=>1390,
			'country_id'=>72,
			'name'=>'Salaj'
		]);

		Province::create([
			'id'=>1391,
			'country_id'=>72,
			'name'=>'Satu Mare'
		]);

		Province::create([
			'id'=>1392,
			'country_id'=>72,
			'name'=>'Sibiu'
		]);

		Province::create([
			'id'=>1393,
			'country_id'=>72,
			'name'=>'Suceava'
		]);

		Province::create([
			'id'=>1394,
			'country_id'=>72,
			'name'=>'Teleorman'
		]);

		Province::create([
			'id'=>1395,
			'country_id'=>72,
			'name'=>'Timis'
		]);

		Province::create([
			'id'=>1396,
			'country_id'=>72,
			'name'=>'Tulcea'
		]);

		Province::create([
			'id'=>1397,
			'country_id'=>72,
			'name'=>'Vaslui'
		]);

		Province::create([
			'id'=>1398,
			'country_id'=>72,
			'name'=>'Valcea'
		]);

		Province::create([
			'id'=>1399,
			'country_id'=>72,
			'name'=>'Vrancea'
		]);

		Province::create([
			'id'=>1400,
			'country_id'=>72,
			'name'=>'Calarasi'
		]);

		Province::create([
			'id'=>1401,
			'country_id'=>72,
			'name'=>'Giurgiu'
		]);

		Province::create([
			'id'=>1404,
			'country_id'=>224,
			'name'=>'Acquaviva'
		]);

		Province::create([
			'id'=>1405,
			'country_id'=>224,
			'name'=>'Chiesanuova'
		]);

		Province::create([
			'id'=>1406,
			'country_id'=>224,
			'name'=>'Domagnano'
		]);

		Province::create([
			'id'=>1407,
			'country_id'=>224,
			'name'=>'Faetano'
		]);

		Province::create([
			'id'=>1408,
			'country_id'=>224,
			'name'=>'Fiorentino'
		]);

		Province::create([
			'id'=>1409,
			'country_id'=>224,
			'name'=>'Borgo Maggiore'
		]);

		Province::create([
			'id'=>1410,
			'country_id'=>224,
			'name'=>'San Marino'
		]);

		Province::create([
			'id'=>1411,
			'country_id'=>224,
			'name'=>'Monte Giardino'
		]);

		Province::create([
			'id'=>1412,
			'country_id'=>224,
			'name'=>'Serravalle'
		]);

		Province::create([
			'id'=>1413,
			'country_id'=>52,
			'name'=>'Banska Bystrica'
		]);

		Province::create([
			'id'=>1414,
			'country_id'=>52,
			'name'=>'Bratislava'
		]);

		Province::create([
			'id'=>1415,
			'country_id'=>52,
			'name'=>'Kosice'
		]);

		Province::create([
			'id'=>1416,
			'country_id'=>52,
			'name'=>'Nitra'
		]);

		Province::create([
			'id'=>1417,
			'country_id'=>52,
			'name'=>'Presov'
		]);

		Province::create([
			'id'=>1418,
			'country_id'=>52,
			'name'=>'Trencin'
		]);

		Province::create([
			'id'=>1419,
			'country_id'=>52,
			'name'=>'Trnava'
		]);

		Province::create([
			'id'=>1420,
			'country_id'=>52,
			'name'=>'Zilina'
		]);

		Province::create([
			'id'=>1423,
			'country_id'=>53,
			'name'=>'Beltinci'
		]);

		Province::create([
			'id'=>1425,
			'country_id'=>53,
			'name'=>'Bohinj'
		]);

		Province::create([
			'id'=>1426,
			'country_id'=>53,
			'name'=>'Borovnica'
		]);

		Province::create([
			'id'=>1427,
			'country_id'=>53,
			'name'=>'Bovec'
		]);

		Province::create([
			'id'=>1428,
			'country_id'=>53,
			'name'=>'Brda'
		]);

		Province::create([
			'id'=>1429,
			'country_id'=>53,
			'name'=>'Brezice'
		]);

		Province::create([
			'id'=>1430,
			'country_id'=>53,
			'name'=>'Brezovica'
		]);

		Province::create([
			'id'=>1432,
			'country_id'=>53,
			'name'=>'Cerklje na Gorenjskem'
		]);

		Province::create([
			'id'=>1434,
			'country_id'=>53,
			'name'=>'Cerkno'
		]);

		Province::create([
			'id'=>1436,
			'country_id'=>53,
			'name'=>'Crna na Koroskem'
		]);

		Province::create([
			'id'=>1437,
			'country_id'=>53,
			'name'=>'Crnomelj'
		]);

		Province::create([
			'id'=>1438,
			'country_id'=>53,
			'name'=>'Divaca'
		]);

		Province::create([
			'id'=>1439,
			'country_id'=>53,
			'name'=>'Dobrepolje'
		]);

		Province::create([
			'id'=>1440,
			'country_id'=>53,
			'name'=>'Dol pri Ljubljani'
		]);

		Province::create([
			'id'=>1443,
			'country_id'=>53,
			'name'=>'Duplek'
		]);

		Province::create([
			'id'=>1447,
			'country_id'=>53,
			'name'=>'Gornji Grad'
		]);

		Province::create([
			'id'=>1450,
			'country_id'=>53,
			'name'=>'Hrastnik'
		]);

		Province::create([
			'id'=>1451,
			'country_id'=>53,
			'name'=>'Hrpelje-Kozina'
		]);

		Province::create([
			'id'=>1452,
			'country_id'=>53,
			'name'=>'Idrija'
		]);

		Province::create([
			'id'=>1453,
			'country_id'=>53,
			'name'=>'Ig'
		]);

		Province::create([
			'id'=>1454,
			'country_id'=>53,
			'name'=>'Ilirska Bistrica'
		]);

		Province::create([
			'id'=>1455,
			'country_id'=>53,
			'name'=>'Ivancna Gorica'
		]);

		Province::create([
			'id'=>1462,
			'country_id'=>53,
			'name'=>'Komen'
		]);

		Province::create([
			'id'=>1463,
			'country_id'=>53,
			'name'=>'Koper-Capodistria'
		]);

		Province::create([
			'id'=>1464,
			'country_id'=>53,
			'name'=>'Kozje'
		]);

		Province::create([
			'id'=>1465,
			'country_id'=>53,
			'name'=>'Kranj'
		]);

		Province::create([
			'id'=>1466,
			'country_id'=>53,
			'name'=>'Kranjska Gora'
		]);

		Province::create([
			'id'=>1467,
			'country_id'=>53,
			'name'=>'Krsko'
		]);

		Province::create([
			'id'=>1469,
			'country_id'=>53,
			'name'=>'Lasko'
		]);

		Province::create([
			'id'=>1470,
			'country_id'=>53,
			'name'=>'Ljubljana'
		]);

		Province::create([
			'id'=>1471,
			'country_id'=>53,
			'name'=>'Ljubno'
		]);

		Province::create([
			'id'=>1472,
			'country_id'=>53,
			'name'=>'Logatec'
		]);

		Province::create([
			'id'=>1475,
			'country_id'=>53,
			'name'=>'Medvode'
		]);

		Province::create([
			'id'=>1476,
			'country_id'=>53,
			'name'=>'Menges'
		]);

		Province::create([
			'id'=>1478,
			'country_id'=>53,
			'name'=>'Mezica'
		]);

		Province::create([
			'id'=>1480,
			'country_id'=>53,
			'name'=>'Moravce'
		]);

		Province::create([
			'id'=>1482,
			'country_id'=>53,
			'name'=>'Mozirje'
		]);

		Province::create([
			'id'=>1483,
			'country_id'=>53,
			'name'=>'Murska Sobota'
		]);

		Province::create([
			'id'=>1487,
			'country_id'=>53,
			'name'=>'Nova Gorica'
		]);

		Province::create([
			'id'=>1489,
			'country_id'=>53,
			'name'=>'Ormoz'
		]);

		Province::create([
			'id'=>1491,
			'country_id'=>53,
			'name'=>'Pesnica'
		]);

		Province::create([
			'id'=>1494,
			'country_id'=>53,
			'name'=>'Postojna'
		]);

		Province::create([
			'id'=>1497,
			'country_id'=>53,
			'name'=>'Radece'
		]);

		Province::create([
			'id'=>1498,
			'country_id'=>53,
			'name'=>'Radenci'
		]);

		Province::create([
			'id'=>1500,
			'country_id'=>53,
			'name'=>'Radovljica'
		]);

		Province::create([
			'id'=>1502,
			'country_id'=>53,
			'name'=>'Rogaska Slatina'
		]);

		Province::create([
			'id'=>1505,
			'country_id'=>53,
			'name'=>'Sencur'
		]);

		Province::create([
			'id'=>1506,
			'country_id'=>53,
			'name'=>'Sentilj'
		]);

		Province::create([
			'id'=>1508,
			'country_id'=>53,
			'name'=>'Sevnica'
		]);

		Province::create([
			'id'=>1509,
			'country_id'=>53,
			'name'=>'Sezana'
		]);

		Province::create([
			'id'=>1511,
			'country_id'=>53,
			'name'=>'Skofja Loka'
		]);

		Province::create([
			'id'=>1513,
			'country_id'=>53,
			'name'=>'Slovenj Gradec'
		]);

		Province::create([
			'id'=>1514,
			'country_id'=>53,
			'name'=>'Slovenske Konjice'
		]);

		Province::create([
			'id'=>1515,
			'country_id'=>53,
			'name'=>'Smarje pri Jelsah'
		]);

		Province::create([
			'id'=>1521,
			'country_id'=>53,
			'name'=>'Tolmin'
		]);

		Province::create([
			'id'=>1522,
			'country_id'=>53,
			'name'=>'Trbovlje'
		]);

		Province::create([
			'id'=>1524,
			'country_id'=>53,
			'name'=>'Trzic'
		]);

		Province::create([
			'id'=>1526,
			'country_id'=>53,
			'name'=>'Velenje'
		]);

		Province::create([
			'id'=>1528,
			'country_id'=>53,
			'name'=>'Vipava'
		]);

		Province::create([
			'id'=>1531,
			'country_id'=>53,
			'name'=>'Vrhnika'
		]);

		Province::create([
			'id'=>1532,
			'country_id'=>53,
			'name'=>'Vuzenica'
		]);

		Province::create([
			'id'=>1533,
			'country_id'=>53,
			'name'=>'Zagorje ob Savi'
		]);

		Province::create([
			'id'=>1535,
			'country_id'=>53,
			'name'=>'Zelezniki'
		]);

		Province::create([
			'id'=>1536,
			'country_id'=>53,
			'name'=>'Ziri'
		]);

		Province::create([
			'id'=>1537,
			'country_id'=>53,
			'name'=>'Zrece'
		]);

		Province::create([
			'id'=>1539,
			'country_id'=>53,
			'name'=>'Domzale'
		]);

		Province::create([
			'id'=>1540,
			'country_id'=>53,
			'name'=>'Jesenice'
		]);

		Province::create([
			'id'=>1541,
			'country_id'=>53,
			'name'=>'Kamnik'
		]);

		Province::create([
			'id'=>1542,
			'country_id'=>53,
			'name'=>'Kocevje'
		]);

		Province::create([
			'id'=>1544,
			'country_id'=>53,
			'name'=>'Lenart'
		]);

		Province::create([
			'id'=>1545,
			'country_id'=>53,
			'name'=>'Litija'
		]);

		Province::create([
			'id'=>1546,
			'country_id'=>53,
			'name'=>'Ljutomer'
		]);

		Province::create([
			'id'=>1550,
			'country_id'=>53,
			'name'=>'Maribor'
		]);

		Province::create([
			'id'=>1552,
			'country_id'=>53,
			'name'=>'Novo Mesto'
		]);

		Province::create([
			'id'=>1553,
			'country_id'=>53,
			'name'=>'Piran'
		]);

		Province::create([
			'id'=>1554,
			'country_id'=>53,
			'name'=>'Preddvor'
		]);

		Province::create([
			'id'=>1555,
			'country_id'=>53,
			'name'=>'Ptuj'
		]);

		Province::create([
			'id'=>1556,
			'country_id'=>53,
			'name'=>'Ribnica'
		]);

		Province::create([
			'id'=>1558,
			'country_id'=>53,
			'name'=>'Sentjur pri Celju'
		]);

		Province::create([
			'id'=>1559,
			'country_id'=>53,
			'name'=>'Slovenska Bistrica'
		]);

		Province::create([
			'id'=>1560,
			'country_id'=>53,
			'name'=>'Videm'
		]);

		Province::create([
			'id'=>1562,
			'country_id'=>53,
			'name'=>'Zalec'
		]);

		Province::create([
			'id'=>1564,
			'country_id'=>109,
			'name'=>'Seychelles'
		]);

		Province::create([
			'id'=>1565,
			'country_id'=>108,
			'name'=>'Mauritania'
		]);

		Province::create([
			'id'=>1566,
			'country_id'=>135,
			'name'=>'Senegal'
		]);

		Province::create([
			'id'=>1567,
			'country_id'=>154,
			'name'=>'Road Town'
		]);

		Province::create([
			'id'=>1568,
			'country_id'=>165,
			'name'=>'Congo'
		]);

		Province::create([
			'id'=>1569,
			'country_id'=>166,
			'name'=>'Avarua'
		]);

		Province::create([
			'id'=>1570,
			'country_id'=>172,
			'name'=>'Malabo'
		]);

		Province::create([
			'id'=>1571,
			'country_id'=>175,
			'name'=>'Torshavn'
		]);

		Province::create([
			'id'=>1572,
			'country_id'=>178,
			'name'=>'Papeete'
		]);

		Province::create([
			'id'=>1573,
			'country_id'=>184,
			'name'=>'St Georges'
		]);

		Province::create([
			'id'=>1574,
			'country_id'=>186,
			'name'=>'St Peter Port'
		]);

		Province::create([
			'id'=>1575,
			'country_id'=>188,
			'name'=>'Bissau'
		]);

		Province::create([
			'id'=>1576,
			'country_id'=>193,
			'name'=>'Saint Helier'
		]);

		Province::create([
			'id'=>1577,
			'country_id'=>201,
			'name'=>'Fort-de-France'
		]);

		Province::create([
			'id'=>1578,
			'country_id'=>207,
			'name'=>'Willemstad'
		]);

		Province::create([
			'id'=>1579,
			'country_id'=>208,
			'name'=>'Noumea'
		]);

		Province::create([
			'id'=>1580,
			'country_id'=>212,
			'name'=>'Kingston'
		]);

		Province::create([
			'id'=>1581,
			'country_id'=>215,
			'name'=>'Adamstown'
		]);

		Province::create([
			'id'=>1582,
			'country_id'=>216,
			'name'=>'Doha'
		]);

		Province::create([
			'id'=>1583,
			'country_id'=>218,
			'name'=>'Jamestown'
		]);

		Province::create([
			'id'=>1584,
			'country_id'=>219,
			'name'=>'Basseterre'
		]);

		Province::create([
			'id'=>1585,
			'country_id'=>220,
			'name'=>'Castries'
		]);

		Province::create([
			'id'=>1586,
			'country_id'=>221,
			'name'=>'Saint Pierre'
		]);

		Province::create([
			'id'=>1587,
			'country_id'=>222,
			'name'=>'Kingstown'
		]);

		Province::create([
			'id'=>1588,
			'country_id'=>225,
			'name'=>'San Tome'
		]);

		Province::create([
			'id'=>1589,
			'country_id'=>226,
			'name'=>'Belgrade'
		]);

		Province::create([
			'id'=>1590,
			'country_id'=>227,
			'name'=>'Freetown'
		]);

		Province::create([
			'id'=>1591,
			'country_id'=>229,
			'name'=>'Mogadishu'
		]);

		Province::create([
			'id'=>1592,
			'country_id'=>235,
			'name'=>'Fakaofo'
		]);

		Province::create([
			'id'=>1593,
			'country_id'=>237,
			'name'=>'Port of Spain'
		]);

		Province::create([
			'id'=>1594,
			'country_id'=>241,
			'name'=>'Mata-Utu'
		]);

		Province::create([
			'id'=>1596,
			'country_id'=>89,
			'name'=>'Amazonas'
		]);

		Province::create([
			'id'=>1597,
			'country_id'=>89,
			'name'=>'Ancash'
		]);

		Province::create([
			'id'=>1598,
			'country_id'=>89,
			'name'=>'Apurímac'
		]);

		Province::create([
			'id'=>1599,
			'country_id'=>89,
			'name'=>'Arequipa'
		]);

		Province::create([
			'id'=>1600,
			'country_id'=>89,
			'name'=>'Ayacucho'
		]);

		Province::create([
			'id'=>1601,
			'country_id'=>89,
			'name'=>'Cajamarca'
		]);

		Province::create([
			'id'=>1602,
			'country_id'=>89,
			'name'=>'Callao'
		]);

		Province::create([
			'id'=>1603,
			'country_id'=>89,
			'name'=>'Cusco'
		]);

		Province::create([
			'id'=>1604,
			'country_id'=>89,
			'name'=>'Huancavelica'
		]);

		Province::create([
			'id'=>1605,
			'country_id'=>89,
			'name'=>'Huánuco'
		]);

		Province::create([
			'id'=>1606,
			'country_id'=>89,
			'name'=>'Ica'
		]);

		Province::create([
			'id'=>1607,
			'country_id'=>89,
			'name'=>'Junín'
		]);

		Province::create([
			'id'=>1608,
			'country_id'=>89,
			'name'=>'La Libertad'
		]);

		Province::create([
			'id'=>1609,
			'country_id'=>89,
			'name'=>'Lambayeque'
		]);

		Province::create([
			'id'=>1610,
			'country_id'=>89,
			'name'=>'Lima'
		]);

		Province::create([
			'id'=>1611,
			'country_id'=>89,
			'name'=>'Loreto'
		]);

		Province::create([
			'id'=>1612,
			'country_id'=>89,
			'name'=>'Madre de Dios'
		]);

		Province::create([
			'id'=>1613,
			'country_id'=>89,
			'name'=>'Moquegua'
		]);

		Province::create([
			'id'=>1614,
			'country_id'=>89,
			'name'=>'Pasco'
		]);

		Province::create([
			'id'=>1615,
			'country_id'=>89,
			'name'=>'Piura'
		]);

		Province::create([
			'id'=>1616,
			'country_id'=>89,
			'name'=>'Puno'
		]);

		Province::create([
			'id'=>1617,
			'country_id'=>89,
			'name'=>'San Martín'
		]);

		Province::create([
			'id'=>1618,
			'country_id'=>89,
			'name'=>'Tacna'
		]);

		Province::create([
			'id'=>1619,
			'country_id'=>89,
			'name'=>'Tumbes'
		]);

		Province::create([
			'id'=>1620,
			'country_id'=>89,
			'name'=>'Ucayali'
		]);

		Province::create([
			'id'=>1622,
			'country_id'=>110,
			'name'=>'Alto Paraná'
		]);

		Province::create([
			'id'=>1623,
			'country_id'=>110,
			'name'=>'Amambay'
		]);

		Province::create([
			'id'=>1624,
			'country_id'=>110,
			'name'=>'Boquerón'
		]);

		Province::create([
			'id'=>1625,
			'country_id'=>110,
			'name'=>'Caaguazú'
		]);

		Province::create([
			'id'=>1626,
			'country_id'=>110,
			'name'=>'Caazapá'
		]);

		Province::create([
			'id'=>1627,
			'country_id'=>110,
			'name'=>'Central'
		]);

		Province::create([
			'id'=>1628,
			'country_id'=>110,
			'name'=>'Concepción'
		]);

		Province::create([
			'id'=>1629,
			'country_id'=>110,
			'name'=>'Cordillera'
		]);

		Province::create([
			'id'=>1630,
			'country_id'=>110,
			'name'=>'Guairá'
		]);

		Province::create([
			'id'=>1631,
			'country_id'=>110,
			'name'=>'Itapúa'
		]);

		Province::create([
			'id'=>1632,
			'country_id'=>110,
			'name'=>'Misiones'
		]);

		Province::create([
			'id'=>1633,
			'country_id'=>110,
			'name'=>'Neembucú'
		]);

		Province::create([
			'id'=>1634,
			'country_id'=>110,
			'name'=>'Paraguarí'
		]);

		Province::create([
			'id'=>1635,
			'country_id'=>110,
			'name'=>'Presidente Hayes'
		]);

		Province::create([
			'id'=>1636,
			'country_id'=>110,
			'name'=>'San Pedro'
		]);

		Province::create([
			'id'=>1637,
			'country_id'=>110,
			'name'=>'Alto Paraguay'
		]);

		Province::create([
			'id'=>1638,
			'country_id'=>110,
			'name'=>'Canindeyú'
		]);

		Province::create([
			'id'=>1639,
			'country_id'=>110,
			'name'=>'Chaco'
		]);

		Province::create([
			'id'=>1642,
			'country_id'=>111,
			'name'=>'Artigas'
		]);

		Province::create([
			'id'=>1643,
			'country_id'=>111,
			'name'=>'Canelones'
		]);

		Province::create([
			'id'=>1644,
			'country_id'=>111,
			'name'=>'Cerro Largo'
		]);

		Province::create([
			'id'=>1645,
			'country_id'=>111,
			'name'=>'Colonia'
		]);

		Province::create([
			'id'=>1646,
			'country_id'=>111,
			'name'=>'Durazno'
		]);

		Province::create([
			'id'=>1647,
			'country_id'=>111,
			'name'=>'Flores'
		]);

		Province::create([
			'id'=>1648,
			'country_id'=>111,
			'name'=>'Florida'
		]);

		Province::create([
			'id'=>1649,
			'country_id'=>111,
			'name'=>'Lavalleja'
		]);

		Province::create([
			'id'=>1650,
			'country_id'=>111,
			'name'=>'Maldonado'
		]);

		Province::create([
			'id'=>1651,
			'country_id'=>111,
			'name'=>'Montevideo'
		]);

		Province::create([
			'id'=>1652,
			'country_id'=>111,
			'name'=>'Paysandú'
		]);

		Province::create([
			'id'=>1653,
			'country_id'=>111,
			'name'=>'Río Negro'
		]);

		Province::create([
			'id'=>1654,
			'country_id'=>111,
			'name'=>'Rivera'
		]);

		Province::create([
			'id'=>1655,
			'country_id'=>111,
			'name'=>'Rocha'
		]);

		Province::create([
			'id'=>1656,
			'country_id'=>111,
			'name'=>'Salto'
		]);

		Province::create([
			'id'=>1657,
			'country_id'=>111,
			'name'=>'San José'
		]);

		Province::create([
			'id'=>1658,
			'country_id'=>111,
			'name'=>'Soriano'
		]);

		Province::create([
			'id'=>1659,
			'country_id'=>111,
			'name'=>'Tacuarembó'
		]);

		Province::create([
			'id'=>1660,
			'country_id'=>111,
			'name'=>'Treinta y Tres'
		]);

		Province::create([
			'id'=>1662,
			'country_id'=>81,
			'name'=>'Valparaíso'
		]);

		Province::create([
			'id'=>1663,
			'country_id'=>81,
			'name'=>'Aisén del General Carlos Ibánez del Campo'
		]);

		Province::create([
			'id'=>1664,
			'country_id'=>81,
			'name'=>'Antofagasta'
		]);

		Province::create([
			'id'=>1665,
			'country_id'=>81,
			'name'=>'Araucanía'
		]);

		Province::create([
			'id'=>1666,
			'country_id'=>81,
			'name'=>'Atacama'
		]);

		Province::create([
			'id'=>1667,
			'country_id'=>81,
			'name'=>'Bío-Bío'
		]);

		Province::create([
			'id'=>1668,
			'country_id'=>81,
			'name'=>'Coquimbo'
		]);

		Province::create([
			'id'=>1669,
			'country_id'=>81,
			'name'=>'Libertador General Bernardo OHiggins'
		]);

		Province::create([
			'id'=>1670,
			'country_id'=>81,
			'name'=>'Los Lagos'
		]);

		Province::create([
			'id'=>1671,
			'country_id'=>81,
			'name'=>'Magallanes y de la Antártica Chilena'
		]);

		Province::create([
			'id'=>1672,
			'country_id'=>81,
			'name'=>'Maule'
		]);

		Province::create([
			'id'=>1673,
			'country_id'=>81,
			'name'=>'Region Metropolitana'
		]);

		Province::create([
			'id'=>1674,
			'country_id'=>81,
			'name'=>'Tarapacá'
		]);

		Province::create([
			'id'=>1676,
			'country_id'=>185,
			'name'=>'Alta Verapaz'
		]);

		Province::create([
			'id'=>1677,
			'country_id'=>185,
			'name'=>'Baja Verapaz'
		]);

		Province::create([
			'id'=>1678,
			'country_id'=>185,
			'name'=>'Chimaltenango'
		]);

		Province::create([
			'id'=>1679,
			'country_id'=>185,
			'name'=>'Chiquimula'
		]);

		Province::create([
			'id'=>1680,
			'country_id'=>185,
			'name'=>'El Progreso'
		]);

		Province::create([
			'id'=>1681,
			'country_id'=>185,
			'name'=>'Escuintla'
		]);

		Province::create([
			'id'=>1682,
			'country_id'=>185,
			'name'=>'Guatemala'
		]);

		Province::create([
			'id'=>1683,
			'country_id'=>185,
			'name'=>'Huehuetenango'
		]);

		Province::create([
			'id'=>1684,
			'country_id'=>185,
			'name'=>'Izabal'
		]);

		Province::create([
			'id'=>1685,
			'country_id'=>185,
			'name'=>'Jalapa'
		]);

		Province::create([
			'id'=>1686,
			'country_id'=>185,
			'name'=>'Jutiapa'
		]);

		Province::create([
			'id'=>1687,
			'country_id'=>185,
			'name'=>'Petén'
		]);

		Province::create([
			'id'=>1688,
			'country_id'=>185,
			'name'=>'Quetzaltenango'
		]);

		Province::create([
			'id'=>1689,
			'country_id'=>185,
			'name'=>'Quiché'
		]);

		Province::create([
			'id'=>1690,
			'country_id'=>185,
			'name'=>'Retalhuleu'
		]);

		Province::create([
			'id'=>1691,
			'country_id'=>185,
			'name'=>'Sacatepéquez'
		]);

		Province::create([
			'id'=>1692,
			'country_id'=>185,
			'name'=>'San Marcos'
		]);

		Province::create([
			'id'=>1693,
			'country_id'=>185,
			'name'=>'Santa Rosa'
		]);

		Province::create([
			'id'=>1694,
			'country_id'=>185,
			'name'=>'Sololá'
		]);

		Province::create([
			'id'=>1695,
			'country_id'=>185,
			'name'=>'Suchitepequez'
		]);

		Province::create([
			'id'=>1696,
			'country_id'=>185,
			'name'=>'Totonicapán'
		]);

		Province::create([
			'id'=>1697,
			'country_id'=>185,
			'name'=>'Zacapa'
		]);

		Province::create([
			'id'=>1699,
			'country_id'=>82,
			'name'=>'Amazonas'
		]);

		Province::create([
			'id'=>1700,
			'country_id'=>82,
			'name'=>'Antioquia'
		]);

		Province::create([
			'id'=>1701,
			'country_id'=>82,
			'name'=>'Arauca'
		]);

		Province::create([
			'id'=>1702,
			'country_id'=>82,
			'name'=>'Atlántico'
		]);

		Province::create([
			'id'=>1703,
			'country_id'=>82,
			'name'=>'Caquetá'
		]);

		Province::create([
			'id'=>1704,
			'country_id'=>82,
			'name'=>'Cauca'
		]);

		Province::create([
			'id'=>1705,
			'country_id'=>82,
			'name'=>'César'
		]);

		Province::create([
			'id'=>1706,
			'country_id'=>82,
			'name'=>'Chocó'
		]);

		Province::create([
			'id'=>1707,
			'country_id'=>82,
			'name'=>'Córdoba'
		]);

		Province::create([
			'id'=>1708,
			'country_id'=>82,
			'name'=>'Guaviare'
		]);

		Province::create([
			'id'=>1709,
			'country_id'=>82,
			'name'=>'Guainía'
		]);

		Province::create([
			'id'=>1710,
			'country_id'=>82,
			'name'=>'Huila'
		]);

		Province::create([
			'id'=>1711,
			'country_id'=>82,
			'name'=>'La Guajira'
		]);

		Province::create([
			'id'=>1712,
			'country_id'=>82,
			'name'=>'Meta'
		]);

		Province::create([
			'id'=>1713,
			'country_id'=>82,
			'name'=>'Narino'
		]);

		Province::create([
			'id'=>1714,
			'country_id'=>82,
			'name'=>'Norte de Santander'
		]);

		Province::create([
			'id'=>1715,
			'country_id'=>82,
			'name'=>'Putumayo'
		]);

		Province::create([
			'id'=>1716,
			'country_id'=>82,
			'name'=>'Quindío'
		]);

		Province::create([
			'id'=>1717,
			'country_id'=>82,
			'name'=>'Risaralda'
		]);

		Province::create([
			'id'=>1718,
			'country_id'=>82,
			'name'=>'San Andrés y Providencia'
		]);

		Province::create([
			'id'=>1719,
			'country_id'=>82,
			'name'=>'Santander'
		]);

		Province::create([
			'id'=>1720,
			'country_id'=>82,
			'name'=>'Sucre'
		]);

		Province::create([
			'id'=>1721,
			'country_id'=>82,
			'name'=>'Tolima'
		]);

		Province::create([
			'id'=>1722,
			'country_id'=>82,
			'name'=>'Valle del Cauca'
		]);

		Province::create([
			'id'=>1723,
			'country_id'=>82,
			'name'=>'Vaupés'
		]);

		Province::create([
			'id'=>1724,
			'country_id'=>82,
			'name'=>'Vichada'
		]);

		Province::create([
			'id'=>1725,
			'country_id'=>82,
			'name'=>'Casanare'
		]);

		Province::create([
			'id'=>1726,
			'country_id'=>82,
			'name'=>'Cundinamarca'
		]);

		Province::create([
			'id'=>1727,
			'country_id'=>82,
			'name'=>'Distrito Capital'
		]);

		Province::create([
			'id'=>1730,
			'country_id'=>82,
			'name'=>'Caldas'
		]);

		Province::create([
			'id'=>1731,
			'country_id'=>82,
			'name'=>'Magdalena'
		]);

		Province::create([
			'id'=>1733,
			'country_id'=>42,
			'name'=>'Aguascalientes'
		]);

		Province::create([
			'id'=>1734,
			'country_id'=>42,
			'name'=>'Baja California'
		]);

		Province::create([
			'id'=>1735,
			'country_id'=>42,
			'name'=>'Baja California Sur'
		]);

		Province::create([
			'id'=>1736,
			'country_id'=>42,
			'name'=>'Campeche'
		]);

		Province::create([
			'id'=>1737,
			'country_id'=>42,
			'name'=>'Chiapas'
		]);

		Province::create([
			'id'=>1738,
			'country_id'=>42,
			'name'=>'Chihuahua'
		]);

		Province::create([
			'id'=>1739,
			'country_id'=>42,
			'name'=>'Coahuila de Zaragoza'
		]);

		Province::create([
			'id'=>1740,
			'country_id'=>42,
			'name'=>'Colima'
		]);

		Province::create([
			'id'=>1741,
			'country_id'=>42,
			'name'=>'Distrito Federal'
		]);

		Province::create([
			'id'=>1742,
			'country_id'=>42,
			'name'=>'Durango'
		]);

		Province::create([
			'id'=>1743,
			'country_id'=>42,
			'name'=>'Guanajuato'
		]);

		Province::create([
			'id'=>1744,
			'country_id'=>42,
			'name'=>'Guerrero'
		]);

		Province::create([
			'id'=>1745,
			'country_id'=>42,
			'name'=>'Hidalgo'
		]);

		Province::create([
			'id'=>1746,
			'country_id'=>42,
			'name'=>'Jalisco'
		]);

		Province::create([
			'id'=>1747,
			'country_id'=>42,
			'name'=>'México'
		]);

		Province::create([
			'id'=>1748,
			'country_id'=>42,
			'name'=>'Michoacán de Ocampo'
		]);

		Province::create([
			'id'=>1749,
			'country_id'=>42,
			'name'=>'Morelos'
		]);

		Province::create([
			'id'=>1750,
			'country_id'=>42,
			'name'=>'Nayarit'
		]);

		Province::create([
			'id'=>1751,
			'country_id'=>42,
			'name'=>'Nuevo León'
		]);

		Province::create([
			'id'=>1752,
			'country_id'=>42,
			'name'=>'Oaxaca'
		]);

		Province::create([
			'id'=>1753,
			'country_id'=>42,
			'name'=>'Puebla'
		]);

		Province::create([
			'id'=>1754,
			'country_id'=>42,
			'name'=>'Querétaro de Arteaga'
		]);

		Province::create([
			'id'=>1755,
			'country_id'=>42,
			'name'=>'Quintana Roo'
		]);

		Province::create([
			'id'=>1756,
			'country_id'=>42,
			'name'=>'San Luis Potosí'
		]);

		Province::create([
			'id'=>1757,
			'country_id'=>42,
			'name'=>'Sinaloa'
		]);

		Province::create([
			'id'=>1758,
			'country_id'=>42,
			'name'=>'Sonora'
		]);

		Province::create([
			'id'=>1759,
			'country_id'=>42,
			'name'=>'Tabasco'
		]);

		Province::create([
			'id'=>1760,
			'country_id'=>42,
			'name'=>'Tamaulipas'
		]);

		Province::create([
			'id'=>1761,
			'country_id'=>42,
			'name'=>'Tlaxcala'
		]);

		Province::create([
			'id'=>1762,
			'country_id'=>42,
			'name'=>'Veracruz-Llave'
		]);

		Province::create([
			'id'=>1763,
			'country_id'=>42,
			'name'=>'Yucatán'
		]);

		Province::create([
			'id'=>1764,
			'country_id'=>42,
			'name'=>'Zacatecas'
		]);

		Province::create([
			'id'=>1766,
			'country_id'=>124,
			'name'=>'Bocas del Toro'
		]);

		Province::create([
			'id'=>1767,
			'country_id'=>124,
			'name'=>'Chiriquí'
		]);

		Province::create([
			'id'=>1768,
			'country_id'=>124,
			'name'=>'Coclé'
		]);

		Province::create([
			'id'=>1769,
			'country_id'=>124,
			'name'=>'Colón'
		]);

		Province::create([
			'id'=>1770,
			'country_id'=>124,
			'name'=>'Darién'
		]);

		Province::create([
			'id'=>1771,
			'country_id'=>124,
			'name'=>'Herrera'
		]);

		Province::create([
			'id'=>1772,
			'country_id'=>124,
			'name'=>'Los Santos'
		]);

		Province::create([
			'id'=>1773,
			'country_id'=>124,
			'name'=>'Panamá'
		]);

		Province::create([
			'id'=>1774,
			'country_id'=>124,
			'name'=>'San Blas'
		]);

		Province::create([
			'id'=>1775,
			'country_id'=>124,
			'name'=>'Veraguas'
		]);

		Province::create([
			'id'=>1777,
			'country_id'=>123,
			'name'=>'Chuquisaca'
		]);

		Province::create([
			'id'=>1778,
			'country_id'=>123,
			'name'=>'Cochabamba'
		]);

		Province::create([
			'id'=>1779,
			'country_id'=>123,
			'name'=>'El Beni'
		]);

		Province::create([
			'id'=>1780,
			'country_id'=>123,
			'name'=>'La Paz'
		]);

		Province::create([
			'id'=>1781,
			'country_id'=>123,
			'name'=>'Oruro'
		]);

		Province::create([
			'id'=>1782,
			'country_id'=>123,
			'name'=>'Pando'
		]);

		Province::create([
			'id'=>1783,
			'country_id'=>123,
			'name'=>'Potosí'
		]);

		Province::create([
			'id'=>1784,
			'country_id'=>123,
			'name'=>'Santa Cruz'
		]);

		Province::create([
			'id'=>1785,
			'country_id'=>123,
			'name'=>'Tarija'
		]);

		Province::create([
			'id'=>1787,
			'country_id'=>36,
			'name'=>'Alajuela'
		]);

		Province::create([
			'id'=>1788,
			'country_id'=>36,
			'name'=>'Cartago'
		]);

		Province::create([
			'id'=>1789,
			'country_id'=>36,
			'name'=>'Guanacaste'
		]);

		Province::create([
			'id'=>1790,
			'country_id'=>36,
			'name'=>'Heredia'
		]);

		Province::create([
			'id'=>1791,
			'country_id'=>36,
			'name'=>'Limón'
		]);

		Province::create([
			'id'=>1792,
			'country_id'=>36,
			'name'=>'Puntarenas'
		]);

		Province::create([
			'id'=>1793,
			'country_id'=>36,
			'name'=>'San José'
		]);

		Province::create([
			'id'=>1795,
			'country_id'=>103,
			'name'=>'Galápagos'
		]);

		Province::create([
			'id'=>1796,
			'country_id'=>103,
			'name'=>'Azuay'
		]);

		Province::create([
			'id'=>1797,
			'country_id'=>103,
			'name'=>'Bolívar'
		]);

		Province::create([
			'id'=>1798,
			'country_id'=>103,
			'name'=>'Canar'
		]);

		Province::create([
			'id'=>1799,
			'country_id'=>103,
			'name'=>'Carchi'
		]);

		Province::create([
			'id'=>1800,
			'country_id'=>103,
			'name'=>'Chimborazo'
		]);

		Province::create([
			'id'=>1801,
			'country_id'=>103,
			'name'=>'Cotopaxi'
		]);

		Province::create([
			'id'=>1802,
			'country_id'=>103,
			'name'=>'El Oro'
		]);

		Province::create([
			'id'=>1803,
			'country_id'=>103,
			'name'=>'Esmeraldas'
		]);

		Province::create([
			'id'=>1804,
			'country_id'=>103,
			'name'=>'Guayas'
		]);

		Province::create([
			'id'=>1805,
			'country_id'=>103,
			'name'=>'Imbabura'
		]);

		Province::create([
			'id'=>1806,
			'country_id'=>103,
			'name'=>'Loja'
		]);

		Province::create([
			'id'=>1807,
			'country_id'=>103,
			'name'=>'Los Ríos'
		]);

		Province::create([
			'id'=>1808,
			'country_id'=>103,
			'name'=>'Manabí'
		]);

		Province::create([
			'id'=>1809,
			'country_id'=>103,
			'name'=>'Morona-Santiago'
		]);

		Province::create([
			'id'=>1810,
			'country_id'=>103,
			'name'=>'Pastaza'
		]);

		Province::create([
			'id'=>1811,
			'country_id'=>103,
			'name'=>'Pichincha'
		]);

		Province::create([
			'id'=>1812,
			'country_id'=>103,
			'name'=>'Tungurahua'
		]);

		Province::create([
			'id'=>1813,
			'country_id'=>103,
			'name'=>'Zamora-Chinchipe'
		]);

		Province::create([
			'id'=>1814,
			'country_id'=>103,
			'name'=>'Sucumbíos'
		]);

		Province::create([
			'id'=>1815,
			'country_id'=>103,
			'name'=>'Napo'
		]);

		Province::create([
			'id'=>1816,
			'country_id'=>103,
			'name'=>'Orellana'
		]);

		Province::create([
			'id'=>1818,
			'country_id'=>5,
			'name'=>'Buenos Aires'
		]);

		Province::create([
			'id'=>1819,
			'country_id'=>5,
			'name'=>'Catamarca'
		]);

		Province::create([
			'id'=>1820,
			'country_id'=>5,
			'name'=>'Chaco'
		]);

		Province::create([
			'id'=>1821,
			'country_id'=>5,
			'name'=>'Chubut'
		]);

		Province::create([
			'id'=>1822,
			'country_id'=>5,
			'name'=>'Córdoba'
		]);

		Province::create([
			'id'=>1823,
			'country_id'=>5,
			'name'=>'Corrientes'
		]);

		Province::create([
			'id'=>1824,
			'country_id'=>5,
			'name'=>'Distrito Federal'
		]);

		Province::create([
			'id'=>1825,
			'country_id'=>5,
			'name'=>'Entre Ríos'
		]);

		Province::create([
			'id'=>1826,
			'country_id'=>5,
			'name'=>'Formosa'
		]);

		Province::create([
			'id'=>1827,
			'country_id'=>5,
			'name'=>'Jujuy'
		]);

		Province::create([
			'id'=>1828,
			'country_id'=>5,
			'name'=>'La Pampa'
		]);

		Province::create([
			'id'=>1829,
			'country_id'=>5,
			'name'=>'La Rioja'
		]);

		Province::create([
			'id'=>1830,
			'country_id'=>5,
			'name'=>'Mendoza'
		]);

		Province::create([
			'id'=>1831,
			'country_id'=>5,
			'name'=>'Misiones'
		]);

		Province::create([
			'id'=>1832,
			'country_id'=>5,
			'name'=>'Neuquén'
		]);

		Province::create([
			'id'=>1833,
			'country_id'=>5,
			'name'=>'Río Negro'
		]);

		Province::create([
			'id'=>1834,
			'country_id'=>5,
			'name'=>'Salta'
		]);

		Province::create([
			'id'=>1835,
			'country_id'=>5,
			'name'=>'San Juan'
		]);

		Province::create([
			'id'=>1836,
			'country_id'=>5,
			'name'=>'San Luis'
		]);

		Province::create([
			'id'=>1837,
			'country_id'=>5,
			'name'=>'Santa Cruz'
		]);

		Province::create([
			'id'=>1838,
			'country_id'=>5,
			'name'=>'Santa Fe'
		]);

		Province::create([
			'id'=>1839,
			'country_id'=>5,
			'name'=>'Santiago del Estero'
		]);

		Province::create([
			'id'=>1840,
			'country_id'=>5,
			'name'=>'Tierra del Fuego'
		]);

		Province::create([
			'id'=>1841,
			'country_id'=>5,
			'name'=>'Tucumán'
		]);

		Province::create([
			'id'=>1843,
			'country_id'=>95,
			'name'=>'Amazonas'
		]);

		Province::create([
			'id'=>1844,
			'country_id'=>95,
			'name'=>'Anzoategui'
		]);

		Province::create([
			'id'=>1845,
			'country_id'=>95,
			'name'=>'Apure'
		]);

		Province::create([
			'id'=>1846,
			'country_id'=>95,
			'name'=>'Aragua'
		]);

		Province::create([
			'id'=>1847,
			'country_id'=>95,
			'name'=>'Barinas'
		]);

		Province::create([
			'id'=>1848,
			'country_id'=>95,
			'name'=>'Bolívar'
		]);

		Province::create([
			'id'=>1849,
			'country_id'=>95,
			'name'=>'Carabobo'
		]);

		Province::create([
			'id'=>1850,
			'country_id'=>95,
			'name'=>'Cojedes'
		]);

		Province::create([
			'id'=>1851,
			'country_id'=>95,
			'name'=>'Delta Amacuro'
		]);

		Province::create([
			'id'=>1852,
			'country_id'=>95,
			'name'=>'Falcón'
		]);

		Province::create([
			'id'=>1853,
			'country_id'=>95,
			'name'=>'Guárico'
		]);

		Province::create([
			'id'=>1854,
			'country_id'=>95,
			'name'=>'Lara'
		]);

		Province::create([
			'id'=>1855,
			'country_id'=>95,
			'name'=>'Mérida'
		]);

		Province::create([
			'id'=>1856,
			'country_id'=>95,
			'name'=>'Miranda'
		]);

		Province::create([
			'id'=>1857,
			'country_id'=>95,
			'name'=>'Monagas'
		]);

		Province::create([
			'id'=>1858,
			'country_id'=>95,
			'name'=>'Nueva Esparta'
		]);

		Province::create([
			'id'=>1859,
			'country_id'=>95,
			'name'=>'Portuguesa'
		]);

		Province::create([
			'id'=>1860,
			'country_id'=>95,
			'name'=>'Sucre'
		]);

		Province::create([
			'id'=>1861,
			'country_id'=>95,
			'name'=>'Táchira'
		]);

		Province::create([
			'id'=>1862,
			'country_id'=>95,
			'name'=>'Trujillo'
		]);

		Province::create([
			'id'=>1863,
			'country_id'=>95,
			'name'=>'Yaracuy'
		]);

		Province::create([
			'id'=>1864,
			'country_id'=>95,
			'name'=>'Zulia'
		]);

		Province::create([
			'id'=>1865,
			'country_id'=>95,
			'name'=>'Dependencias Federales'
		]);

		Province::create([
			'id'=>1866,
			'country_id'=>95,
			'name'=>'Distrito Federal'
		]);

		Province::create([
			'id'=>1867,
			'country_id'=>95,
			'name'=>'Vargas'
		]);

		Province::create([
			'id'=>1869,
			'country_id'=>209,
			'name'=>'Boaco'
		]);

		Province::create([
			'id'=>1870,
			'country_id'=>209,
			'name'=>'Carazo'
		]);

		Province::create([
			'id'=>1871,
			'country_id'=>209,
			'name'=>'Chinandega'
		]);

		Province::create([
			'id'=>1872,
			'country_id'=>209,
			'name'=>'Chontales'
		]);

		Province::create([
			'id'=>1873,
			'country_id'=>209,
			'name'=>'Estelí'
		]);

		Province::create([
			'id'=>1874,
			'country_id'=>209,
			'name'=>'Granada'
		]);

		Province::create([
			'id'=>1875,
			'country_id'=>209,
			'name'=>'Jinotega'
		]);

		Province::create([
			'id'=>1876,
			'country_id'=>209,
			'name'=>'León'
		]);

		Province::create([
			'id'=>1877,
			'country_id'=>209,
			'name'=>'Madriz'
		]);

		Province::create([
			'id'=>1878,
			'country_id'=>209,
			'name'=>'Managua'
		]);

		Province::create([
			'id'=>1879,
			'country_id'=>209,
			'name'=>'Masaya'
		]);

		Province::create([
			'id'=>1880,
			'country_id'=>209,
			'name'=>'Matagalpa'
		]);

		Province::create([
			'id'=>1881,
			'country_id'=>209,
			'name'=>'Nueva Segovia'
		]);

		Province::create([
			'id'=>1882,
			'country_id'=>209,
			'name'=>'Rio San Juan'
		]);

		Province::create([
			'id'=>1883,
			'country_id'=>209,
			'name'=>'Rivas'
		]);

		Province::create([
			'id'=>1884,
			'country_id'=>209,
			'name'=>'Zelaya'
		]);

		Province::create([
			'id'=>1886,
			'country_id'=>113,
			'name'=>'Pinar del Rio'
		]);

		Province::create([
			'id'=>1887,
			'country_id'=>113,
			'name'=>'Ciudad de la Habana'
		]);

		Province::create([
			'id'=>1888,
			'country_id'=>113,
			'name'=>'Matanzas'
		]);

		Province::create([
			'id'=>1889,
			'country_id'=>113,
			'name'=>'Isla de la Juventud'
		]);

		Province::create([
			'id'=>1890,
			'country_id'=>113,
			'name'=>'Camaguey'
		]);

		Province::create([
			'id'=>1891,
			'country_id'=>113,
			'name'=>'Ciego de Avila'
		]);

		Province::create([
			'id'=>1892,
			'country_id'=>113,
			'name'=>'Cienfuegos'
		]);

		Province::create([
			'id'=>1893,
			'country_id'=>113,
			'name'=>'Granma'
		]);

		Province::create([
			'id'=>1894,
			'country_id'=>113,
			'name'=>'Guantanamo'
		]);

		Province::create([
			'id'=>1895,
			'country_id'=>113,
			'name'=>'La Habana'
		]);

		Province::create([
			'id'=>1896,
			'country_id'=>113,
			'name'=>'Holguin'
		]);

		Province::create([
			'id'=>1897,
			'country_id'=>113,
			'name'=>'Las Tunas'
		]);

		Province::create([
			'id'=>1898,
			'country_id'=>113,
			'name'=>'Sancti Spiritus'
		]);

		Province::create([
			'id'=>1899,
			'country_id'=>113,
			'name'=>'Santiago de Cuba'
		]);

		Province::create([
			'id'=>1900,
			'country_id'=>113,
			'name'=>'Villa Clara'
		]);

		Province::create([
			'id'=>1901,
			'country_id'=>12,
			'name'=>'Acre'
		]);

		Province::create([
			'id'=>1902,
			'country_id'=>12,
			'name'=>'Alagoas'
		]);

		Province::create([
			'id'=>1903,
			'country_id'=>12,
			'name'=>'Amapa'
		]);

		Province::create([
			'id'=>1904,
			'country_id'=>12,
			'name'=>'Amazonas'
		]);

		Province::create([
			'id'=>1905,
			'country_id'=>12,
			'name'=>'Bahia'
		]);

		Province::create([
			'id'=>1906,
			'country_id'=>12,
			'name'=>'Ceara'
		]);

		Province::create([
			'id'=>1907,
			'country_id'=>12,
			'name'=>'Distrito Federal'
		]);

		Province::create([
			'id'=>1908,
			'country_id'=>12,
			'name'=>'Espirito Santo'
		]);

		Province::create([
			'id'=>1909,
			'country_id'=>12,
			'name'=>'Mato Grosso do Sul'
		]);

		Province::create([
			'id'=>1910,
			'country_id'=>12,
			'name'=>'Maranhao'
		]);

		Province::create([
			'id'=>1911,
			'country_id'=>12,
			'name'=>'Mato Grosso'
		]);

		Province::create([
			'id'=>1912,
			'country_id'=>12,
			'name'=>'Minas Gerais'
		]);

		Province::create([
			'id'=>1913,
			'country_id'=>12,
			'name'=>'Para'
		]);

		Province::create([
			'id'=>1914,
			'country_id'=>12,
			'name'=>'Paraiba'
		]);

		Province::create([
			'id'=>1915,
			'country_id'=>12,
			'name'=>'Parana'
		]);

		Province::create([
			'id'=>1916,
			'country_id'=>12,
			'name'=>'Piaui'
		]);

		Province::create([
			'id'=>1917,
			'country_id'=>12,
			'name'=>'Rio de Janeiro'
		]);

		Province::create([
			'id'=>1918,
			'country_id'=>12,
			'name'=>'Rio Grande do Norte'
		]);

		Province::create([
			'id'=>1919,
			'country_id'=>12,
			'name'=>'Rio Grande do Sul'
		]);

		Province::create([
			'id'=>1920,
			'country_id'=>12,
			'name'=>'Rondonia'
		]);

		Province::create([
			'id'=>1921,
			'country_id'=>12,
			'name'=>'Roraima'
		]);

		Province::create([
			'id'=>1922,
			'country_id'=>12,
			'name'=>'Santa Catarina'
		]);

		Province::create([
			'id'=>1923,
			'country_id'=>12,
			'name'=>'Sao Paulo'
		]);

		Province::create([
			'id'=>1924,
			'country_id'=>12,
			'name'=>'Sergipe'
		]);

		Province::create([
			'id'=>1925,
			'country_id'=>12,
			'name'=>'Goias'
		]);

		Province::create([
			'id'=>1926,
			'country_id'=>12,
			'name'=>'Pernambuco'
		]);

		Province::create([
			'id'=>1927,
			'country_id'=>12,
			'name'=>'Tocantins'
		]);

		Province::create([
			'id'=>1930,
			'country_id'=>83,
			'name'=>'Akureyri'
		]);

		Province::create([
			'id'=>1931,
			'country_id'=>83,
			'name'=>'Arnessysla'
		]);

		Province::create([
			'id'=>1932,
			'country_id'=>83,
			'name'=>'Austur-Bardastrandarsysla'
		]);

		Province::create([
			'id'=>1933,
			'country_id'=>83,
			'name'=>'Austur-Hunavatnssysla'
		]);

		Province::create([
			'id'=>1934,
			'country_id'=>83,
			'name'=>'Austur-Skaftafellssysla'
		]);

		Province::create([
			'id'=>1935,
			'country_id'=>83,
			'name'=>'Borgarfjardarsysla'
		]);

		Province::create([
			'id'=>1936,
			'country_id'=>83,
			'name'=>'Dalasysla'
		]);

		Province::create([
			'id'=>1937,
			'country_id'=>83,
			'name'=>'Eyjafjardarsysla'
		]);

		Province::create([
			'id'=>1938,
			'country_id'=>83,
			'name'=>'Gullbringusysla'
		]);

		Province::create([
			'id'=>1939,
			'country_id'=>83,
			'name'=>'Hafnarfjordur'
		]);

		Province::create([
			'id'=>1943,
			'country_id'=>83,
			'name'=>'Kjosarsysla'
		]);

		Province::create([
			'id'=>1944,
			'country_id'=>83,
			'name'=>'Kopavogur'
		]);

		Province::create([
			'id'=>1945,
			'country_id'=>83,
			'name'=>'Myrasysla'
		]);

		Province::create([
			'id'=>1946,
			'country_id'=>83,
			'name'=>'Neskaupstadur'
		]);

		Province::create([
			'id'=>1947,
			'country_id'=>83,
			'name'=>'Nordur-Isafjardarsysla'
		]);

		Province::create([
			'id'=>1948,
			'country_id'=>83,
			'name'=>'Nordur-Mulasysla'
		]);

		Province::create([
			'id'=>1949,
			'country_id'=>83,
			'name'=>'Nordur-Tingeyjarsysla'
		]);

		Province::create([
			'id'=>1950,
			'country_id'=>83,
			'name'=>'Olafsfjordur'
		]);

		Province::create([
			'id'=>1951,
			'country_id'=>83,
			'name'=>'Rangarvallasysla'
		]);

		Province::create([
			'id'=>1952,
			'country_id'=>83,
			'name'=>'Reykjavik'
		]);

		Province::create([
			'id'=>1953,
			'country_id'=>83,
			'name'=>'Saudarkrokur'
		]);

		Province::create([
			'id'=>1954,
			'country_id'=>83,
			'name'=>'Seydisfjordur'
		]);

		Province::create([
			'id'=>1956,
			'country_id'=>83,
			'name'=>'Skagafjardarsysla'
		]);

		Province::create([
			'id'=>1957,
			'country_id'=>83,
			'name'=>'Snafellsnes- og Hnappadalssysla'
		]);

		Province::create([
			'id'=>1958,
			'country_id'=>83,
			'name'=>'Strandasysla'
		]);

		Province::create([
			'id'=>1959,
			'country_id'=>83,
			'name'=>'Sudur-Mulasysla'
		]);

		Province::create([
			'id'=>1960,
			'country_id'=>83,
			'name'=>'Sudur-Tingeyjarsysla'
		]);

		Province::create([
			'id'=>1961,
			'country_id'=>83,
			'name'=>'Vestmannaeyjar'
		]);

		Province::create([
			'id'=>1962,
			'country_id'=>83,
			'name'=>'Vestur-Bardastrandarsysla'
		]);

		Province::create([
			'id'=>1964,
			'country_id'=>83,
			'name'=>'Vestur-Isafjardarsysla'
		]);

		Province::create([
			'id'=>1965,
			'country_id'=>83,
			'name'=>'Vestur-Skaftafellssysla'
		]);

		Province::create([
			'id'=>1966,
			'country_id'=>35,
			'name'=>'Anhui'
		]);

		Province::create([
			'id'=>1967,
			'country_id'=>35,
			'name'=>'Zhejiang'
		]);

		Province::create([
			'id'=>1968,
			'country_id'=>35,
			'name'=>'Jiangxi'
		]);

		Province::create([
			'id'=>1969,
			'country_id'=>35,
			'name'=>'Jiangsu'
		]);

		Province::create([
			'id'=>1970,
			'country_id'=>35,
			'name'=>'Jilin'
		]);

		Province::create([
			'id'=>1971,
			'country_id'=>35,
			'name'=>'Qinghai'
		]);

		Province::create([
			'id'=>1972,
			'country_id'=>35,
			'name'=>'Fujian'
		]);

		Province::create([
			'id'=>1973,
			'country_id'=>35,
			'name'=>'Heilongjiang'
		]);

		Province::create([
			'id'=>1974,
			'country_id'=>35,
			'name'=>'Henan'
		]);

		Province::create([
			'id'=>1975,
			'country_id'=>35,
			'name'=>'Hebei'
		]);

		Province::create([
			'id'=>1976,
			'country_id'=>35,
			'name'=>'Hunan'
		]);

		Province::create([
			'id'=>1977,
			'country_id'=>35,
			'name'=>'Hubei'
		]);

		Province::create([
			'id'=>1978,
			'country_id'=>35,
			'name'=>'Xinjiang'
		]);

		Province::create([
			'id'=>1979,
			'country_id'=>35,
			'name'=>'Xizang'
		]);

		Province::create([
			'id'=>1980,
			'country_id'=>35,
			'name'=>'Gansu'
		]);

		Province::create([
			'id'=>1981,
			'country_id'=>35,
			'name'=>'Guangxi'
		]);

		Province::create([
			'id'=>1982,
			'country_id'=>35,
			'name'=>'Guizhou'
		]);

		Province::create([
			'id'=>1983,
			'country_id'=>35,
			'name'=>'Liaoning'
		]);

		Province::create([
			'id'=>1984,
			'country_id'=>35,
			'name'=>'Nei Mongol'
		]);

		Province::create([
			'id'=>1985,
			'country_id'=>35,
			'name'=>'Ningxia'
		]);

		Province::create([
			'id'=>1986,
			'country_id'=>35,
			'name'=>'Beijing'
		]);

		Province::create([
			'id'=>1987,
			'country_id'=>35,
			'name'=>'Shanghai'
		]);

		Province::create([
			'id'=>1988,
			'country_id'=>35,
			'name'=>'Shanxi'
		]);

		Province::create([
			'id'=>1989,
			'country_id'=>35,
			'name'=>'Shandong'
		]);

		Province::create([
			'id'=>1990,
			'country_id'=>35,
			'name'=>'Shaanxi'
		]);

		Province::create([
			'id'=>1991,
			'country_id'=>35,
			'name'=>'Sichuan'
		]);

		Province::create([
			'id'=>1992,
			'country_id'=>35,
			'name'=>'Tianjin'
		]);

		Province::create([
			'id'=>1993,
			'country_id'=>35,
			'name'=>'Yunnan'
		]);

		Province::create([
			'id'=>1994,
			'country_id'=>35,
			'name'=>'Guangdong'
		]);

		Province::create([
			'id'=>1995,
			'country_id'=>35,
			'name'=>'Hainan'
		]);

		Province::create([
			'id'=>1996,
			'country_id'=>35,
			'name'=>'Chongqing'
		]);

		Province::create([
			'id'=>1997,
			'country_id'=>97,
			'name'=>'Central'
		]);

		Province::create([
			'id'=>1998,
			'country_id'=>97,
			'name'=>'Coast'
		]);

		Province::create([
			'id'=>1999,
			'country_id'=>97,
			'name'=>'Eastern'
		]);

		Province::create([
			'id'=>2000,
			'country_id'=>97,
			'name'=>'Nairobi Area'
		]);

		Province::create([
			'id'=>2001,
			'country_id'=>97,
			'name'=>'North-Eastern'
		]);

		Province::create([
			'id'=>2002,
			'country_id'=>97,
			'name'=>'Nyanza'
		]);

		Province::create([
			'id'=>2003,
			'country_id'=>97,
			'name'=>'Rift Valley'
		]);

		Province::create([
			'id'=>2004,
			'country_id'=>97,
			'name'=>'Western'
		]);

		Province::create([
			'id'=>2006,
			'country_id'=>195,
			'name'=>'Gilbert Islands'
		]);

		Province::create([
			'id'=>2007,
			'country_id'=>195,
			'name'=>'Line Islands'
		]);

		Province::create([
			'id'=>2008,
			'country_id'=>195,
			'name'=>'Phoenix Islands'
		]);

		Province::create([
			'id'=>2010,
			'country_id'=>1,
			'name'=>'Australian Capital Territory'
		]);

		Province::create([
			'id'=>2011,
			'country_id'=>1,
			'name'=>'New South Wales'
		]);

		Province::create([
			'id'=>2012,
			'country_id'=>1,
			'name'=>'Northern Territory'
		]);

		Province::create([
			'id'=>2013,
			'country_id'=>1,
			'name'=>'Queensland'
		]);

		Province::create([
			'id'=>2014,
			'country_id'=>1,
			'name'=>'South Australia'
		]);

		Province::create([
			'id'=>2015,
			'country_id'=>1,
			'name'=>'Tasmania'
		]);

		Province::create([
			'id'=>2016,
			'country_id'=>1,
			'name'=>'Victoria'
		]);

		Province::create([
			'id'=>2017,
			'country_id'=>1,
			'name'=>'Western Australia'
		]);

		Province::create([
			'id'=>2018,
			'country_id'=>27,
			'name'=>'Dublin'
		]);

		Province::create([
			'id'=>2019,
			'country_id'=>27,
			'name'=>'Galway'
		]);

		Province::create([
			'id'=>2020,
			'country_id'=>27,
			'name'=>'Kildare'
		]);

		Province::create([
			'id'=>2021,
			'country_id'=>27,
			'name'=>'Leitrim'
		]);

		Province::create([
			'id'=>2022,
			'country_id'=>27,
			'name'=>'Limerick'
		]);

		Province::create([
			'id'=>2023,
			'country_id'=>27,
			'name'=>'Mayo'
		]);

		Province::create([
			'id'=>2024,
			'country_id'=>27,
			'name'=>'Meath'
		]);

		Province::create([
			'id'=>2025,
			'country_id'=>27,
			'name'=>'Carlow'
		]);

		Province::create([
			'id'=>2026,
			'country_id'=>27,
			'name'=>'Kilkenny'
		]);

		Province::create([
			'id'=>2027,
			'country_id'=>27,
			'name'=>'Laois'
		]);

		Province::create([
			'id'=>2028,
			'country_id'=>27,
			'name'=>'Longford'
		]);

		Province::create([
			'id'=>2029,
			'country_id'=>27,
			'name'=>'Louth'
		]);

		Province::create([
			'id'=>2030,
			'country_id'=>27,
			'name'=>'Offaly'
		]);

		Province::create([
			'id'=>2031,
			'country_id'=>27,
			'name'=>'Westmeath'
		]);

		Province::create([
			'id'=>2032,
			'country_id'=>27,
			'name'=>'Wexford'
		]);

		Province::create([
			'id'=>2033,
			'country_id'=>27,
			'name'=>'Wicklow'
		]);

		Province::create([
			'id'=>2034,
			'country_id'=>27,
			'name'=>'Roscommon'
		]);

		Province::create([
			'id'=>2035,
			'country_id'=>27,
			'name'=>'Sligo'
		]);

		Province::create([
			'id'=>2036,
			'country_id'=>27,
			'name'=>'Clare'
		]);

		Province::create([
			'id'=>2037,
			'country_id'=>27,
			'name'=>'Cork'
		]);

		Province::create([
			'id'=>2038,
			'country_id'=>27,
			'name'=>'Kerry'
		]);

		Province::create([
			'id'=>2039,
			'country_id'=>27,
			'name'=>'Tipperary'
		]);

		Province::create([
			'id'=>2040,
			'country_id'=>27,
			'name'=>'Waterford'
		]);

		Province::create([
			'id'=>2041,
			'country_id'=>27,
			'name'=>'Cavan'
		]);

		Province::create([
			'id'=>2042,
			'country_id'=>27,
			'name'=>'Donegal'
		]);

		Province::create([
			'id'=>2043,
			'country_id'=>27,
			'name'=>'Monaghan'
		]);

		Province::create([
			'id'=>2044,
			'country_id'=>50,
			'name'=>'Karachaeva-Cherkesskaya Respublica'
		]);

		Province::create([
			'id'=>2045,
			'country_id'=>50,
			'name'=>'Raimirskii (Dolgano-Nenetskii) AO'
		]);

		Province::create([
			'id'=>2046,
			'country_id'=>50,
			'name'=>'Respublica Tiva'
		]);

		Province::create([
			'id'=>2047,
			'country_id'=>32,
			'name'=>'Newfoundland'
		]);

		Province::create([
			'id'=>2048,
			'country_id'=>32,
			'name'=>'Nova Scotia'
		]);

		Province::create([
			'id'=>2049,
			'country_id'=>32,
			'name'=>'Prince Edward Island'
		]);

		Province::create([
			'id'=>2050,
			'country_id'=>32,
			'name'=>'New Brunswick'
		]);

		Province::create([
			'id'=>2051,
			'country_id'=>32,
			'name'=>'Quebec'
		]);

		Province::create([
			'id'=>2052,
			'country_id'=>32,
			'name'=>'Ontario'
		]);

		Province::create([
			'id'=>2053,
			'country_id'=>32,
			'name'=>'Manitoba'
		]);

		Province::create([
			'id'=>2054,
			'country_id'=>32,
			'name'=>'Saskatchewan'
		]);

		Province::create([
			'id'=>2055,
			'country_id'=>32,
			'name'=>'Alberta'
		]);

		Province::create([
			'id'=>2056,
			'country_id'=>32,
			'name'=>'British Columbia'
		]);

		Province::create([
			'id'=>2057,
			'country_id'=>32,
			'name'=>'Nunavut'
		]);

		Province::create([
			'id'=>2058,
			'country_id'=>32,
			'name'=>'Northwest Territories'
		]);

		Province::create([
			'id'=>2059,
			'country_id'=>32,
			'name'=>'Yukon Territory'
		]);

		Province::create([
			'id'=>2060,
			'country_id'=>19,
			'name'=>'Drenthe'
		]);

		Province::create([
			'id'=>2061,
			'country_id'=>19,
			'name'=>'Friesland'
		]);

		Province::create([
			'id'=>2062,
			'country_id'=>19,
			'name'=>'Gelderland'
		]);

		Province::create([
			'id'=>2063,
			'country_id'=>19,
			'name'=>'Groningen'
		]);

		Province::create([
			'id'=>2064,
			'country_id'=>19,
			'name'=>'Limburg'
		]);

		Province::create([
			'id'=>2065,
			'country_id'=>19,
			'name'=>'Noord-Brabant'
		]);

		Province::create([
			'id'=>2066,
			'country_id'=>19,
			'name'=>'Noord-Holland'
		]);

		Province::create([
			'id'=>2067,
			'country_id'=>19,
			'name'=>'Utrecht'
		]);

		Province::create([
			'id'=>2068,
			'country_id'=>19,
			'name'=>'Zeeland'
		]);

		Province::create([
			'id'=>2069,
			'country_id'=>19,
			'name'=>'Zuid-Holland'
		]);

		Province::create([
			'id'=>2071,
			'country_id'=>19,
			'name'=>'Overijssel'
		]);

		Province::create([
			'id'=>2072,
			'country_id'=>19,
			'name'=>'Flevoland'
		]);

		Province::create([
			'id'=>2073,
			'country_id'=>138,
			'name'=>'Duarte'
		]);

		Province::create([
			'id'=>2074,
			'country_id'=>138,
			'name'=>'Puerto Plata'
		]);

		Province::create([
			'id'=>2075,
			'country_id'=>138,
			'name'=>'Valverde'
		]);

		Province::create([
			'id'=>2076,
			'country_id'=>138,
			'name'=>'María Trinidad Sánchez'
		]);

		Province::create([
			'id'=>2077,
			'country_id'=>138,
			'name'=>'Azua'
		]);

		Province::create([
			'id'=>2078,
			'country_id'=>138,
			'name'=>'Santiago'
		]);

		Province::create([
			'id'=>2079,
			'country_id'=>138,
			'name'=>'San Cristóbal'
		]);

		Province::create([
			'id'=>2080,
			'country_id'=>138,
			'name'=>'Peravia'
		]);

		Province::create([
			'id'=>2081,
			'country_id'=>138,
			'name'=>'Elías Piña'
		]);

		Province::create([
			'id'=>2082,
			'country_id'=>138,
			'name'=>'Barahona'
		]);

		Province::create([
			'id'=>2083,
			'country_id'=>138,
			'name'=>'Monte Plata'
		]);

		Province::create([
			'id'=>2084,
			'country_id'=>138,
			'name'=>'Salcedo'
		]);

		Province::create([
			'id'=>2085,
			'country_id'=>138,
			'name'=>'La Altagracia'
		]);

		Province::create([
			'id'=>2086,
			'country_id'=>138,
			'name'=>'San Juan'
		]);

		Province::create([
			'id'=>2087,
			'country_id'=>138,
			'name'=>'Monseñor Nouel'
		]);

		Province::create([
			'id'=>2088,
			'country_id'=>138,
			'name'=>'Monte Cristi'
		]);

		Province::create([
			'id'=>2089,
			'country_id'=>138,
			'name'=>'Espaillat'
		]);

		Province::create([
			'id'=>2090,
			'country_id'=>138,
			'name'=>'Sánchez Ramírez'
		]);

		Province::create([
			'id'=>2091,
			'country_id'=>138,
			'name'=>'La Vega'
		]);

		Province::create([
			'id'=>2092,
			'country_id'=>138,
			'name'=>'San Pedro de Macorís'
		]);

		Province::create([
			'id'=>2093,
			'country_id'=>138,
			'name'=>'Independencia'
		]);

		Province::create([
			'id'=>2094,
			'country_id'=>138,
			'name'=>'Dajabón'
		]);

		Province::create([
			'id'=>2095,
			'country_id'=>138,
			'name'=>'Baoruco'
		]);

		Province::create([
			'id'=>2096,
			'country_id'=>138,
			'name'=>'El Seibo'
		]);

		Province::create([
			'id'=>2097,
			'country_id'=>138,
			'name'=>'Hato Mayor'
		]);

		Province::create([
			'id'=>2098,
			'country_id'=>138,
			'name'=>'La Romana'
		]);

		Province::create([
			'id'=>2099,
			'country_id'=>138,
			'name'=>'Pedernales'
		]);

		Province::create([
			'id'=>2100,
			'country_id'=>138,
			'name'=>'Samaná'
		]);

		Province::create([
			'id'=>2101,
			'country_id'=>138,
			'name'=>'Santiago Rodríguez'
		]);

		Province::create([
			'id'=>2102,
			'country_id'=>138,
			'name'=>'San José de Ocoa'
		]);

		Province::create([
			'id'=>2103,
			'country_id'=>70,
			'name'=>'Chiba'
		]);

		Province::create([
			'id'=>2104,
			'country_id'=>70,
			'name'=>'Ehime'
		]);

		Province::create([
			'id'=>2105,
			'country_id'=>70,
			'name'=>'Oita'
		]);

		Province::create([
			'id'=>2106,
			'country_id'=>85,
			'name'=>'Skopje'
		]);

		Province::create([
			'id'=>2108,
			'country_id'=>35,
			'name'=>'Schanghai'
		]);

		Province::create([
			'id'=>2109,
			'country_id'=>35,
			'name'=>'Hongkong'
		]);

		Province::create([
			'id'=>2110,
			'country_id'=>35,
			'name'=>'Neimenggu'
		]);

		Province::create([
			'id'=>2111,
			'country_id'=>35,
			'name'=>'Aomen'
		]);

		Province::create([
			'id'=>2112,
			'country_id'=>92,
			'name'=>'Amnat Charoen'
		]);

		Province::create([
			'id'=>2113,
			'country_id'=>92,
			'name'=>'Ang Thong'
		]);

		Province::create([
			'id'=>2114,
			'country_id'=>92,
			'name'=>'Bangkok'
		]);

		Province::create([
			'id'=>2115,
			'country_id'=>92,
			'name'=>'Buri Ram'
		]);

		Province::create([
			'id'=>2116,
			'country_id'=>92,
			'name'=>'Chachoengsao'
		]);

		Province::create([
			'id'=>2117,
			'country_id'=>92,
			'name'=>'Chai Nat'
		]);

		Province::create([
			'id'=>2118,
			'country_id'=>92,
			'name'=>'Chaiyaphum'
		]);

		Province::create([
			'id'=>2119,
			'country_id'=>92,
			'name'=>'Chanthaburi'
		]);

		Province::create([
			'id'=>2120,
			'country_id'=>92,
			'name'=>'Chiang Mai'
		]);

		Province::create([
			'id'=>2121,
			'country_id'=>92,
			'name'=>'Chiang Rai'
		]);

		Province::create([
			'id'=>2122,
			'country_id'=>92,
			'name'=>'Chon Buri'
		]);

		Province::create([
			'id'=>2124,
			'country_id'=>92,
			'name'=>'Kalasin'
		]);

		Province::create([
			'id'=>2126,
			'country_id'=>92,
			'name'=>'Kanchanaburi'
		]);

		Province::create([
			'id'=>2127,
			'country_id'=>92,
			'name'=>'Khon Kaen'
		]);

		Province::create([
			'id'=>2128,
			'country_id'=>92,
			'name'=>'Krabi'
		]);

		Province::create([
			'id'=>2129,
			'country_id'=>92,
			'name'=>'Lampang'
		]);

		Province::create([
			'id'=>2131,
			'country_id'=>92,
			'name'=>'Loei'
		]);

		Province::create([
			'id'=>2132,
			'country_id'=>92,
			'name'=>'Lop Buri'
		]);

		Province::create([
			'id'=>2133,
			'country_id'=>92,
			'name'=>'Mae Hong Son'
		]);

		Province::create([
			'id'=>2134,
			'country_id'=>92,
			'name'=>'Maha Sarakham'
		]);

		Province::create([
			'id'=>2137,
			'country_id'=>92,
			'name'=>'Nakhon Pathom'
		]);

		Province::create([
			'id'=>2139,
			'country_id'=>92,
			'name'=>'Nakhon Ratchasima'
		]);

		Province::create([
			'id'=>2140,
			'country_id'=>92,
			'name'=>'Nakhon Sawan'
		]);

		Province::create([
			'id'=>2141,
			'country_id'=>92,
			'name'=>'Nakhon Si Thammarat'
		]);

		Province::create([
			'id'=>2143,
			'country_id'=>92,
			'name'=>'Narathiwat'
		]);

		Province::create([
			'id'=>2144,
			'country_id'=>92,
			'name'=>'Nong Bua Lam Phu'
		]);

		Province::create([
			'id'=>2145,
			'country_id'=>92,
			'name'=>'Nong Khai'
		]);

		Province::create([
			'id'=>2146,
			'country_id'=>92,
			'name'=>'Nonthaburi'
		]);

		Province::create([
			'id'=>2147,
			'country_id'=>92,
			'name'=>'Pathum Thani'
		]);

		Province::create([
			'id'=>2148,
			'country_id'=>92,
			'name'=>'Pattani'
		]);

		Province::create([
			'id'=>2149,
			'country_id'=>92,
			'name'=>'Phangnga'
		]);

		Province::create([
			'id'=>2150,
			'country_id'=>92,
			'name'=>'Phatthalung'
		]);

		Province::create([
			'id'=>2154,
			'country_id'=>92,
			'name'=>'Phichit'
		]);

		Province::create([
			'id'=>2155,
			'country_id'=>92,
			'name'=>'Phitsanulok'
		]);

		Province::create([
			'id'=>2156,
			'country_id'=>92,
			'name'=>'Phra Nakhon Si Ayutthaya'
		]);

		Province::create([
			'id'=>2157,
			'country_id'=>92,
			'name'=>'Phrae'
		]);

		Province::create([
			'id'=>2158,
			'country_id'=>92,
			'name'=>'Phuket'
		]);

		Province::create([
			'id'=>2159,
			'country_id'=>92,
			'name'=>'Prachin Buri'
		]);

		Province::create([
			'id'=>2160,
			'country_id'=>92,
			'name'=>'Prachuap Khiri Khan'
		]);

		Province::create([
			'id'=>2162,
			'country_id'=>92,
			'name'=>'Ratchaburi'
		]);

		Province::create([
			'id'=>2163,
			'country_id'=>92,
			'name'=>'Rayong'
		]);

		Province::create([
			'id'=>2164,
			'country_id'=>92,
			'name'=>'Roi Et'
		]);

		Province::create([
			'id'=>2165,
			'country_id'=>92,
			'name'=>'Sa Kaeo'
		]);

		Province::create([
			'id'=>2166,
			'country_id'=>92,
			'name'=>'Sakon Nakhon'
		]);

		Province::create([
			'id'=>2167,
			'country_id'=>92,
			'name'=>'Samut Prakan'
		]);

		Province::create([
			'id'=>2168,
			'country_id'=>92,
			'name'=>'Samut Sakhon'
		]);

		Province::create([
			'id'=>2169,
			'country_id'=>92,
			'name'=>'Samut Songkhran'
		]);

		Province::create([
			'id'=>2170,
			'country_id'=>92,
			'name'=>'Saraburi'
		]);

		Province::create([
			'id'=>2172,
			'country_id'=>92,
			'name'=>'Si Sa Ket'
		]);

		Province::create([
			'id'=>2173,
			'country_id'=>92,
			'name'=>'Sing Buri'
		]);

		Province::create([
			'id'=>2174,
			'country_id'=>92,
			'name'=>'Songkhla'
		]);

		Province::create([
			'id'=>2175,
			'country_id'=>92,
			'name'=>'Sukhothai'
		]);

		Province::create([
			'id'=>2176,
			'country_id'=>92,
			'name'=>'Suphan Buri'
		]);

		Province::create([
			'id'=>2177,
			'country_id'=>92,
			'name'=>'Surat Thani'
		]);

		Province::create([
			'id'=>2178,
			'country_id'=>92,
			'name'=>'Surin'
		]);

		Province::create([
			'id'=>2180,
			'country_id'=>92,
			'name'=>'Trang'
		]);

		Province::create([
			'id'=>2182,
			'country_id'=>92,
			'name'=>'Ubon Ratchathani'
		]);

		Province::create([
			'id'=>2183,
			'country_id'=>92,
			'name'=>'Udon Thani'
		]);

		Province::create([
			'id'=>2184,
			'country_id'=>92,
			'name'=>'Uthai Thani'
		]);

		Province::create([
			'id'=>2185,
			'country_id'=>92,
			'name'=>'Uttaradit'
		]);

		Province::create([
			'id'=>2186,
			'country_id'=>92,
			'name'=>'Yala'
		]);

		Province::create([
			'id'=>2187,
			'country_id'=>92,
			'name'=>'Yasothon'
		]);

		Province::create([
			'id'=>2188,
			'country_id'=>69,
			'name'=>'Busan'
		]);

		Province::create([
			'id'=>2189,
			'country_id'=>69,
			'name'=>'Daegu'
		]);

		Province::create([
			'id'=>2191,
			'country_id'=>69,
			'name'=>'Gangwon'
		]);

		Province::create([
			'id'=>2192,
			'country_id'=>69,
			'name'=>'Gwangju'
		]);

		Province::create([
			'id'=>2193,
			'country_id'=>69,
			'name'=>'Gyeonggi'
		]);

		Province::create([
			'id'=>2194,
			'country_id'=>69,
			'name'=>'Gyeongsangbuk'
		]);

		Province::create([
			'id'=>2195,
			'country_id'=>69,
			'name'=>'Gyeongsangnam'
		]);

		Province::create([
			'id'=>2196,
			'country_id'=>69,
			'name'=>'Jeju'
		]);

		Province::create([
			'id'=>2201,
			'country_id'=>25,
			'name'=>'Delhi'
		]);

		Province::create([
			'id'=>2202,
			'country_id'=>82,
			'name'=>'Bolivar'
		]);

		Province::create([
			'id'=>2203,
			'country_id'=>82,
			'name'=>'Boyacá'
		]);
    }
}
