<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use App\Models\User;

class UsersSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        User::create([
        	'email' => 'admin@example.com',
        	'name' => 'administrador',
        	'surnames' => 'sistema',
        	'phone' => '1234567890',
        	'identification_number' => '12345678',
        	'birthdate' => '1990-01-01',
        	'password' => bcrypt('123456'),
        	'role' => 'admin',
            'city_id' => 10
        ]);
    }
}
