$(function(){
	$.ajaxSetup({
         headers: { 'X-CSRF-Token' : $('meta[name=_token]').attr('content') }
	})

	// Consulta y carga el listado de paises
	getCountries();

	// Calcula la edad al seleccionar la fecha de nacimiento
    $('#birthdate').on('change', calculateAge)

    //Consulta y carga los estados al seleccionar pais
    $('#countries').on('change', getProvinces)

    // Consulta y carga las ciudades al seleccionar el estado
    $('#provinces').on('change', getCities)

    //Asigna el id de la ciudad
    $('#cities').on('change', selectCity)

});

function selectCity() {
	let city_id = $(this).val()
	$('#city_id').val(city_id)
}

function getCities() {
	let id = $(this).val()

	$('#cities').empty();

	$.ajax({
		url: '/cities-by-province/' + id,
		type: 'GET',
		dataType: 'json',
		success: function(cities){
				document.getElementById("cities").innerHTML = "<option value='null'>Seleccione Ciudad</option>"
			for(let i in cities) {
				document.getElementById("cities").innerHTML += "<option value='"+cities[i]['id']+"'>"+cities[i]['name']+"</option>";
			}
		},
		error: function(error){
			console.log(error);
		}
	})
}

function getProvinces() {
	let id = $(this).val()

	$('#provinces').empty();
	$('#cities').empty();

	$.ajax({
		url: '/provinces-by-country/' + id,
		type: 'GET',
		dataType: 'json',
		success: function(provinces){
			document.getElementById("provinces").innerHTML = "<option value='null'>Seleccione Estado</option>"

			for(let i in provinces) {
				document.getElementById("provinces").innerHTML += "<option value='"+provinces[i]['id']+"'>"+provinces[i]['name']+"</option>";
			}
		},
		error: function(error){
			console.log(error);
		}
	})
}

function getCountries() {
	$.ajax({
		url: '/countries',
		type: 'GET',
		dataType: 'json',
		success: function(countries){
			for(let i in countries) {
				document.getElementById("countries").innerHTML += "<option value='"+countries[i]['id']+"'>"+countries[i]['name']+"</option>";
			}
		},
		error: function(error){
			console.log(error);
		}
	})
}


function calculateAge() {
	let today= moment()
	let birthdate = moment($(this).val())

	var age = today.diff(birthdate, "years")

	$('#age').val(age)
}