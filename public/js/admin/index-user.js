$(function(){
	$('#order-list').on('click', orderByField)

})

function editUser(id) {
	if($('#edit').val() == 0) {
		let confirmation = confirm('Esta seguro de editar el usuario?')

		if( confirmation ) {
			$('#edit').val(1)
			changeAttributeRowTable(id, false)
			$('#edit-user-' + id).html('Guardar')

			$('#birthdate-' + id).on('change', function(e) {
			 	calculateAge(id, $(this).val())
			})
		}
	} else {
		let confirmation_saved = confirm('Esta seguro de guardar los datos modificado?')

		if( confirmation_saved ) {
			$('#edit').val(0)
			let data = {
				name: $('#name-' + id).val(),
				surnames: $('#surnames-' + id).val(),
				email: $('#email-' + id).val(),
				phone: $('#phone-' + id).val(),
				identification_number: $('#identification_number-' + id).val(),
				birthdate: $('#birthdate-' + id).val()
			}

			console.log(data)

			$.ajaxSetup({
		         headers: { 'X-CSRF-Token' : $('meta[name=csrf-token]').attr('content') }
			})

			$.ajax({
				url: '/users/' + id,
				type: "patch",
				data: data,
				dataType: 'json',
				success: function(result){
					changeAttributeRowTable(id, true)
					$('#edit-user-' + id).html('Editar')
					alert('Datos actualizado')
				},
				error: function(error){
					if(error.status == 422) {
						let errors = error.responseJSON.errors
						let message = 'Ocurrio los siguientes errores: \n'
						for(let i in errors) {
							message += '- ' + errors[i][0] + '\n'
						}

						alert(message)
					} else {
						alert('Ocurrio un error al editar los datos')
					}

				}
			})
		}

		$('#edit').val(0)
		changeAttributeRowTable(id, true)
		$('#edit-user-' + id).html('Editar')
	}
}

function changeAttributeRowTable(id, state) {
	$('#name-' + id).attr('readonly', state)
	$('#surnames-' + id).attr('readonly', state)
	$('#email-' + id).attr('readonly', state)
	$('#identification_number-' + id).attr('readonly', state)
	$('#phone-' + id).attr('readonly', state)
	$('#birthdate-' + id).attr('readonly', state)
}

function deleteUser(id) {
	let confirmation = confirm('Esta seguro de eliminar el usuario?')
	if(confirmation) {
		$.ajaxSetup({
	         headers: { 'X-CSRF-Token' : $('meta[name=csrf-token]').attr('content') }
		})

		$.ajax({
			url: '/users/' + id,
			type: "delete",
			success: function(result){
				console.log(result)
				window.location.reload(true)
			},
			error: function(error){
				console.log(error);
			}
		})
	}
}


function orderByField() {
	let field = $('#fieldby').val()
	let sort = $('#orderby').val()

	let url = '/users?'
	console.log(field)
	if(field !== 'null') {
		url += 'field=' + field
	}

	if(sort !== 'null' && field !== 'null') {
		url += '&&sort=' + sort
	} else {
		url += 'sort=' + sort
	}

	$.ajax({
		url: url,
		type: 'GET',
		dataType: 'json',
		success: function(result){
			$('#list-user').html('');
			$('#list-user').html(result.users);
		},
		error: function(error){
			console.log(error);
		}
	})
}

function calculateAge(id, value) {
	let today= moment()
	let birthdate = moment(value)

	var age = today.diff(birthdate, "years")
	$('#age-' + id).html(age)
}
