@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-10">
            <div class="card">
                <div class="card-header">Perfil</div>

                <div class="card-body">
                    @if (session('status'))
                        <div class="alert alert-success" role="alert">
                            {{ session('status') }}
                        </div>
                    @endif

                    <div>
                        <div class="input-group col-md-8">
                            <div class="col-md-4">
                                <label for="id" class="">
                                    <strong>ID</strong>
                                </label>
                            </div>
                            <input  class="form-control" type="text" name="id" value="{{$user->id}}">
                        </div>
                        <div class="input-group mt-2 col-md-8">
                            <div class="col-md-4">
                                <label for="name" class="">
                                    <strong>Nombres</strong>
                                </label>
                            </div>
                            <input  class="form-control" type="text" name="name" value="{{$user->name}}">
                        </div>
                        <div class="input-group mt-2 col-md-8">
                            <div class="col-md-4">
                                <label for="surnames" class="">
                                    <strong>Apellidos</strong>
                                </label>
                            </div>
                            <input  class="form-control" type="text" name="surnames" value="{{$user->surnames}}">
                        </div>
                        <div class="input-group mt-2 col-md-8">
                            <div class="col-md-4">
                                <label for="email" class="">
                                    <strong>Telefono</strong>
                                </label>
                            </div>
                            <input  class="form-control" type="text" name="email" value="{{$user->email}}">
                        </div>
                        <div class="input-group mt-2 col-md-8">
                            <div class="col-md-4">
                                <label for="identification_number" class="">
                                    <strong>Numero de identificacion</strong>
                                </label>
                            </div>
                            <input  class="form-control" type="text" name="identification_number" value="{{$user->identification_number}}">
                        </div>
                        <div class="input-group mt-2 col-md-8">
                            <div class="col-md-4">
                                <label for="phone" class="">
                                    <strong>Telefono</strong>
                                </label>
                            </div>
                            <input  class="form-control" type="text" name="phone" value="{{$user->phone}}">
                        </div>
                        <div class="input-group mt-2 col-md-8">
                            <div class="col-md-4">
                                <label for="birthdate" class="">
                                    <strong>Fecha de Nacimiento</strong>
                                </label>
                            </div>
                            <input  class="form-control" type="text" name="birthdate" value="{{$user->birthdate}}">
                        </div>
                        <div class="input-group mt-2 col-md-8">
                            <div class="col-md-4">
                                <label for="age" class="">
                                    <strong>Edad</strong>
                                </label>
                            </div>
                            <input  class="form-control" type="text" name="age" value="{{$user->age}}">
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
