@extends('layouts.app')

@section('title', 'Crear Correo')
@section('content')
	<div class="container">
	    <div class="row justify-content-center">
	        <div class="col-md-12">
	            <div class="card">
	                <div class="card-header">Nuevo Email</div>

	                <div class="card-body">
		                <form action="{{ route('mails.store') }}" method="POST" >
							<input type="hidden" name="_token" value="{{ csrf_token() }}">

							<div class="form-row">
						    	<div class="form-group col-md-12">
						      		 <label for="affair" class="control-label">
										Asunto
									</label>
									<input type="text" class="form-control" name="affair" id="affair" value="{{ old('affair') }}" required>
									@error('affair')
									    <small class="text-danger">{{ $message }}</small>
									@enderror
						    	</div>
							    <div class="form-group col-md-12">
							    	<label for="destination" class="control-label">
										Destinatario
									</label>
									<input type="email" class="form-control" name="destination" id="destination" value="{{ old('destination') }}" required>
									@error('destination')
									    <small class="text-danger">{{ $message }}</small>
									@enderror
							    </div>
							    <div class="form-group col-md-12">
							    	<label for="message" class="control-label">
										Mensaje
									</label>
									<textarea class="form-control" name="message" id="message" value="{{ old('message') }}" required row="6"></textarea>
									@error('message')
									    <small class="text-danger">{{ $message }}</small>
									@enderror
							    </div>
						  	</div>
							<div class="form-row">
								<div class="form-group text-right col-md-12">
									<a href="{{ route('home') }}" class="btn btn-danger">Volver</a>
									<button type="submit" class="btn btn-success">Guardar</button>
								</div>
							</div>
						</form>
					</div>
	            </div>
	        </div>
	    </div>
	</div>
@endsection