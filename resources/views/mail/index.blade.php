@extends('layouts.app')

@section('title', 'Listar Correo')
@section('content')
	<div class="container">
	    <div class="row justify-content-center">
	        <div class="col-md-12">
	            <div class="card">
	            	<div class="card-header">Listado de Emails</div>

	            	<div class="card-body">
	            		<div>
							<table class="table table-bordered" width="100%" id="list-user">
								<thead>
									<tr>
										<th>ID</th>
										<th>Asunto</th>
										<th>Destinatario</th>
										<th>Mensage</th>
										<th>Estatus</th>
									</tr>
								</thead>
								<tbody>
									@foreach($mails as $mail)
										<tr class="item{{$mail->id}}">
											<td>{{$mail->id}}</td>
											<td>{{$mail->affair}}</td>
											<td>{{$mail->destination}}</td>
											<td>{{$mail->message}}</td>
											<td>{{$mail->status == true ? 'Entregado':'Pendiente por entregar' }}</td>
										</tr>
									@endforeach
									@if(count($mails) == 0)
										<tr class="text-center">
											<td colspan="5"><h2>No existe Emails</h2></td>
										</tr>
									@endif
								</tbody>
							</table>

							<div class="row">
								<div class="mx-auto pull-right">
									{{ $mails->links() }}
								</div>
							</div>
						</div>
	            	</div>
	            </div>
	        </div>
	    </div>
	</div>
@endsection