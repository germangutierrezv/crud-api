@extends('layouts.app')

@section('title', 'Crear Usuario')
@section('content')
	<div class="ml-4 mr-4">
		<div class="card">
			<div class="card-body">
		    	<h1 class="card-title text-center">Crear Usuario</h1>

		    	<div>
				<form action="{{ route('users.store') }}" method="POST" >
					<input type="hidden" name="_token" value="{{ csrf_token() }}">
					<input type="hidden" name="city_id" id="city_id" value="">

					<div class="form-row">
				    	<div class="form-group col-md-6">
				      		 <label for="names" class="control-label">
								Nombres
							</label>
							<input type="text" class="form-control" name="name" id="name" value="{{ old('names') }}" required>
							@error('names')
							    <small class="text-danger">{{ $message }}</small>
							@enderror
				    	</div>
					    <div class="form-group col-md-6">
					    	<label for="surnames" class="control-label">
								Apellidos
							</label>
							<input type="text" class="form-control" name="surnames" id="surnames" value="{{ old('surnames') }}" required>
							@error('surnames')
							    <small class="text-danger">{{ $message }}</small>
							@enderror
					    </div>
				  	</div>

					<div class="form-row">
					    <div class="form-group col-md-4">
					    	<label for="email" class="control-label">
								Correo Electronico
							</label>
							<input type="email" class="form-control" name="email" id="email" value="{{ old('email') }}" required>
							@error('email')
							    <small class="text-danger">{{ $message }}</small>
							@enderror
					    </div>

					    <div class="form-group col-md-4">
					    	<label for="identification_number" class="control-label">
								Documento de Identificacion
							</label>
							<input type="text" class="form-control" name="identification_number" id="identification_number" value="{{ old('identification_number') }}" required>
							@error('identification_number')
							    <small class="text-danger">{{ $message }}</small>
							@enderror
					    </div>

					     <div class="form-group col-md-4">
					    	<label for="password" class="control-label">
								Contraseña
							</label>
							<input type="password" class="form-control" name="password" id="password" value="{{ old('password') }}" required>
							@error('password')
							    <small class="text-danger">{{ $message }}</small>
							@enderror
					    </div>
					</div>
					<div class="form-row">
					    <div class="form-group col-md-4">
					    	<label for="phone" class="control-label">
								Telefono
							</label>
							<input type="text" min="8" max="11" class="form-control" name="phone" id="phone" value="{{ old('phone') }}" required>
							@error('phone')
							    <small class="text-danger">{{ $message }}</small>
							@enderror
					    </div>
					    <div class="form-group col-md-4">
					    	<label for="birthdate" class="control-label">
								Fecha de Nacimiento
							</label>
							<input type="date" max="<?php echo date("Y-m-d",strtotime(date("Y-m-d")."- 15 year"));?>" class="form-control" name="birthdate" id="birthdate" value="{{ old('birthdate') }}" required>
							@error('birthdate')
							    <small class="text-danger">{{ $message }}</small>
							@enderror
					    </div>
					    <div class="form-group col-md-4">
					    	<label for="age" class="control-label">
								Edad
							</label>
							<input type="text" disabled class="form-control" name="age" id="age" value="{{ old('age') }}">
					    </div>
					</div>
					<div class="form-row">
					    <div class="form-group col-md-4">
					    	<label for="countries" class="control-label">
								Pais
							</label>
							<select class="form-control" value="{{ old('countries') }}" name="countries" id="countries">
								<option>Seleccione un pais</option>
							</select>
					    </div>
					    <div class="form-group col-md-4">
					    	<label for="provinces" class="control-label">
								Estado
							</label>
							<select class="form-control" value="{{ old('provinces') }}" name="provinces" id="provinces">
							</select>
					    </div>
					    <div class="form-group col-md-4">
					    	<label for="cities" class="control-label">
								Ciudad
							</label>
							<select class="form-control" value="{{ old('cities') }}" name="cities" id="cities">
							</select>
							@error('city_id')
							    <div class="alert alert-danger">{{ $message }}</div>
							@enderror
					    </div>
					</div>
					<div class="form-row">
						<div class="form-group text-right col-md-12">
							<a href="{{ route('users.index') }}" class="btn btn-danger">Volver</a>
							<button type="submit" class="btn btn-success">Guardar</button>

						</div>
					</div>
				</form>
		  </div>
		</div>
	</div>
@endsection

@section('js')
	<script src="{{asset('js/admin/create-user.js?code='.rand(0,9999))}}"></script>
	<script src="https://cdnjs.cloudflare.com/ajax/libs/moment.js/2.29.1/moment.min.js" integrity="sha512-qTXRIMyZIFb8iQcfjXWCO8+M5Tbc38Qi5WzdPOYZHIlZpzBHG3L3by84BBBOiRGiEb7KKtAOAs5qYdUiZiQNNQ==" crossorigin="anonymous"></script>
@endsection

