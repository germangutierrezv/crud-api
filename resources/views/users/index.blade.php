@extends('layouts.app')

@section('title', 'Listado de Usuario')
@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-12">
            <div class="card mb-4">
		    	<div class="card-header">Listado de Usuarios</div>

		    	<div class="ml-3 mb-3 mt-5">
		    		<a href="{{ route('users.create') }}" class="btn btn-success">Crear Usuario</a>
		    	</div>

		    	<div class="input-group">
			    	<div class="col-md-6 input-group">
			    		<label><strong>Ordenar:</strong></label>
			    		<select id="fieldby" class="form-control ml-3">
			    			<option value="null">Seleccionar Campo</option>
			    			<option value="name">Nombres</option>
			    			<option value="surnames">Apellidos</option>
			    			<option value="email">Correo Electronico</option>
			    			<option value="identification_number">Numero de identificacion</option>
			    			<option value="birthdate">Fecha de Nacimiento</option>
			    			<option value="age">Edad</option>
			    		</select>
			    		<select id="orderby" class="form-control ml-3">
			    			<option value="null">Seleccionar Orden</option>
			    			<option value="asc">Ascendente</option>
			    			<option value="desc">Descendente</option>
			    		</select>
			    		<div>
				    		<button class="btn btn-primary ml-2" id="order-list">
	                            Ordenar
	                        </button>
			    		</div>
			    	</div>
			    	<div class="col-md-2">
			    	</div>
			    	<div class="mx-auto mb-3 col-md-4">
			    		<form action="{{ route('users.index') }}" method="GET" role="search">
				    		<div class="input-group">
		                        <input type="text" class="form-control mr-2" name="search" id="search" value="{{ old('search') }}">
	                            <button class="btn btn-info mr-2" type="submit" title="Buscar">
	                                Buscar
	                            </button>
		                        <a href="{{ route('users.index') }}" class="btn btn-danger">Resetear</a>
		                    </div>
	                	</form>
			    	</div>
		    	</div>
		    	<div>
		    		@include('notifications.success')
		    	</div>
		    	@include('users.partial')

		    </div>
		</div>
	</div>
</div>
@endsection

@section('js')
	<script src="{{asset('js/admin/index-user.js?code='.rand(0,9999))}}"></script>
	<script src="https://cdnjs.cloudflare.com/ajax/libs/moment.js/2.29.1/moment.min.js" integrity="sha512-qTXRIMyZIFb8iQcfjXWCO8+M5Tbc38Qi5WzdPOYZHIlZpzBHG3L3by84BBBOiRGiEb7KKtAOAs5qYdUiZiQNNQ==" crossorigin="anonymous"></script>
@endsection