<div>
	<table class="table table-bordered" width="100%" id="list-user">
		<thead>
			<tr>
				<th>ID</th>
				<th>Nombres</th>
				<th>Apellidos</th>
				<th>Correo Electronico</th>
				<th>Documento de Identificacion</th>
				<th>Telefono</th>
				<th>Fecha de Nacimiento</th>
				<th>Edad</th>
				<th>Ciudad</th>
				<th>Opciones</th>
			</tr>
		</thead>
		<tbody>
			@foreach($users as $user)
				<tr class="item{{$user->id}}">
					<td>{{$user->id}}</td>
					<td>
						<input type="hidden" name="edit" value="0" id="edit">
						<input type="text" class="form-control" name="name" value="{{$user->name}}" readonly id="names-{{$user->id}}">
					</td>
					<td>
						<input type="text" class="form-control" name="surnames" value="{{$user->surnames}}" readonly id="surnames-{{$user->id}}">
					</td>
					<td>
						<input type="email" class="form-control" name="email" value="{{$user->email}}" readonly id="email-{{$user->id}}">
					</td>
					<td>
						<input type="text" class="form-control" name="identification_number" value="{{$user->identification_number}}"  id="identification_number-{{$user->id}}" readonly>
					</td>
					<td>
						<input type="text" class="form-control" name="phone" value="{{$user->phone}}"  id="phone-{{$user->id}}" readonly>
					</td>
					<td>
						<input type="date" class="form-control"  name="birthdate" value="{{$user->birthdate}}" max="<?php echo date("Y-m-d",strtotime(date("Y-m-d")."- 15 year"));?>" id="birthdate-{{$user->id}}" readonly>
					</td>
					<td>
						<label id="age-{{$user->id}}">
							{{$user->age}}
						</label>
					</td>
					<td>{{$user->city['name']}}</td>
					<td>
						<div class="input-group">
							<button class="btn btn-warning btn-sm mb-1" id="edit-user-{{$user->id}}" onClick="editUser({{$user->id}})">Editar</button>
							<button class="btn btn-danger btn-sm" id="delete-user-{{$user->id}}" onClick="deleteUser({{$user->id}})">Eliminar</button>
						</div>
					</td>
				</tr>
			@endforeach
			@if(count($users) == 0)
				<tr class="text-center">
					<td colspan="10"><h2>No existe usuario</h2></td>
				</tr>
			@endif
		</tbody>
	</table>

	<div class="row">
		<div class="mx-auto pull-right">
			{{ $users->links() }}
		</div>
	</div>
</div>