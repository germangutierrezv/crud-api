<?php

use Illuminate\Foundation\Application;
use Illuminate\Support\Facades\Route;
use Inertia\Inertia;
use App\Http\Controllers\UserController;
use App\Http\Controllers\CountryController;
use App\Http\Controllers\ProvinceController;
use App\Http\Controllers\CityController;
use App\Http\Controllers\HomeController;
use App\Http\Controllers\MailController;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function() {
	return view('welcome');
});


// Rutas de Paises, Estados y Ciudades
Route::get('/countries', [CountryController::class, 'index']);
Route::get('/provinces-by-country/{id}', [ProvinceController::class, 'getProvicesByCountry']);
Route::get('/cities-by-province/{id}', [CityController::class, 'getCitiesByProvice']);

Auth::routes();
// Ruta de inicio
Route::get('/dashboard', [HomeController::class, 'index'])->name('home');

Route::get('/mails', [MailController::class, 'index'])->name('mails');
Route::get('/mails/create', [MailController::class, 'create'])->name('mails.create');
Route::post('/mails', [MailController::class, 'store'])->name('mails.store');

// Rutas de usuarios
Route::resource('/users', UserController::class);
